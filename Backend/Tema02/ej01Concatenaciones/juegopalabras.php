<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Combinación de Frases</title>

</head>
<body>
    <?php
        // Variables de entrada 
        $strFirst = "ESTA es la primera frase ";
        $strSecond = ".Esta es la segunda frase TONTA";

        //Imprime las frases originales
        echo "</p> frase 01 Original: ".$strFirst."</p>";
        echo "</p> frase 02 Original: ".$strSecond."</p>";

        $strUnidas = "$strFirst"." "."$strSecond";
        
        // frase combinada con la 1a letra en mayuscula
   
        $strTodoMinusculas =  mb_strtolower($strUnidas);
        $strMayusculaInicial =  ucfirst($strTodoMinusculas);
        echo "<p> frase combinada con primera letra en mayuscula: $strMayusculaInicial </p>";

        // una coma entre la 1a y segunda frase.

        $strUnidasConComa = "$strFirst"." , "."$strSecond";
        echo "</p> frases unidas con la coma: ".$strUnidasConComa."</p>";
 
        // un punto al final al finald e la frase combinada.

        $strFinalConPunto = $strMayusculaInicial. ".";
        echo "</p> Un punto al final de las frases: ".$strFinalConPunto."</p>";

        // cambiar la palabra TONTA por astericos.

        $strCensurada =str_replace ("TONTA","*****",$strUnidas);
        //$strUnidasCensuradas = $strFirst . $strCensurada;
        echo "</p> NO Muestra la frase censurada: ".$strCensurada."</p>";


        // eliminar puntos o espacios de  las frases originales
        echo trim($strFirst),"<br>\n";
        echo trim ($strSecond, ".");

        // averiguar la longitud total de las cadenas
        $strLongitudCadenas=strlen( $strUnidas);
        echo "</p> Longitud Total de las frases: ".$strLongitudCadenas."</p>";





    ?>
    
</body>
</html>
