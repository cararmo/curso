<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

<style>
    .tabla01{
        background-color: white;
        border: 1px solid black;
        border-collapse: collapse;
}
.rojo{
    background-color: red;
    /*width:70px; */
    height:10px;
}
.verde{
    background-color: green;
    /*width:70px;*/
    height:10px;
}

</style>
</head>

<body>

<table class="tabla01">
    <caption>Tabla Inicial</caption>
    <thead>
    <tr>
        <th scope="col">Animal</th>
        <th scope="col">Sonido</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="rojo">grillo</td>
        <td class="rojo"> grid</td>
    </tr>
    <tr>
        <td class="verde">pato</td>
        <td class="verde">quack</td>
    </tr>
    <tr>
        <td class="rojo">loro</td>
        <td class="rojo">croack</td>
    </tr>
    <tr>
        <td class="verde">perro</td>
        <td class="rojo">guau</td>
    </tr>
    <tr>
        <td class="rojo">serpiente</td>
        <td class="rojo">fff</td>
    </tr>
    <tr>
        <td class="verde">gato</td>
        <td class="verde">miau</td>
    </tr>
    <tr>
        <td class="rojo">vaca</td>
        <td class="rojo">muuu</td>
    </tr>
</tbody>
</table>

<table class="tabla02">
    <caption>Tabla Final</caption>
    <thead>
    <tr>
        <th scope="col">Animal</th>
        <th scope="col">Sonido</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>perro</td>
        <td> wof</td>
    </tr>
    <tr>
        <td>cerdo</td>
        <td>oink</td>
    </tr>
        <td>pato</td>
        <td>quack</td>
    </tr>
    </tr>
        <td>gallo</td>
        <td>kikiriki</td>
    </tr>
    </tr>
        <td>leon</td>
        <td>roarrr</td>
    </tr>
    </tr>
        <td>gato</td>
        <td>miau</td>
    </tr>
    </tr>
</tbody>
</table>
<?php
    $tabla01= [
        'grillo'=>'crick', 
        'pato'=>'quack',
        'loro'=>'croack',
        'perro'=>'guau',
        'serpiente'=>'fff',
        'gato'=>'miau',
        'vaca'=>'muu'
    ];
    $tabla02= [
        'perro'=>'wof',
        'cerdo'=>'oink',
        'pato'=>'quack',
        'gallo'=>'kikiriki',
        'leon'=>'roarrr',
        'gato'=>'miau'
    ];
    echo '<br><br>';
    echo '<h2> EL COMPRARADOR </h2>';
    echo '<table class="tabla01">';
    foreach ($tabla01 as $x=> $filas)
    {
        echo '<tr>';
        if (isset($tabla02[$x]))
        {
            echo ('<td class="verde">'.$x.'</td>' );
            if ($tabla02[$x]==$filas)
            {
                echo ('<td class="verde">'.$filas.'</td>' );
            }
            else
            {
                echo ('<td class="rojo">'.$filas.'</td>' );
            }
        }
        else
        {
            echo '<td class="rojo">'.$x.'</td>';
            echo '<td class="rojo">'.$filas.'</td>';

        }
        echo '</tr>';
        echo '</tr>';


    }
    echo '</table>';

?>
    
</body>
</html>