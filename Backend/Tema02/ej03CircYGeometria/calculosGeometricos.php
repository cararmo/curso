
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!--/*  EJERCICIO PROPORCION AUREA 
        Se enuentra en muchas figuras geometricas. */ -->
<style>
.pageGlobal{
    
    border: 1px solid rgb(30, 40, 200);
}
.circulo{
    border: 1px solid rgb(47, 24, 102);
}
.numAureo{
    border: 1px solid rgb(47, 24, 102);

}
.rectAureo{
    border: 1px solid rgb(47, 24, 102);

}
.triAureo{
    border: 1px solid rgb(47, 24, 102);

}
</style>
</head>

<body>
    <?php
    
        //variable de entrada
        $x = rand(1,10);

        // Diametro del circulo
        $diametroCircle=2*$x;

        //Perimetro del circulo
        $perimetroCircle=(2*pi()*$x);

        //area del circulo
        $areaCircle=pi()*($x**2);

        // numero aureo
        define("PHY",(1+sqrt(5))/2);

        // rectangulo aureo
        // b es la equivalente a la variable $x que es el lado corto en las formulas
        $a=PHY*$x;
        $periRectangulo= (2*$x)+(2*$a);
        $areaRectangulo= $a*$x;
    
        //Triangulo aureo

        $h= (sqrt(($a**2))-(($x**2)/4));
        $periTriangulo=(2*$a)+$x;
        $areaTriangulo= (($x*(($a**2)-($x**2))-(($x**2)/4)))/2;

        echo ('<div class="pageGlobal">');
        echo ('<div class="circulo">');

            echo("<h2>CIRCULO</h2>");
            echo ("<h3> Variable de Entrada =".$x."<h3>");
            echo ("<h3> Diametro = ".$diametroCircle."<h3>");
            echo ("<h3> Perimetro = ".$perimetroCircle."<h3>");
            echo ("<h3> Area = ".$areaCircle."<h3>");
        echo "</div>";
        
        echo ('<div class="numAureo">');
            echo("<h2>NUMERO AUREO</h2>");
            echo ("<h3> número aureo =".$x."<h3>");
         echo "</div>";

         echo ('<div class="rectAureo">');
            echo("<h2>RECTANGULO AUREO</h2>");
            echo ("<h3> longitud del lado largo =".$a."<h3>");
            echo ("<h3> Perimetro =".$periRectangulo."<h3>");
            echo ("<h3> Area =".$areaRectangulo."<h3>");
         echo "</div>";

         echo ('<div class="triaAureo">');
            echo("<h2>TRIANGULO AUREO</h2>");
            echo ("<h3> Altura  =".$h."<h3>");
            echo ("<h3> Perimetro =".$periTriangulo."<h3>");
            echo ("<h3> Area =".$areaTriangulo."<h3>");
         echo "</div>";
      


        echo "</div>";

        
    ?>
</body>
</html>