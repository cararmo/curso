<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Parte en lineas</title>
</head>
<body>
    <?php 
     // Variable de entrada 
     $strFrase = "plou poc pero pel que plou plou prou";
     echo ("frase original:".$strFrase."<br><br>");
     // palabra buscada
     $buscada="plou";
     $longPalabra=strlen($buscada);

     // primera vez  que se encuentra
     
     $posPrimeraBusqueda = strpos($strFrase,$buscada);
     $posFrase01 = strpos($strFrase,$buscada,$posPrimeraBusqueda);

     $numCaracteresFrase01= substr($strFrase,0,$posFrase01+$longPalabra);
     echo ("posicion de 'plou' la primera vez:".$posPrimeraBusqueda."<br><br>");
     echo ("la primera frase recortada es:".$numCaracteresFrase01."<br><br>");
     
     // posicion inicial de la segunda vez que se busca

     $posSegundaBusqueda=$posFrase01 + $longPalabra;
     $posFrase02 = strpos($strFrase,$buscada,$posSegundaBusqueda);
 
     $numCaracteresFrase02= substr($strFrase,$posSegundaBusqueda,$posFrase02);
     echo ("posicion de 'plou' la segunda vez:".$posFrase02."<br><br>");
     echo ("la SEGUNDA frase recortada es:".$numCaracteresFrase02."<br><br>");
     
    // posicion  inicial de la tercera vez que se busca

    $posTerceraBusqueda=$posFrase02 + $longPalabra;
    $posFrase03 = strpos($strFrase,$buscada,$posTerceraBusqueda);

    $numCaracteresFrase03= substr($strFrase,$posTerceraBusqueda,$posFrase03);
    echo ("posicion de 'plou' la tercera vez:".$posFrase03."<br><br>");
    echo ("la TERCEA frase recortada es:".$numCaracteresFrase03."<br><br>");


    ?>
    
</body>
</html>