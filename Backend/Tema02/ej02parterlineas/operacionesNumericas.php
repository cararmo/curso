<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Operaciones con numeros</title>

    <style>

        .p01{
            display:flex;
            justify-content: center;
            padding:300px;
            background: #303030;
        }

        h1{
            text-align: center;
            padding:30px;
        }
        .p02{
                border-radius: 25px;
                background: #73AD21;
                padding: 10px;
                /*width: 500px; */
                text-align: center;
                center;

            }
    

    </style>

</head>
<body>
    <h1> Ejemplos en PHP con los números</h1>
<?php

    echo '<div class="p01">';
    echo '<div class="p02">';
        // numeros iniciales
        $num01=rand(1,10);
        $num02=rand(1,10);

        // suma
        $suma=$num01+$num02;
        echo ("suma de: $num01 + $num02  = $suma"."<br><br>");

        //resta
        $resta=$num01-$num02;
        echo ("resta de: $num01 - $num02  = $resta"."<br><br>");

        //mult
        $mult=$num01*$num02;
        echo ("multipl de: $num01 * $num02  = $mult"."<br><br>");

        //division
        $divi=$num01/$num02;
        echo ("division de: $num01 / $num02  = $divi"."<br><br>");
        
        //modulo
        $modulo=$num01%$num02;
        echo ("modulo de $num01 % $num02  = $modulo"."<br><br>");

        //potencia
        $potencia=$num01**$num02;
        echo ("potencia de $num01 <sup>$num02</sup>  = $potencia"."<br><br>");

        //raiz cuadrada y redondea los decimales a 3
        $vrPositivo=abs($num01);
        $raizNro=round(sqrt($vrPositivo),3);


        echo ("raiz cuadrada de $vrPositivo = $raizNro"."<br><br>");

        // contenido de una variable
        echo "la primera variable es: ";
        var_dump($num01); echo "<br><br>";

        echo "la segunda variable es: ";
        var_dump($num02);


        // incremento y decremento
        $vrInicial = 10;
       // $sumaPosterior = ++$vrInicial; 
        //$sumaAnterior = $vrInicial++;

        echo "Valor inicial: ".$vrInicial." <br>";
        echo "suma posterior: ".++$vrInicial."<br>";
        echo "suma anterior: ".$vrInicial++;

        echo "</div>";
        echo "</div>";

    ?>
</body>
</html>