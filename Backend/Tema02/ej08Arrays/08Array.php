<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    table, th, td {
      border: 1px solid black;
      border-collapse: collapse; 
    }
    </style>
</head>
<body>
    <?php
    $columnas = ['nombre','stock','vendidos'];
    $coches =
    [
        ['Volvo',22,18],
        ['BMW',15,13],
        ['Saab',5,2],
        ['Land Rover',17,15]
    ];
    $colum=count($columnas);
    $filas=count($coches);
    echo ('<div>');
    echo '<table>';
    
    echo '<tr>';
    
    for ($i=0; $i<$colum; $i++)
    {
        echo '<th scope="col">'.$columnas[$i];
        echo '</th>';
    } 
    echo '</tr>';
    echo '</thead>';
    echo '<tr>';
    for ($i=0; $i<$filas; $i++)
    {
        for ($j=0; $j<$filas-1; $j++)
        {
            echo '<td scope="col">'.$coches[$i][$j];
       }
       echo '</tr>';
    }
    echo '</table></div><br>';
  
    echo ('<div>');
    // con foreach
    echo '<br><br>';
    echo '<table>';
    foreach( $columnas as $colum)
    {
        echo '<th scope="col">'.$colum;
        echo '</th>';
    } 
    echo '</tr>';
    echo '</thead>';
    echo '<tr>';
    foreach ( $coches as $filas)
    {
        foreach ($filas as $filas2)
        {
            echo '<td scope="col">'.$filas2;
        }
        echo '</tr>';
    }
    echo '</div><br>';
    ?>
</body>
</html>