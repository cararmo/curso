<?// COOKIES
?>

    - con cookies puedes guardar strings (cadenas de texto) ene el navegador de las personas que visiten tu pagina.
    - Las cookies permiten identificar quien te visita y como se comporta.
    - cada cookie es un string independiente.
    - el cliente adjuntara todas las cookies para nuestro dominio en todas las solicitudes que nos haga.
    - atencion: la 1a vez que el usuario entre, la cookie estara en la resp cuando el usuario recarge la pagina.
    - la cookies viaja en la cabcera de la solicitud, por lo tanto su tratamiento deben estar al princpio de todo.
    - se puede vacceder a las cookies que nos ha enviado el navegador del cliente desde el vector $COOKIE:

        EJ: print_r ($_COOKIE),

    - la cabecera de una solicitud http contiene la info sobre la solicitud, (metadatos no visibles en el cuerpo de la pagina).
    - la cabecera, las solicitudes y las respuestas viajan al principio del paquete, antes de todo el contenido.
    - las cookies y las operaciones de grabacion viajan en la cabecera de los mensajes entre elcliente y el servidor.
    - deben ahcerse antes de imprimir ningun contenido php.
    - TODAs la cookies TIENEN fecha de caducidad. se hace con la funcion : SETCOOKIE, llegada esa fecha la cookies expirará.
    - la fecha de caducidad se declara en SEGUNDOS.

    - para borrar uan cookie debe star caducada o ser null;
    - SETCOOKIE solo se puede usar antes de imprimir contenido (con echo, print.. )
        para guaradarla: setcookie($nombre,$valor,$caducidad);

    - la funcion time() devuelve el momento presente EPOCH (segundos transcurridos desde el 1 de enero de 1970 a als 00:00 hastas ahora).
        y luego se suma los segundos que faltan hasta el momento en que queremos que caduque.

        ej: <?
                $caducidad=time()+(60*60*24*7); // 1semana (segundos)
                setcookie('nombre','juan',$caducidad);
                setcookie('apellido','Garvia',$caducidad);
                ?>
                setcookie($nombre,$valor$caducidad);
      
    - para acceder ala cookie desde  $COOKIE:
            <?
                if(isset($_COOKIE['nombre']))
                {
                    $nombre=$_COOKIE['nombre'];
                }
                else
                {
                    $nombre='';
                }
                echo $nombre;

                $nombre=empty($COOKIE['nombre'])?'':$_COOKIE['nombre'];
                
           
// SESIONES
?>

    - Para  iniciar un sesion se usat sesion_start().

    - Para conocer el estado de la sesion se usa: session_status()
    <?
        if(session_status()==PHP_SESSION_NONE)
        {
            echo 'No hay session iniciada';
        }
        if (session_status()==PHP_SESSION_ACTIVE)
        {
            echo "sesion activa";
        }
    ?>
    - para cerrar la sesion es usa session_destroy();
    <?
        $session_start();
        $S_SESSION['Nombre']='siso';
        echo $_SESSION['Nombre'];
        unset($S_SESSION['nOMBRE']);
        session_destroy();
    ?>

    PARA ELIMINAR UNA sesion de forma segura:

    - unset($_SESSION['COUNT']); //desregoistrar o eliminar una variable de sesion (count)
    - session_destroy(); --> para cerrr o eliminar la sesion.
<?
// ENVIO DE EMAIL
?>
    - Podemos enviar un email desde PHP utilizando la funcion mail:
        retorna true/false si puede enviar el correo.

        <?
        mail(para,asunto,mensaje,cabecera); ?>

    - las 3 primeras son obligatorias, la 4a (cabecera) es opcional y es uan cadena que puede contener info multiple.

        Si la envia retorna true, de lo contrario retorna false.

    - para enviar mensajes se debe tener configurado un servidor de correo saliente, configurado.

    - el paramtero para se inroduce la direcc del destinatario del correo. Si son varias direcciones, se separan con comas.
        (mail1@gmail.com,mail2@gmail.com).

    - si se requiere introducir saltos de linea en el parametro 'mensaje' del email, se hace introduciendo los carateres \r\n (retorno de carro y ueva linea).
        esta ddefinido asi para compatibilidad con sistemas antiguos.

    - la cabecera se usa para añadir paramteros extra(From, CC y Bcc). Las cabeceras multiples adicionales se separan con CRLF (\r\n).

        $cabecera='from:backend@raining.cat'

    - NOTA: Gmail NO acepta correos sin el campo 'from'.

    EJ: <?
        $destino='destino@gmail.com';
        $asunto='primer correo';
        $cabeceras='from:backend@training.cat\r\n bcc:destino2@gmail.com';

        mail($destinno,$correo,$asunto,$cabeceras);
        ?>
<?
// FICHEROS
?>

    - para comprobar si existen ficheros se usa: file_exists('fichero.txt')
        la funcion devuelve TRUE si el fichero existe o FALSE si no existe.

    - para grabar un archivo se usa la funcion is_writable(), devuelve TRUE si es posible escribir en el archivo.
        is_writable('fichero.php')
        la funcion devuelve TRUE si PHP tiene permisos para escribir en el archivo.

    - para ver si s epuede leer un archivo se usa la funcion is_readable().
        devuelve TRUE si es posible leer archivo.

    - el primer paso para trabajr con archivos en PHP es ABRIRLO.
        para abrrlo se usa fopen().
        luego se puede actuar sobre el archivo(leer o escribir) usando el enlace que retorna fopen().
        $enlace = fopen('archivo.txt,$modo);
        fopen() devuelve FALSE si no se pude abrir.
<?
//    - para poder grabar se abre en MODO ESCRITURA con: ?>
        $enlace=fopen('archivo.txt','w');
        en el modo  w se empieza a escribir desde el primer caracter.
        Si ya existe el archivo con ese nombre, PHP lo borra y crea un nuevo archivo en blanco.
        si todavia no existe el archvo PHP lo crea.

    - para escribir sobre el archivo abierto se usa la funcion fwrite();

        $byteescritos=fwrite($enlace,'hola');

        la funcion devuelve el numero de bytes escritos o false si no ha podido escribir.

    - para abrir el archivo sin destruir lo que ya contiene se usa:
        $enlace = fopen('archivo.txt','x');
        PHP creara el fichero en caso de que no exista.
    
    - cuando se termina de tabajar con el archivo se debe cerrar con:
        fclose($enlace);

    - para insertar salto de linea eb ficheros 
        Windows: \r\n 
        Linux : \n

        PHP automatico: PHP_EOL

    - ver fecha actual $fecha=date('j/n/Y \a \l\a\s G:i:s');
    
<?   //para abrir el archivo en MODO LECTURA se usa:
?>
        $enlace=fopen('archivo.txt','r'),

        empieza a leer desde el primer caracter.

<? // Para leer una linea del fichero abierto usando FGETS ?>

        - fgets($enlace);

        - fgets leerá desde el punto donde terminó la última lectura
            hasta el siguiente salto de linea.
        - cuando no queden lineas por leer, retonará FALSE.
        - NOTA: NO devuelve el caracter de salto de linea.

<?// leer un fichero abierto por trozos, se usa el comando: ?> 
        - $contenido=fread($enlace,$numerobytes);

        - se debe especificar la cantidad de caracteres desde la posicion donde termino la ultima lectura,
            al llegar al final del fichero, fread retornará FALSE.

        - Para conocer el tamaño del archivo en bytes se usa:
            filesize('fichero.txt');

            la funcion devuelve el nro de bytes que contiene el fichero.

<?// para conocer la posicion desde doonde se va a escribir o leer se usa: ?>

        - $pocision=ftell($enlace);

<?// para colocar el puntero al princpio del archivo se usa: ?>

        - rewind($enlace);

<? // para colocar el puntero en cualquier posicion se usa: ?>

        - fseek($enlace,0);
        - si la operacion tiene exito devuelve 0, si no -1

        - fseek($enlace, $posicion-3);
        - de nuevo si la operacion tiene exito devuelve 0, si no -1

<?//  para cambiar el punto inicial desde donde se empeiza a contar con fseek(), ?>
        con la constante SEEK_CUR.
        
        fseek ($enlace,$offset,SEEK_CUR);
        fseek ($enlace,0,SEEK_END);

<?// para saber si el punto esta al final del fichero se usa : ?>
    - feof($enlace);

    si el puntero esat al final, la funcion devolvera TRUE, en caso contrario FALSE.
    
<?// resumen de los modos de administarcion de archivos: ?>

        r -> lee desde el comienzo (debe existir)                         -> r+ -> escribe y lee de donde se quiera.
        w -> elimina el contenido del archivo  / escibe desde el comienzo -> w+ -> permite ademas leer en cualquier punto.
        a -> agrega / escribe desde el final                              -> a+ -> permite leer ademas de grabar al final siempre.
        x -> escribir desde el comienzo (puede no existir)                -> x+ -> permite ademas leer

<? //28/02/2022
   //  LEER DE UNA VEZ UN ARCHIVO
?>
   - con file_get-contest permite abriri un archivo, leerlo entero y cerrarlo, utilizando un solo comando.

    $datos=file_get_contents('archivo.txt');

   // para escrirbr una vez

    - con file_put_contents, permite grabar ficheros.
    - la funcion retorna el numero de bytes escritos. atencion: file_put_contents sobreescribe!
        file_put_contents('archivo.txt',$datos)
<? // SERIALIZE ?>

        - permite convertir un array en un string, funciona con array asociativos y multidimensionales.
            $string=serialize($array);

        UNSERIALIZE :       realiza el proceso inverso, acepta un sring que procede de un array serializado y lo convierte en un array.

            $array=unserialize($string);

<? //ENVIAR FICHERO EN FORM ?>

    - Em el array $_files, se puede hallar toda la info relacionada con el fichero subido,
        incluyendo la ruta temporal donde se encuentra el archvo en el servidor, el nombre original del fichero y
        un codigo que indica si se han producido erroes ene l proceso.

    - estructura: 

        
        [archivo]=> Array   -> name del input de tipo file en HTML
            [name] => wally.jpg  -> Nombre y extension original
            [type] => image/jpge etc ->tipo de datos que contiene el archivo recibido
            [tmp_name] =>/applications/mamp/tmp/php/phpvrFdoa ->ruta temproal del archivo en el servidor
            [error] => 0  -> contiene 0 si no han habido errrores.
            [size] => 50850  -> tamaño del fichero (bytes)

    - para trasladar archivos que han enviado de un form , se usa la siguiente funcion:
        
        move_uploaded_file(), devuelve true si ha podido trasladarlo.

        move_uploaded_file($rutatemporal,$rutafinal);

<? //ADJUNTAR MULTIPLES ARCHIVOS ?>

        - se debe incluir el atributo 'multiple' en el input y colocar despues [] en el nombre del input.

<?    //maro 3/2022
?>
    - la funcion is_dir() permite  saber si el nombre corresponde a una carpeta

        is_dir('nuevacarpeta');
        
        devuelve true si corresponde a una carpeta de lo contrario retorna false, si no existe o no es una carpeta.

    - la funcion rename() permite renombrar un archivo.

        rename('archivo.txt','renombrado.txt');

    - la funcion copy permite duplicar un archivo o directorio.

        copy ('archivo1.txt','archivo2.txt');

    - la funcion glob() permite listar archivos en una carpeta (retorna un array)

        glob('*.txt')

        *.txt - ficheros txt de la carpeta actual.
        *.* - todos los archivos.
        * - todos los archivos y carpetas.

    - PHP dispone de varias funciones para trabajar con la ruta de un archivo.

        - $ruta='carpeta/fichero.php';

    - la funcion basename() extrae de la ruta solo el nombre del archivo.

        - $ruta='carpeta/fichero.php';
            basename($ruta);

            devuelve el string 'fichero.php'

        si se pasa un segundo argumento a la funcion basename, extraera elnombre del archivo SIN INCLUIR la extension.

        - $ruta='carpeta/fichero.php';
            basename($ruta,'php');

            devuelve el string 'fichero'. 

    - la funcion dirname() extrae de la ruta solo el string con los diectorios (sin la / del final pero (si la tiene), si que se incluiria la / del principio)

        $ruta='carpeta/fichero.php';
            dirname($ruta);

            devuelve el string 'carpeta'. 

    - la funcion realpath() devuelve la ruta completa del archivo en el disco duro del servidor.

        $archivo ='archivo.txt';
        realpath($archivo);

        devuelve el string : 'c:\xampp\htdocs\phpsandbox\web\archivo.txt'. 


<? // INPUT OCULTO '?>

    - Podemos insertar un input que no sea visible en el inerior de un form usando
        type='hidden'. aunque NO APAREZCA EN LA WEB, php recibira su name y su valuw.

        <input type='hidden' name='oculto' value='1'>


    