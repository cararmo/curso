<?php
/* Caracteristicas de php

- php permite contenido dinamico.
- permite crud ficheros en el dd del servidor.
- puede recoletcar y procesar datod de form y urls.
- phph trabaja con bd (crud).
- puede controlar acceso a los usuarios.
- puede usar cookies y seciones.
- puede cifrar y descencriptar datos.
- es gratis y opensource.
- funciona en toso sloa s.op.
- muy extendido.
- la mayoria de web del world estan en php.
- la mayoria de gestores de contenido estan hechos en php.

* que es un file php

- es un fichero qu puede contener codigo html,css,javascript + codigo php.
- php se ejecuta en el servidor y el resultado se devuelve al cliente como HTML.
- los archivos PHP usan la extension .php.

que se necesita

- encontrar un servidor de hosting con soporte php
- instalar un servidor web (apache,ngix) + php (el interpetre) + mysql /mariadb, para manejo de bd, en tu ordenador.
- para resumir se puede usar un soft que se llama xampp, wampp, mampp, lampp.

- se debe colocar los archivos en uan carpeta prredefinida, y para ahcerla servir, se hace a traves de una direccion ip.
- la direccion ip para el servidor local es: localhost.
- la carpeta de inicio del servidor es xampp/htdocs.
*/
/*SINTAXIS

- un script se puede colocar en cualuqier lugar del doc html.
- empieza con: <?php y termina con ?> */
    <?php
        //Script PHP
    ?>

- // -> es un comentario en PHP.
- para insertar texto en PHP se usa -> echo

- <?php
        echo "texto PHP";
    ?>
// - COMENTARIOS

/* comentario de multiples lineas, (igual que en CSS) 
 */

// Comentario de una sola linea, no necesita cerrarse

# otra manera de insertar un comentario de una sola linea

// comando "PRINT", es igual para imprimir como el comando "ECHO"
- se pueden usar con y sin parentesis.
- echo es ligeramente mas eficiente.
- echo permite multiples parametros (solo si no usamos parentesis)

EJ: IMPRIMIENDO LAS ETIQUETAS HTML.

<?php
    echo("<h2>Me encanta el PHP!</h2>");
    echo "<p>Hola Mundo!! sin parentesis</p>";
    echo "<p>","varios","textos","concatenados","</p>";
?>

// - VARIABLES PHP

- Las variables, son "contenedores" que nos permiten almacenar info.
- suelen tener un nombre qque describe su contenido, o un nombre corto.
- en PHP los nombres de las variables es OBLIGATORIO, EL SIGNO $. ($edad, $nombre,$y etc).
- el nombre de la var, NO puede empezar con un numero.

- los nombres de variables pueden contener caracteres alfanumericos,numeros y la barra baja.
    ($coche, $PERRO, $menu1, $menu_izq).

- los nombres de var. son sensibles las may/minus ($edad y $EDAD, son variables diferentes)
- Se recomienda que las variables SIEMPRE empiecen con minusculas.

// - STRING

- un string e suan secuancia de caracteres.
- puede ser cualquier texto.
- hayque esribirlo entre comillas, sencillas o dobles.

// - para asignar el contenido a una variable se usa el signo = .

- y volcamos su contenido al HTML con echo.

ej: 

    <?php
    $texto="texto de prueba";
    echo $texto;
    ?>

// CONCATENACION DE TEXTO,

Si se usa comillas dobles se puede insertar variables dentro del string rodeandolas por llaves : {}

    <?php
    $test="prueba";
    echo "texto para hacer {$test}s".;

    ?>

las comillas simples no se insertan en el valor de la variable, se interpetra como texto normal.

    <?php
    $test="prueba";
    echo 'texto para hacer {$test}s'.;

    ?>

Se puede concatenar estring con el signo punto (.)

    <?php
    $texto="Texto "."de "."prueba.";
    echo $texto;

    ?>

si una variable contiene un string podemos concatenar otro al final con el operador ".="

<?php
    $texto="Texto unido con .= "."de ";
    $texto .="prueba.";
    echo $texto;

    ?>

// SUBSTITUIR TEXTO CON STRING.

la funcion str_replace(), reemplaza un texto por otro dentro de un stream_set_blocking,
el orden en que consume la funcion es dentro del parentesis, (palabra buscar, palabra a reemplazar y por ultimo donde esta el texto.)

    <?php
        echo str_replace("world","dolby","Hello world");

    ?>

// ELIMINAR ESPACIOS

se usa la funion TRIM()

elimina espacios del principio  y final de un string, (espacio, tabulador,salto de linea)

    <?php
        echo trim("  Borra espacios del principio y final  de una cadena de texto ");
    ?>

* puede usarse para eliminar otros caracteres de principio y del final de un string.
 " " espacio simple. "\t" tabulacion. "\n" salto de linea

    <?php
        echo trim(" *Borra.*.otros...",".*\n \t");
    ?>

* la funcion ltrim() 
elimina espacios en blanco al inicio de un string.
* la funcion rtrim()
elimina espacios en blanco al final de un string.

<?php
        echo ltrim(" Borra esapcios al prinicpio del texto");
        echo rtrim(" Borra esapcios al final del texto");

?>

// CONVERTIR A MAYUSCULAS. y minusculas

<?php
        echo mb_strtoupper("hola mayusculas! \n");
        echo mb_strtolower("HOLA MINUSCULAS \n");

?>

/CONVERTIR LA PRIMERA LETAR A MYUSCULAS

<?php
        echo ucfirst("hola primear letra en mayusculas \n" );

?>

//convertir la 1a letar de cada palabra de un string a mayusculas.

<?php
        echo ucwords("Hola Cada priemra Letra en Minusculas \n" );

?>

// PHP.NET pagina web con la DOC OFICIAL.

//CONTAR CARACeTRES.

- Se usa la funcion strlen()

<?php
        echo strlen("Hola longitud de la cadena" );

?>

// CONTAR LAS PALABRAS DE UN STRING

<?php
        echo str_word_count("Hola cuantas palabras hay aqui" ) ;

?>

// HACER UN STRING DE OTRO

la funcion substr() devuelve un string a partir de un fragmento de otro string.

ECHO SUBSTR("ADIOS MUNDO CRUEL,6,11);") // devuelve MUNDO 

//BUSCAR TEXTO EN UN STRING

la funcion strpos() busca un string concreto dentro de un string, en caso de que
lo encuentre, la funcion retornara la posicion del caracter donde se ha dado la primera coincidencia,
si no lo haya,, devuelve el valor false.

echo strpos("Hello World","world"); // devuelve la posicion 6

// Si se pasa un TERCER PARAMETRO, se puede especificar un offset(en caracteres), a partir 

    del cual se sigue buscando.

    echo strpos("Hello world world!","world",6); // devuelve el segundo world

// INTEGER EN PHP 

Los operdorea artimeticos permiten relizar las operaciones matematicas basicas + - * / 
-> + suma
-> - resta
-> * división
-> / multiplicacicón
-> %  modulo.    $x%$y   -> Resto de la division de $x por $y
-> ** exponente. $x**$y  -> Resultado de elevar $x a $y , esta version funciona para los PHP mas nuevos

// DEVOLVER VALOR ABSOLUTO: ABS()

    abs() -> si es negativo lo pasa a positivo
    ej: echo abs(-512); -> resultado: 512

// ELEVAR UN NUMERO A OTRO: POW()
    ej: echo pow(3,3) -> Esta es la forma mas antigua 

// RAIZ CUADRADA SQRT()

    ej: echo sqrt(9) -> devuelve la raiz cuadrada de un número.

// NUMERO ALEATORIO RAND()

    La funcion rand() entrega un nro aleatorio.
    echo rand(1,6)   el primer nro es el ranngo inicial y el segundo el rango final, entre los cuales saldra el nro aleatorio.

 //FUNCION VAR_DUMP()

    La funcion var_dump() retorna el contenido de uan variable y su tipo

    si se quiere ver mejor se puede envolver en la etiqueta <pre> </pre>

// FLOAT

    Es un tipo de datos que permite numeros decimales.
    OJO que se separa con el signo punto, y no con coma.

        $valordoble=5.45;

// REDONDEO DE decimales

    La funcion round() fuerza el redondeo.

        echo round(500.69753,2); -> muestra 500.70

// REDONDEO hacia ARRIBA y hacia ABAJO

    La funcion ceil() convierte al int inmediatamente superior.

    echo ceil(500.66353) --> muestra 500.70

    la funcion floor() convierte al int inmediatamente inferior.

    echo floor(500.66353) --> muestra 500.60

// OPERADORES DE ASIGNACION

    Los op. de asignaciones se usan para escribir valores en una variable

    =  -> $x=$y  el operando de la izq adquiere el valor de la espresion de la derecha.( es le tradicional)

    += -> ($x += $y) es como ($x = $x+ $y) Suma.

    -= -> ($x -= $y) es como ($x = $x - $y) Resta.

    *= -> ($x *= $y) es como ($x = $x * $y) Multiplicacion.

    /= -> ($x /= $y) es como ($x = $x / $y) Division.

    %= -> ($x %= $y) es como ($x = $x- $y) Modulo.

// OPERADORES De Incremento O  De Decremento

    ++$x Pre-incremento   -> Incrementa $x en uno y retorna $x

    $x++ Post-incremento  -> Retorna $x e incrementa $x en uno

    --$x Pre-decremento   -> Decrementa $x en uno y retorna $x

    $x-- Post-decremento  -> Retorna $x y decrementa $x en uno

// CASTING AUTOMATICO

    PHP convierte automaticamente una variable a su tipo de datos adecuado en funcion de su contenido.
    
    En otros lenguajes (c,c++,java)se debe declacar la var antes de usarla.

// CONSTANTES PHP

    es un identificador para un valor fijo., el valor no se puede modificar durante la ejecucion del script. 
    el nombre de las constante NO empiezan con el simbolo $, a diferencia de las variables.

    se usa la funcion define(), para declararla.

    <?php
        define(nombre,valor) -> define("PI",3.141592653)
        echo PI;
    ?>

    // FUNCIONES PREDIFINIDAS.

        Funcion Pi retorna el numero Pi.
        <?php
            define("PI",pi());
            echo PI;
        ?>

// VALOR NULL

    nulL es un tipo de dato que representa uan variable vacia.(no tiene ningun valor asignado.
    podemos vaciar la variable asignadole null, aunque en PHP no hay necesidad porque lo hace el.

    <?php
        $x= null;
        var_dump($x);
        
    ?>

// BOOLEAN

    Un booleano tiene 2 posibles valores verdadero (true) o falso (false).
    <?php
        $x=true;
        $y=false;
        var_dump($x);
   ?>

    se utilzian mucho en las expresiones condicionales.

// OPERADORES COMPARATIVOS.

    Se usan para comparar valores (numeros/string), el resultado de ñ a compracion es un booleano (true o false).
    los siguientes son los operadores comparativos mas comunes.

    - operador: xor nombre: XOR ejemplo: $x xor $y - true si cualquiera de los dos $x o $y son true, pero no los dos.
    - operador: &&  nombre: AND ejemplo: $x && $y - true si los dos $x y $y son true.
    - operador: ||  nombre: OR ejemplo: $x || $y - true si cualquiera de los dos $x o $y son true.
    - operador: !   nombre: NOT ejemplo: !$x - true si $x noes true y false si $x no es false.

    - operador: ==    nombre: IGUAL ejemplo: $x==$y - retorna true si $x es igual a $y.
    - operador: ===   nombre: IDENTICO ejemplo: $x===$y - retorna true si $x es igual que $y, y ambos son del mismo tipo.
    - operador: !=    nombre: NO IGUAL ejemplo: $x!=$y - retorna true si $x NO es igual a $y.
    - operador: <>    nombre: NO IGUAL ejemplo: $x<>$y - retorna true si $x NO es igual a $y.
    - operador: !==   nombre: NO IDENTICO ejemplo: $!==$y - retorna true si $x NO es igual a $y, O SON DE TIPOS diferentes.
    - operador: >     nombre: MAYOR QUE ejemplo: $x>$y - retorna true si $x es mayor que $y.
    - operador: <     nombre: MENOR QUE ejemplo: $x<$y - retorna true si $x es menor que $y.
    - operador: >=    nombre: MAYOR O IGUAL QUE ejemplo: $x>=$y - retorna true si $x es mayor o igual a $y.
    - operador: <=    nombre: MEOR O IGUAL QUE ejemplo: $x<=$y - retorna true si $x es menor o igual a $y.



// SENTENCIAS condicionales

// IF 

    la sentencia if se ejecuta cierto codigo solo si la condicion es true y otro codigo si es false.

    if(condicion)
    {
        codigo a ejecutar si la condicion es true;   
    }else{
        codigo a  ejecutar si la condicion es false;

    }

// IF... ELSEIF... ELSE

    Esta entencia ejecuta cierto codigo si la 1a condicion es true, otro codigo si ha sido false, pero se cumple la segunda o alguna 
    de las condiciones elseif siguientes.

//SWITCH

    Esta sentencia para seleccionar diferentes acciones de entre una lista de bloques de codigo.
    La seleccion de una u otra opcion dependedera del contenido de una variable.

    switch($variable){
        case valor1:
            codigo a ejecutar si $variable=valor1;
            break;
        case valor2:
            codigo a ejecutar si $variable=valor2;
            break;
            case valor3:
            codigo a ejecutar si $variable=valor3;
            break;
        ...
        default:
            codigo a ejecutar si $variable no coincide con ninguno;
    }

    //OPERADOR TERNARIO

    $m=12;
    $nom= ($m=1?"enero": 
    ($m=2?"enero":
        ($m=3?"enero":
        ($m=4?"enero":
        ($m=5?"enero":
        "otro")))))
  