<?php

//FUNCIONES PHP ?>

- Una funcion es un bloque de sentencias que pueden usarse de forma repetida en un programa.
- PHP tiene mas de 1000 funciones,ademas de las integradas, tambien se pueden crear las propias.
- La funcion personalizada no se ejeutara a menos que se invoque.

function nombredelafuncion()
{
    codigo de la función;
}

Ejemplo:
    <?php
    function saludo()
    {
        echo 'Hola Mundo!!';
    }
    saludo();

// ARGUMENTOS DE FUNCIONES ?>

    - Podemos pasar info a uan fucnion usando argumentos, un argunmento actua como una variable.
    - Los argumentos especifican despues del nombre de la funcion, dentro del parentesis.
    - podemos añadir multiples argumentos separados con comas.

    <?php
        function nombrap($nombre, $apellido)
        {
            echo '{$nombre}{apellido}.<br>';
        }

        nombrap('jorge','Moya');
        nombrap('Siso','Cuarteto');
    
    // ARGUMENTO POR DEFECTO
    ?>
        - Se puede utilizar un argumento por defecto al momento de llamar la funcion
            asignadola con el simbolo = 


    <?php
        function printaltura($altura=2)
        {
            echo 'la altura es :{$altura} metros.<br>';
        }

        nombrap('jorge','Moya');
        nombrap('Siso','Cuarteto');
    
    // valor de retorno ?>

        - SUCEDE CUANDO una funcion devuelve el resultado de las operaciones para ser utilizado fuera de ella
    <?php

        function suma($x,$y){
            $z=$x+$y;
            return $z;
        }

        $n1=5;
        $n2=3;
        $resultado =suma($n1,$n2);
        echo "{$n1} + {$n2} = {$resultado}.";

    ?>

- al momento de imprimir se puede usar punto com venimos usando o coma.     

<?php
        function suma($x,$y){
            $z=$x+$y;
            return $z;
        }
        echo "5 + 10=".suma(5,10)."<br>";
        echo "7 + 13=",suma(7,13),"<br>";

  
 // VALOR DE RETORNO EN UNA FUNCION CON MATRICES. 

        function sumaresta($x,$y)
        {
            $s=$x+$y;
            $r=$x-$y;
            $vector=[$s,$r];
            return $vector;
        }
        print_r (sumaresta(5,2));

        ?>
<?php
// FUNCION ISSET ?> 

- La función isset($variable) devuelve TRUE en caso de que
    $variable exista o FALSE si la variable no existe.

    - Se PUEDE USAR CUALQUIER Variable.
    - Es util para comprobar si existe un array o alguno de sus elementos.

<?php
// INCLUSION DE ARCHIVOS ?>
    - PHP permite incluir codigo  que se encuentre en una archivo externo.
    - nos permite crear codigo mas legible, reutilizar y centralizar cambios en codigo comun.
    - el archivo externo puede contener codigo PHP o de cualiqer otro tipo.

    si el archivo externo sollo contienee codigo PHP,podemos omitir la etiqueta de cierre?>(la etiqueta de apertura si es oibligatoria).

<?php   
    include ("archivoexterno.php");
?>


<?php

// OTRAS FUNCIONES 

        // FUNCION REQUIRE ?>

            A diferencia de include(), si se usa require(); saltara un error si la inclusion no puede realizarse.

            <?php   
            require ("archivoexterno.php");

        // FUNCION requiere_once ?>

            - Con esta funcion no permitira que la inclusion se realize mas de una vez, aun si se realiza la llamada varias veces.
            
            - PHP ABORTA EN CASO DE DECLARASE LA MISMA FUNCION MAS DE UNA VEZ, POR PRECACUION , USAREMOS CUANDO QUERAMMOS incluir un fichero externo donde se
            declaren funciones.
<?php   
    
//  $_SERVER SUPERBLOBAL ?>

        - $_SERVER (es un array asociativo), es una variable SUPERGLOBAL que nos permite acceder a info del servidor. Se genera 
        automaticamente y esta a nuestra disposicion en cualuqier lugar del codigo.

        con print_r podemos ver lo que contiene.

        - en $_SRVER['HTTP_HOST'] : Contiene el nombre del dominio donde se encuentra la pagina 
        - ej: localhost.

        - $_SERVER['HTTP_REFERER'] : Contiene la direccion de la pagina que nos ha enlazado.
        - ej: http://pacopil.net/enlaces.html


        $_SERVER['HTTP_USER_AGENT'] : Contiene el navegador utilizado por el visitante del sitio web.
        - ej:    Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11;rv:46.0) Gecko/20100101 Firefox/46.0 
        
        $_SERVER['REMOTE_ADDR'] : Contiene la direccion IP de visitante.
        - ej:  137.34.80.52

        $_SERVER['PHP_SELF'] : Conitene el nombre del archivo en ejecución.
        - ej: /formulariocheck/form.php

        $_SERVER['REQUEST_METHOD'] : Contiene el método utilizado en la petición. (GET O POST)
        - ej: POST   ( es un mecanismo para saber si se ha recibido un form basicamente.)
        
        $_POST La variable superglobal $_POST, contiene la info enviada en el form usando el metodo POST.
        - El vector asociativo $_POST utiliza como clave los nombres especificados en el atributo "name" de cada elemento del form.

        La función EMPTY($variable) devuelve TRUE en caso de que $variable este vacia o 
            FALSE si hay algo dentro de la variable. (reemplaza el uso de isset y strlen(), en la validacion de un form)

        en cada uno de los imput hay que usar value=' '; para que el sistema pueda recordar lso campos que el usuario puso.
        
 //    POST DATA CHECK
        
        - Podeomos comprobar si hemos recibido datos POST utilicando:

            if($SERVER['REQUEST_METHOD']==POST)

        - Con al funcion ISSET, tambien se puede comprobar si se ha recibido el form comprobando si existe $_POST['submit'].
            if(iiset($_POST['submit']))

            este metodo solo funciona si al boton se le ha puesto nombre.            
        
<?php        
// FUNCION EMPTY

// funcion HTMLSPECIALCHARS
?>

        - La funcion htmlspecialchars($string); convierte cualquier caracter del string
            susceptible a confundirse con codigo HTML a su entidad HTML equivalente. (se hace por seguridad).

            <?php        
// FUNCION ENTITIES

?>  La funcion html_entities(),codifica todos los caracteres cuya entidad html existe, por ej, tambien convierte todos las lestras acentuadas.

    - puede ser util cuando la codificacion de caracteres del destino es desconocida (puede ser distinta a UTF8)
<?
// funcion ENTITY DECODE
?>
    - la funcion html_entity_decode() realiza la funcion inversa a html_entities() devuelve el string con caracteres codificados a entidades html
        a su estadooriginal, interpetable como codigohtml.
<?
        $texto='<h1>hola</h1>';
        echo ($texto);
        $texto = html_entities($texto);
        echo ($texto);

//Funcion STRIP TAGS
?>

    - la función strip_tags($string)elimina todas las etiquetas html en un string.
        Se puede pasar un segundo parametropara permitir algunas etiquetas.
        <?
        $texto='<p>Text</p><!-- Hola --> <a href="#">Adiós</a>';
        echo strip_tags($text);
        echo '\n';
        // permite <p> y <a>
        echo strip_tags($texto,'<p><a>');

        ?>
    - Al retirar las etiquetas con strip_tags() también se eliminan sus argumentos.
        (por ej. argumentos con codigo javascipt).
        Si se omite la   

<?
// EXPRESION REGULAR para validar un email en PHP 
?>
    - PHP tiene una funcion para validar internamenet si un campo email, realmente es un email o no.
        filter_var() filtra una varable de acuerdo a ciertos patrones preestablecidos.

        Se usa para validar direcciones de URL's y direcciones IP.

        La funcion devuelve TRUE o FALSE.

        filter_var($email,FILTER_VALIDATE_EMAIL)
        filter_var($web,FILTER_VALIDATE_URL)
        filter_var($ip,FILTER_VALIDATE_IP)
        
        - se puede usar para limpiar posibles caracteres invalidos que contenga la variable,con SAITIZE.
        filter_var($web, FILTER_SANITIZR_URL)
        filter_var($email, FILTER_SANITIZR_EMAIL)

    hacer form de email y contraseña
<? 
// CIFRADO DE PASSWORDS
?>
    - se deben cifrar las contraseñas antes de almacenarlas
    - www.haveibeenpwned.com informa de cuantas brechas de seguridad ha habido en tu cuenta /o nro de telef.

    - se deben cifrar antes de almacenarlas en un servidor.

    $pwc=md5('rainbow');

    se puede comprobar si la contraseña es correcta comparando el string resultante de encriptarla con el resultado almacenado 
    
    - el cifrado md5 es inseguro y esat en desuso para webs seguras.
 <?   
    // CIFRADO BLOWFISH
?>

        - Es un cifrado mas robusto ya que cada vez genera la contraseña, devuelve un valor diferente.

        se usa la funcion password_bcrypt(); y para verificar si la contraseña es buena  a traves de su clave, lo comprobamos con 
        la funcion PASSWORD_VERIFY();

        - con la constante 'PASSWORD_DEFAULT' el sistema ecriptara la contraseña usando el algoritmo mas avanzado disponible en la fecha de encriptacion.
        - la cadena resultante puede contener hasta 256 caracteres. (en la bd , minimo hay que dejar 256 espacios de caracteres).
        








