<?//------------ GIT ------------- ?>

    - Guarda un historial de versiones de nuestro proyecto.
    - Trabajar en equipo y en multiples ordenadores.
    - Trabajar de forma paralela en diferentes partes del programa.

    - GIT permite recuperar verisones anteriore de ficheros.

        http://git-scm.com/

<?    // para instalar en linux    ?>
    - sudo apt install git

<?//CONFIGURAR EL USUARIO ?>

- lo primero es espeficar los detalles del usaurio (nombre y mail)

- se crea un nuevo repositorio GIT en la carpeta donde se encuentren los archivos del proyecto con:

    GIT INIT

hay que crear una carpeta donde se quiere usar el control de versiones.

<?// omision de archvos ?> 

- para hacer que GIT omita algunos ficheros o directorios que haya en el directorio del proyecto, se create_function

    un archivo que se llame .gitignore
    
git tiene:

- directorio de trabajo
- directorio de staging area: es un area intermedia donde estan los cambios que se realizan del direcotrio de trabajo y estan 
    listas para subir al repositorio. 
- repositorio.

// para añadir o pasar archivos/directorios  modificados en stating area se usa los comandos:

    - git add -A o 
    - git add .

// para añadiral repositorio los archivso que estan en el staing area con:

    - git commit

    es IMPRESCINDIBLE INCLUIR un mensaje que explique que se ah camabiado o añadido al proyecto en ese commit

    git commit -m "cosas que he cambiado"


    - git log - sirve para ver el listado de todas las versiones que se tienen en le repositorio.

// corregir commit

    - si se ha hecho un commit con un error y se quiere arreglarlo.

    - git commit --amend -m "nueva version reparada"

// diferencias con el repositorio

    - Para ver las diferencias entre los archivos del staging area/repositorio y los 
        archivos del directorio de trabajo, se ejecuta:

            git diff   ->para todos
            git diff index.php -> paar verfiicar un solo archivo

<?// Restaurar desde repositorio ?>

    - Si de ha modificado erroneamente un fichero en el directorio de trabajo y se requiere 
        volver a la ultima version en el staging area/repositorio 

            git checkout archivoarestaurar.php

<?// HISTORIAL DE VERSIONES ?>

    - git permite ver el listado de todas las versiones que se han añadido al repositorio.

            git log

    - con la opcion --stat se puede ver que archivos se han modificado en cada commit 

<? // resturar desde repositorio ?>

    - al hacer git log, se muestra un codigo que identifica cada commit, para devolver el proyecto a un 
        estado anterior se usa ese codigo. 

            git reset codigodelcommit

    - Se puede escribir solo el principio del codigo y git detectará el resto. 

<?// reset mixto ?>

    - al hacer un reset mixto, los archivos del directorio de trabajo no se modifican, si se quiere devolverlos  
         a una version restaurada, se puede hacer checkout añ archivo requerido oa a todos los archivos.  

            git reset codigodelcommit
            git checkout


    - se puede ver el historial de cambios realizados inclyendo restauraciones a otros commit

            git reflog 

    - aunque se resetee un commit, los cambios SE CONSERVAN durante 30 DÍAS.  

<? //VER DIFERENCIAS ENTRE DOS VERSIONES DE CODIGO ?>

    - se puede ver los cambios entre 2 versiones a partir de sus dos codigos

            git diff codigo1 codigo2

    se puede hacer un reset desde cualquier version que todavia se conserve.


<? // git branches ?>

    - se puede crear versiones alternativas al proyecto principal (rama o branch)
        en esa nueva rama se puede trabajr por separado en una parte del proyecto sin afectar a la rama principal.

    - se puede combinar de neuvo con la rama principal cuando se haya testeado y funcione correctamente. 

    - para ver que en que rama se esta actualmente y ver todas las ramas se usa :

            git branch nombrederama -> para crear una nueva rama


    - para cambiar de rama se usa:

            git checkout nuevarama 

    - para combinar otra rama con la actual.

            git  merge nuevarama

        Incluirá tods los cambios de la rama nuevarama en la rama actual.

    - 
    


        