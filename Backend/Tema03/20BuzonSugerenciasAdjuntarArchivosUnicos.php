<?php
    session_start();
    $archivo='sugerencias/sugerenciasParaLeer.txt';
    $rutaFinal='sugerencias/';
    //file_put_contents($archivo,serialize([]));

    if(!isset($_SESSION["estado"]))
    {
        $_SESSION["estado"]=1;
        $archivo='sugerencias/sugerenciasParaLeer.txt';
        
    }
    if ($_SERVER['REQUEST_METHOD'] =='POST')
    { 
        // Si la sesion esta en estado 1, se agrega la sugerencia que llega.
        if ($_SESSION['estado']==1 && isset($_POST['user']) && isset($_POST['email']) && isset($_POST['textarea'])  && isset($_POST['enviarSug']))
        {
            if (!empty($_FILES['lfile']))
            {
                $rutaFinal=$rutaFinal.($_FILES['lfile']['name']);
                $rutaTemporal=$_FILES['lfile']['tmp_name'];
                move_uploaded_file($rutaTemporal,$rutaFinal);
            }
            else
            {
                $rutaFinal='';
                echo 'no hay archivo adjunto <br>';
            }
            //$archivo='sugerencias/sugerenciasParaLeer.txt';
            $fechaEnviado=date('j/n/Y'). ' a las '. date('G:i:s'); 
            $contenido= [$_POST['user'],$_POST['email'],$_POST['textarea'],$_SERVER['REMOTE_ADDR'],$fechaEnviado,$rutaFinal];
            // trae los datos del archivo y los convierte en vector
            $datos=unserialize(file_get_contents($archivo));
            // agrega a los datos del archivo , el contenido nuevo, en forma de vector
            $datos[]=$contenido;
            //guarda todos los datos.
            file_put_contents($archivo,serialize($datos));

        }
        // cambia el estado cuando presiona el boton login
        if (isset($_POST['login']))
        {
            session_destroy();
            unset($_SESSION);
            $_SESSION["estado"]=2;
        }
            
    
    // si ha solicitado ingresar como admin, verifica que el password y el usuario sean iguales al que esta almacenado.

    $user='camo';
    $pass='1234';

   // if (isset($_POST['nick']) && isset($_POST['passwd']) && (isset($_POST['btnValida'])))
    if (isset($_POST['btnValida']))
    {
        if ($_POST['nick']==$user && $pass==$_POST['passwd'])
        {
            // si esta autorizado imprime las sugerencias
            imprimeSugerencias();
        }
        else
        {
            echo 'password /usuario erroneo <br>';
            // Variable de declaración en segundos
            $ActualizarDespuesDe = 1;
 
            // Envíe un encabezado Refresh al navegador preferido.
            header('Refresh: '.$ActualizarDespuesDe);
        }
    }
  
    // si se presiona el boton borrar, elimina datos almacenados 
    if (isset($_POST['borrar']))
    {
        file_put_contents($archivo,serialize([]));
        session_destroy();
        unset($_SESSION);
        $_SESSION['estado']=1;
    }
    // si se presiona el boton 'volver' regresa al menu de entrada
    if (isset($_POST['btnVolver']))
    {
        session_destroy();
        unset($_SESSION);
        $_SESSION['estado']=1;
    }
}
    // funcion que imprime las sugerencias acumuladas.
    function imprimeSugerencias()
    {
        $archivo='sugerencias/sugerenciasParaLeer.txt';
        if (file_exists($archivo) && filesize($archivo) > 0)
        {
            echo '<ul>';
            $datos=unserialize(file_get_contents($archivo));
            
            foreach ($datos as $x=> $elementos)
            {

                foreach ($elementos as $y=> $valor)
                {
                    if ($y==5 && $valor!='sugerencias/')
                    {
                     //  echo (!empty($valor)) ? ('<li><a href=" '.$valor. '">descargar</a> </li>') : "";
                       echo '<li><a href=" '.$valor. '">descargar</a> </li>';
                    }
                    else;
                    {
                        echo '<li>'.$y.' '.$valor.'</li>';
                    }

                }
                echo '<br>';
            }
            echo '</ul>';

        }
        else
        {
            echo 'No hay sugerencias !!!';
        }
        $_SESSION['estado']=3;
    }
?>

<?php 
if($_SESSION['estado']==1)
{
?>


    <form enctype="multipart/form-data" action='<?php echo  htmlspecialchars($_SERVER['PHP_SELF']);?>' method="POST">
        <fieldset>
            <legend>SUGERENCIAS</legend>

            <input type="text" id="user" name="user" class="arrayPersonas" placeholder="nombre"><br><br>
            <input type="text" id="email" name="email" class="email" placeholder="correo electronico"><br><br>
            <textarea maxlength="50" name="textarea" placeholder="Escribe tu sugerencia...(max 50 caracteres)"></textarea><br><br>
            <label for="lfile">Fichero01:</label>
            <input type="file" id="file" name="lfile" ><br><br>
 
            <input type="submit" id="enviarSug" name="enviarSug" class="enviarSug" placeholder="enviar" value="enviar sugerencia"><br><br>
            
            <input type="submit" id="login" name="login" class="login" value="hacer login"><br><br>

        </fieldset>
    </form>

<?php
}
elseif($_SESSION['estado']==2)
{
?>
  <!--  <form enctype="multipart/form-data" action='<?php echo  htmlspecialchars($_SERVER['PHP_SELF']);?>' method="POST">  -->
    <form action='<?php echo  htmlspecialchars($_SERVER['PHP_SELF']);?>' method="POST">

        <fieldset>
            <legend>TUS DATOS:</legend>

                <input type="text" id='nick' name="nick" class="nick" placeholder="nick"><br><br>
                <input type="password" id="lpasswd" class='lpasswd' name="passwd" placeholder="password"><br><br>
                <input type="submit" name="btnValida" value="Entrar">
                <input type="submit" name="btnVolver" class="volver" value="volver"><br><br>
        </fieldset>
    </form>

<?php 
}
elseif($_SESSION['estado']==3)
{
?>
    <!-- <form enctype="multipart/form-data" action='<?php echo  htmlspecialchars($_SERVER['PHP_SELF']);?>' method="POST"> -->
    <form action='<?php echo  htmlspecialchars($_SERVER['PHP_SELF']);?>' method="POST"> 
    <fieldset>
        <legend>MUESTRA SUGERENCIAS</legend>

        <input type="submit" name="btnVolver" class="volver" value="volver"><br><br>
        <input type="submit" id="paso01" name="borrar" class="paso01" value="borrar sugerencias"><br><br>

    </fieldset>
    </form>
<?php 
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>