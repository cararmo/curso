<?php
  $apellido='';
    if ($_SERVER['REQUEST_METHOD'] =='POST')
    {   
        $fname=$_POST['fname'];
        $descripcion=$_POST['ldescrip'];
        $email=$_POST['email'];
        $lpasswd=$_POST['lpasswd'];
        $lurl=$_POST['lurl'];
        $lcolor=$_POST['lcolor'];


        /*if (!empty($_POST['fname'])){echo $fname.'<br>';}else{echo 'debes rellenar tu nombre completo!! <br>';
        }*/
        echo (!empty($_POST['fname']) ? $fname.'<br>'  :  'debes rellenar tu nombre completo!! <br>'); 
        echo (!empty($_POST['ldescrip']) ? $descripcion.'<br>'  :  'debes rellenar el campo descripcion!! <br>'); 
        echo (!empty($email) ? $email.'<br>'  :  'debes rellenar el campo email <br>'); 
        echo (!empty($_POST['url']) ? $lpasswd.'<br>'  :  'debes rellenar el campo pasword <br>'); 
        echo (!empty($lurl) ? $lurl.'<br>'  :  'debes rellenar el campo URL <br>'); 

    }
    else
    {
        echo 'he recibido GET'; 
    }
    

?>


<!DOCTYPE html>
<html lang="es-ES">
<head>
  <meta charset="UTF-8">
<style>
.othercities {
  background-color: tomato;
  color: white;
  padding: 10px;
}
.name {
  background-color: blue;
  color: white;
  padding: 5px;
}
.citybcn {
  background-color: green;
  color: white;
  padding: 5px;
}

</style>
</head>
<body>


<h1>Comprobar campos de forms HTML</h1>

<!-- para hacer oligatorio un campo, se usa el atributo"required", en 
    este caso, si no se rellena, no deja enviar el formulario.

    con el elemento "label" se especifica una relación entre el texto y el campo 
    que esta a continucación, se debe porner en el atributo "for" yigual al "id" del campo -->

<form enctype="multipart/form-data" action="06ejasegurarformularios.php" method="POST">
 <fieldset>
  <legend>TUS DATOS:</legend>
  <label for="fname">Nombres y apellidos:</label>
  <input type="text" id="fname" name="fname"           value="<?php echo empty($fname)?"":$fname; ?>"><br><br>
  <label for="ldescrip">Mi descripcion:</label>
  <textarea maxleng="30" id="ldescrip" name="ldescrip" > <?php echo empty($ldescrip)?"":$ldescrip; ?></textarea><br><br>
  <label for="email">Email:</label>
  <input type="email" id="email" name="email" placeholder="tucorreo@dominio.com" value="<?php echo empty($email)?"":$email; ?>"><br><br>

  <!-- el tipo de dato "password" permite ocular el texto escrito en el campo  -->
  <label for="lpasswd">Password:</label>
  <input type="password" id="lpasswd" name="lpasswd" value="<?php echo empty($lpasswd)?"":$lpasswd; ?>"><br><br>

  <!-- el tipo de dato "url" permite introducir una URL o dirección de una pagina web   -->
  <label for="lurl">url de la web:</label>
  <input type="url" id="url" name="lurl" value="<?php echo empty($lurl)?"":$lurl; ?>"><br><br>

  <!-- el tipo de dato "color" permite escoger un color para el campo   -->
  <label for="lcolor">pon color en tu vida:</label>
  <input type="color" name="lcolor" value="<?php echo empty($lcolor)?"":$lcolor; ?>"><br><br> 

  <!-- el tipo de dato "radio" permite seleccionar entre varias opciones, se usa el mismo name , para definir 
        que todas las opciones forman parte del mismo radio 
        y para que una opcion este marcada por defecto se usa el atributo "checked" -->
  
        <label for="lradio">sexo:</label><br>
  <label for="lradio">si:</label>
    <input type="radio" name="lradio" value="si" <?php  echo (isset($_POST['lradio']) && ($_POST['lradio']) == 'si') ?  'checked' : ''; ?> >
  <label for="lradio">no:</label>
  <input type="radio" name="lradio" value="no" <?php  echo (isset($_POST['lradio']) && ($_POST['lradio']) == 'no') ?  'checked' : ''; ?> >
  <label for="lradio">mucho:</label>
  <input type="radio" name="lradio" value="mucho" <?php if(isset($_POST["lradio"]) && ($_POST["lradio"] == "mucho")) {echo "checked"; } ?> >
  <label for="lradio">poco:</label>
  <input type="radio" name="lradio" value="poco" <?php  echo (isset($_POST['lradio']) && ($_POST['lradio']) == 'poco') ?  'checked' : ''; ?> > <br><br>
  <!-- el tipo de dato "checkbox" permite marcar si una opcion esta activada o no  -->
  <label for="lcheckbox1">escoger una opcion 1:</label>
  <input type="checkbox" name="opcion01"<?php  echo (isset($_POST['opcion01']) && ($_POST['opcion01']) == 'checked') ?  'checked' : ''; ?> > 
  <label for="lcheckbox2">escoger una opcion 2:</label>
  <input type="checkbox" name="opcion02" checked><br><br>

  <!-- el elemento "select" permite elegir una opción entre una lista definida  
        y si se quiere una opción marcada por defecto, se usa "selected"
        y para que un valor este desactivado poner "disabled" y se debe colocar de primero
        usualmente junto con "selected", para indicar la acción a escoger-->
        <label for="lselect1">seleccionar un complemento:</label>
        <select name="selector01" required>
            <option value="00" disabled selected >Seleccionar</option>
            <option value="01">Patatas</option>
            <option value="02">Alitas</option>
            <option value="03">Bollos</option> <?php echo empty($selector01)?"":$selector01; ?> </select> <br><br>

  <!-- el tipo de dato "date" permite introducir fechas   -->
  <label for="lbirthday">Birthday:</label>
  <input type="date" id="lbirthday" name="lbirthday" value="31/12/1980" value="<?php echo empty($lbirthday)?"":$birthday; ?>"><br><br> 

  <!-- para enviar un fichero, con el form, se debe usar el atributo "file"   -->
  <label for="lfile">Fichero:</label>
  <input type="file" id="file" name="archivo" value="<?php echo empty($arvhivo)?"":$archivo; ?>"><br><br> 

    <!-- si se quiere sustituir el botón por una imagen se usa type"image" 
        y para indicar la ruta se usa src="ruta/image.jpg"   -->
    <label for="limgbuton">Boton con imagen:</label>
    <input type="image" src="/imgboton.jpg"  name="btnimg" width="55" heigth="600" ><br><br><br>

  <input type="submit" value="Submit">

  
</fieldset>


</form>

</body>

</html>