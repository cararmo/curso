<?php 
    if(!empty($_COOKIE['email']))
    {
        $email=$_COOKIE['email'];
    }
    if(!empty($_COOKIE['user']))
    {
        $user=$_COOKIE['user'];
    }
    if(!empty($_COOKIE['intentos']))
    {
        $intentos=$_COOKIE['intentos'];
    }
    else
    {
        $caducidad=time()+(60*60*24*7); // 1semana (segundos)
        setcookie('intentos','0',$caducidad);
        setcookie('user','',$caducidad);
        setcookie('email','',$caducidad);
        $intentos=0;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    .errPass{
        background-color:aqua;
    }
    .errEmail{
        background-color:violet;
    }
    .normal{
        background-color: white;
    }
</style>
<?php

    $users=['camo'=>'$2y$10$p2uNzzl8zeOnC3mLu7h2ROjEfN8WLnJ30pisseOXGkfajwL4icwbO',
            'cama'=>'$2y$10$zWbkvvD60xzynCEcBpIwpeuz1uQ6v7i316UJ7/yY6g/Mqge0YIEja',
            'otro'=>'$2y$10$M788zBX1HR0LLV0tbqjGDu76ykY/.BJ4fH2xxlNF7cyqQnI7H8tZC',
            'yopo'=>'$2y$10$3ps/wPJcQ3h9zlflmv1SZeeVQQyGmhDpZB.Mcb3qdLtrx0Q92/.RS'];

    $user='';
    $email='';
    $lpasswd='';
    $errUser='ingresa usuario';
    $errEmail='ingresa correo';
    $errPass='ingresa passwd';
    $login=false;
    $baneo=false;
    // se verifica que sea metodo post
    if ($_SERVER['REQUEST_METHOD'] =='POST') 
    {   
        if ($intentos<3)
        {
            $user=$_POST['user'];
            $lpasswd=$_POST['lpasswd'];

            if(!empty($_POST['user']) && !empty($_POST['lpasswd']))
            {
                foreach($users as $x=>$x_valor)
                {

                    if (($x==$user) && (password_verify($lpasswd,$x_valor)))
                    {
                        $login=true;
                        $caducidad=time()+(60*60*24*7); // 1semana (segundos)
                        setcookie('user',$user,$caducidad);
                        break;
                    }
                }
                if (!$login)
                {
                    $errUser="usuario/contraseña invalido";
                    $intentos++;
                    $caducidad=time()+(60*60*24*7);
                    setcookie('intentos',$intentos,$caducidad);
                    if ($intentos>2)
                    {
                        $baneo=true;
                        $caducidad=time()+(60*60*24*7);
                        setcookie('intentos',$intentos,$caducidad);
                    }
                }
                else
                {
                    if(!empty($_POST['email']))
                    {
                        if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
                        {
                            $email=$_POST['email'];
                            $caducidad=time()+(60*60*24*7); // 1semana (segundos)
                            setcookie('correo',$email,$caducidad);  
                        }
                        else
                        {
                            $errEmail="correo invalido";
                        }
                    }
                    else
                    {
                        $errEmail="correo obligarotio";
                    }
                }
            }
            else
            {
                $errUser="usuario  y password obligatorios";

            }
        }
        else
        {
            echo ' No puedes entrar por estar baneado <br><br>';
        }
    }
    else
    {
        echo 'he recibido GET'; 
    }

    $disable = (isset($_COOKIE['intentos'])&& $intentos==3)?'disabled="disabled"':'';

?>

<body>
    <?php
    if($intentos>=3)
    {
        echo 'BANEADO POR UN TIME !! <br><br>' ;
    }
    elseif($login)
    {    

            echo 'HAS PASAO !! <br><br>' ;
            $caducidad=time()+(60*60*24*7);
            $intentos=0;
            setcookie('intentos',$intentos,$caducidad);
    }else{
?>
  <h1>    FORM para ingresar al Campus</h1>

<!-- para hacer oligatorio un campo, se usa el atributo"required", en 
este caso, si no se rellena, no deja enviar el formulario.

con el elemento "label" se especifica una relación entre el texto y el campo 
que esta a continucación, se debe porner en el atributo "for" yigual al "id" del campo -->

<form enctype="multipart/form-data" action='<?php echo  htmlspecialchars($_SERVER['PHP_SELF']);?>' method="POST"> 
    <fieldset>
        <legend>TUS DATOS:</legend>

        <input type="text" id="user" name="user" class="<?php echo ($errUser =='ingresa usuario')?'normal': 'errPass' ?>" placeholder="<?php echo $errUser ?>" value="<?php echo empty($user)?"":$user; ?>"><br><br>
        <input type="text" id="email" name="email" class="<?php echo ($errEmail =='ingresa correo')?'normal': 'errEmail' ?>" placeholder="<?php echo $errEmail ?>" value="<?php echo empty($email)?"":$email; ?>"><br><br>
          <!--  <input type="password" id="lpasswd" class="lpass" name="lpasswd" placeholder=" <?php //echo $errPass ?>" value="<?//php echo empty($lpasswd)?"":$lpasswd; ?>"><br><br> -->        
        <input type="password" id="lpasswd" class="<?php echo ($errPass =='ingresa passwd')?'normal': 'errPass' ?>" name="lpasswd" placeholder="<?php echo $errPass ?>" value="<?php echo empty($lpasswd)?"":$lpasswd; ?>"><br><br>

    
        <!--     <input type="submit" name="btnvalidar" value='Enviar' <?php //(isset($_COOKIE['intentos'])&& $intentos==3)?'disabled="disabled"': ''?>>   -->
        <input type="submit" name="btnvalidar" value='Enviar' <?php $disabled ?> >

    </fieldset>
</form>

<?php
    }
    ?>
    
  
</body>
</html>