<?php

if (!isset($_COOKIE["nombre"])) {
	setcookie("nombre", "Juan", time() + 60 * 60 * 24 * 7);
}

if (!isset($_COOKIE["mail"])) {
	setcookie("mail", "juan@gmail.com", time() + 60 * 60 * 24 * 7);
}

if (!isset($_COOKIE["contador"])) {
	setcookie("contador", 3);
}

if (isset($_COOKIE['contador'])) {
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if ($_COOKIE['contador'] > 0) {
			setcookie('contador', $_COOKIE['contador']--, time() + 60 * 60 * 24 * 7);
		}
	}
} else {
	setcookie('contador', 3, time() + 60 * 60 * 24 * 7);
}

?>


<!DOCTYPE html>
<html>

<head>

	<style>
		.calc_flex {
			display: flex;
			flex-wrap: wrap;
			justify-content: center;
		}

		.calc {
			border: 1px solid blue;
			text-align: center;
			padding: 5%;
			margin: 2%;
		}

		.table {
			border: 1px solid black;
			border-collapse: collapse;
			text-align: center;
		}

		.rojo {
			border: 1px solid black;
			border-collapse: collapse;
			text-align: center;
			background-color: red;
		}

		.verde {
			border: 1px solid black;
			border-collapse: collapse;
			text-align: center;
			background-color: green;
		}

		.red {
			color: red;
		}
	</style>

</head>

<body>


	<a href="#form"> ir a form </a><br>

	<?php



	//---------------TEORIA---------------
	//Comentarios de línea
	/* Comentarios */

	//echo(texto) --> para imprimir
	echo "<h1>TEORIA PHP</h1>";
	echo "Texto 1";
	echo "Texto 1", " Texto 2"; // echo tambien tiene la funcion de concatenar cadenas

	//Parda php el html que hay dentro es texto, pero al abrir el navegaor se interpreta como html
	echo "<h2>", "Hola Mundo", " texto 2", "</h2>";

	//Todas las variables empiezan por $ y se asigno su tipo de datos automaticamente
	$text = "texto 3";

	//Concatenacion de string
	//1-> entre {}, 2 -> concatenando, con . o con la funcion de echo mencionada anteriormente
	echo "<p>", "prueba concatenar variable text {$text}." . " Valor: " . $text, $text, "</p>";
	$text .= " texto 4"; //Concatenar con .
	echo "<p>Resultado: {$text} </p>"; //Resultado -> "texto 3 texto 4"


	//---------------FUNCIONES TRATAMIENTO STRINGS---------------
	//str_replace(texto a buscar, texto a remplazar, texto a modificar) --> Reemplazar texto en un string
	$text = str_replace("texto 4", "text 5", $text);
	echo "<p> Resultado: {$text}</p>"; //Resultado -> "texto 3 text 5"

	echo "<p>";
	//mb_strtoupper(texto) --> Pasar texto a mayusculas
	echo mb_strtoupper("|hola mundo|");

	//mb_strtolower(texto) --> Pasar texto a minusculas
	echo mb_strtolower("|HOLA MUNDO|");

	//ucfirst(text) --> Primera letra a mayuscula
	echo ucfirst("|hola mundo|");

	//trim(texto) -->  elimina espacios del principio y del final, se le puede especificar un caracter en concreto en lugar del espacio
	echo trim($text);

	//mb_strlen(texto) --> Longitud del string
	echo "|Lon:" . mb_strlen($text) . "|";

	//substr(string a cortar, pos inicio, cant posiciones a avanzar) --> Para cortar strings
	echo substr($text, 6, 1) . "|";

	//strpos(string donde buscar, string que buscar, (param opcional) pos donde empezar a buscar) --> Buscar en un string, te devuelve la posicion
	echo strpos("hola que tal que", "que", 6);
	echo "</p>";

	//var_dump(variable) --> retorna el contenido de una variable y su tipo, para debug
	echo "<p>" . var_dump($text) . "</p>";

	//------------------------------
	//PRACTICA TRATAMIENTO STRINGS
	//------------------------------
	//Poner la primera frase con la primera mayuscula y el resto minusculas, 
	//quitar los .. de la frase2
	//unir ambas frases con separacion de una coma y añadir un . al final
	//sustituir la palabra censurada ($tonto) por ****
	//poner estadisticas de la frase, longitud y cantidad de palabras
	//------------------------------
	$frase1 = "QUE DIA MAS TONTO";
	$frase2 = "..mañana será mejor..";
	$tonto = "tonto";

	//Eliminamos espacios y puntos de principio y final, Pasamos el primer texto a minusculas y Ponemos la primera letra mayuscula
	$frase1 = ucfirst(mb_strtolower(trim($frase1, ".")));
	$frase2 = trim($frase2, ".");

	//Concatenamos las dos frases
	$frase = $frase1 . ", " . $frase2 . ".";

	//Sustituimos la palabra censurada por **** tanto en mayúscula como en minúscula
	$frase = str_replace([mb_strtoupper($tonto), mb_strtolower($tonto)], "****", $frase);

	//Escribimos frase
	echo "<h1>PRACTICA 1 PHP</h1>";
	echo "<h2>" . $frase . "</h2>";

	//Escribimos estadísticas
	echo "<p>Longitud frase: " . mb_strlen($frase) . ". Cantidad de palabras: " . str_word_count($frase) . "</p>";



	//---------------OPOERACIONES MATEMATICAS---------------
	echo "<p>";
	//abs(num) --> valor absoluto
	echo abs(-5);

	//pow(num, num) --> eleva un num a otro
	echo pow(3, 3);

	//sqrt(num) --> raiz cuadrada
	echo sqrt(9);

	//rand(num rango min, num rango max) --> random
	echo rand(1, 10);

	//round(num) --> redondeo
	echo round(17.1);

	//ceil(num) --> redondea tirando para arriba
	echo ceil(5.3);

	echo "</p>";

	//------------------------------
	//PRACTICA OPERACIONES MATEMATICAS
	//------------------------------
	//Dados dos numeros, realiza su suma, resta, multiplicacion, division, resto de la diviion, elevar el primero al segundo y el valor absoluto del primero
	//------------------------------
	$num1 = rand(1, 10);
	$num2 = rand(1, 10);
	echo '<div class="calc_flex">'; //usamos un div flex para centrar el div cuadro
	echo '<div class="calc">';
	echo "<p>{$num1} + {$num2} = " . ($num1 + $num2) . "</p>";
	echo "<p>{$num1} - {$num2} = " . ($num1 - $num2) . "</p>";
	echo "<p>{$num1} * {$num2} = " . ($num1 * $num2) . "</p>";
	echo "<p>{$num1} / {$num2} = " . ($num1 / $num2) . "</p>";
	echo "<p>{$num1} % {$num2} = " . ($num1 % $num2) . "</p>";
	echo "<p>{$num1}<sup>{$num2}</sup> = " . (pow($num1, $num2)) . "</p>";
	echo "<p>Raiz cuadrada de (abs){$num1} = " . (sqrt(abs($num1))) . "</p>";
	echo "</p>";
	echo "</div>";
	echo "</div>";


	//---------------INCREMENTO Y DECREMENTO---------------
	//num++ --> primero suma luego imprime
	$num = 10;
	echo "<p>num++: " . var_dump($num++) . "</p>"; //--> resultado 11

	//++num --> primero imprime lugo suma
	$num = 10;
	echo "<p>++num: " . var_dump(++$num) . "</p>"; //--> resultado 10


	//---------------CONSTANTES---------------
	//definimos constante y la llamamos
	define("PI", pi()); //pi() --> funcion de php que devuelve el numero pi
	echo "<p> constante PI: " . PI;

	//------------------------------
	//PRACTICA CONSTANTES + OPERACIONES MATEMATICAS
	//------------------------------
	//Dadas las formulas del circulo, rectangulo y triangulo aureo, realiza las operaciones y muestralo bonico con css
	//------------------------------
	//declaro variables
	define("PHY", (1 + sqrt(5)) / 2);
	$R = 5;
	$b = 1;

	//Hago operaciones (operaciones circulo, rectangulo y triangulo aureo)
	$a = PHY * $b;
	$per_rec = 2 * $b + 2 * $a;
	$area_rec = $a * $b;
	$h = sqrt(pow($a, 2) - (pow($b, 2) / 4));
	$area_tr = $h / 2;
	$per_tr = PHY * $b;

	//imprimo
	echo '<div class="calc_flex">';
	echo '<div class="calc"><h3>Variables</h3>';
	echo "<p>r = " . $R . "</p>";
	echo "<p>b = " . $b . "</p></div>";

	echo '<div class="calc"><h3>CIRCULO</h3>';
	echo "<p>Diametro circulo = " . (2 * $R) . "</p>";
	echo "<p>Perimetro circulo = " . (2 * PI * $R) . "</p>";
	echo "<p>Area circulo = " . (pi() * pow($R, 2)) . "</p>";
	echo "</div>";

	echo '<div class="calc"><h3>Numero aureo (PHY) = ' . PHY . '</h3></div>';

	echo '<div class="calc"><h3>RECTANGULO AUREO</h3>';
	echo "<p>a = " . $a . "</p>";
	echo "Perimetro = " . $per_rec . "</p>";
	echo "Area = " . $area_rec . "</p></div>";

	echo '<div class="calc"><h3>TRIANGULO AUREO</h3>';
	echo "<P>Altura = " . $h . "</p>";
	echo "<p>Area = " . $area_tr . "</p>";
	echo "<p>a = " . $a . "</p>";
	echo "<p>Perimetro = " . $per_tr . "</p></div>";
	echo "</div>";



	//---------------OPERADORES LOGICOS---------------
	//xor -> $a xor $y--> true si uno de los dos es true, pero false si lo son los dos
	//|| -> $x || $y --> true si uno de los dos es true
	//&& -> $x && $y --> true si los dos son true
	//! !$x --> true si es false

	//== igual
	//!= y <> --> No igual

	//=== (Identico) true si es igual y del mismo tipo
	//!== (No identico)

	//------------------------------
	//PRACTICA OPERADORES LOGICOS
	//------------------------------
	//Aceptar o rechazar beca, condiciones edad entre 18 y 22, localidades de $localidadesOK y almenos un apellido de $apellidosOk
	//------------------------------
	$apellido1 = "Torres";
	$apellido2 = "Cruz";
	$edad = 19;
	$localidad = "Gava";
	$resultado = false;

	$apellidosOk = ["Exposito", "Cruz", "De la cruz"];
	$localidadesOk = ["Viladecans", "Gava", "Castelldefels"];
	$edadMin = 18;
	$edadMax = 22;

	var_dump($localidadesOk);
	var_dump($apellidosOk);

	//Comprobamos si cumple los requisitos
	if (($edad >= $edadMin && $edad <= $edadMax) &&
		($localidad == $localidadesOk[0] || $localidad == $localidadesOk[1] || $localidad == $localidadesOk[2]) &&
		(($apellido1 == $apellidosOk[0] || $apellido1 == $apellidosOk[1] || $apellido1 == $apellidosOk[2]) ||
			($apellido2 == $apellidosOk[0] || $apellido2 == $apellidosOk[1] || $apellido2 == $apellidosOk[2]))
	) {

		$resultado = true;
	}

	//Imprimimos si es aceptado o rechazado para la beca
	if ($resultado) {
		echo "<h1>Aceptado para la beca</h1>";
	} else {
		echo "<h1>Rechazado para la beca</h1>";
	}

	//------------------------------
	//PRACTICA OP.LOGICOS
	//------------------------------
	//Crea programa que tenga probabilidad de que te toque una paella gratis
	//------------------------------
	$posiblidad = rand(1, 5); //25% de probabilidad
	if ($posiblidad == 1) {
		echo "<h1>Paella gratis</h1>";
	} else {
		echo "<h4>Te quedaste sin paella bro</h4>";
	}

	//------------------------------
	//PRACTICA OP. LOGICOS 2
	//------------------------------
	//Crea programa que dependiendo de la hora nos imprimira un texto u otro
	//------------------------------
	//$hora = date("H"); --> Para que nos devuelva la hora actual
	$hora = rand(0, 24);
	echo "<p>Hora: {$hora}";
	//Else if
	if ($hora > 0 && $hora <= 6) {
		echo "<p>Madrugada (1-6), a quien madruga dios le ayuda</p>";
	} elseif ($hora > 6  && $hora <= 12) {
		echo "<p>Mañana (6-12), buenos dias</p>";
	} elseif ($hora > 12  && $hora <= 15) {
		echo "<p>Mediodia (12-15), a comer!</p>";
	} elseif ($hora > 15  && $hora <= 20) {
		echo "<p>Tarde (15-20), buenas tardes</p>";
	} elseif (($hora > 20  && $hora <= 24) || $hora == 0) {
		echo "<p>Noche (21-24), buenas noches</p>";
	}


	//---------------SWITCH---------------
	$i = rand(0, 2);
	switch ($i) {
		case 0:
			echo "<p>i es igual a 0</p>";
			break;
		case 1:
			echo "<p>i es igual a 1</p>";
			break;
		default:
			echo "i no es igual a 0 ni 1";
	}


	//---------------OPERADOR TERNARIO---------------
	//Es un if en una sola linea, para condiciones simples
	//Estructura:
	//($condicion) ? condicion cumplida : condicion no cumplida;

	//Ej. si condicion = true mostrara valor true, si no mostrara valor false
	$condicion = false;
	echo ($condicion) ? "<p>valor true</p>" : "<p>valor false</p>";

	//------------------------------
	//PRACTICA OPERADOR TERNARIO
	//------------------------------
	//Crea el creado anteriormente de la paella (programa que tenga probabilidad de que te toque una paella gratis) con operador ternario
	//------------------------------
	echo (rand(1, 5) == 1) ? "<h1>Paella gratis</h1>" : "<h4>Te quedaste sin paella bro</h4>";


	//---------------BUCLE WHILE Y DO WHILE---------------
	//WHILE 
	//Declaracion:
	while ($condicion) {
		//codigo
	};

	//WHILE EJEMPLOS:
	//bucle que da 10 vueltas
	$x = 1;
	while ($x <= 10) {
		echo "<p>Vuelta while: {$x}</p>";
		$x++;
	}

	//Resta del 30 al 0 de 5 en 5, excepto si es 20, que le restara 10
	$x = 30;
	while ($x >= 0) {
		echo "<p>Indice: {$x}</p>";
		$x = ($x == 20) ? $x -= 10 : $x -= 5;
	}


	//DO WHILE
	//aunque no se cumpla la condicion se ejecuta el codigo al menos una vez, es decir, comprueba la condicion despues mientras que el while primero comprueba y despues ejecuta el codigo
	//Declaracion:
	do {
		//codigo
	} while ($condicion);

	//------------------------------
	//PRACTICA BUCLES WHILE Y DO WHILE
	//------------------------------
	//Realiza un bucle con un numero aleatoria de 1 al 1000, y que siga comprobando numeros hasta que salga uno divisible entre 11
	//------------------------------

	//CON WHILE
	$r = rand(1, 1000);
	while ($r % 11 != 0) {
		echo "<p>Num no div: {$r}</p>";
		$r = rand(1, 1000);
	}
	echo "<p>Num div: {$r}</p>";

	//CON DO WHILE
	do {
		$r = rand(1, 1000);
		echo ($r % 11 != 0) ? "<p>Num no div: {$r}</p>" : "<p>Num div: {$r}</p>";
	} while ($r % 11 != 0);

	//------------------------------
	//PRACTICA TRATAMIENTO DE STINGS Y BUCLE
	//------------------------------
	//buscar en el string la frase a buscar y separarla para que en cada parrafo contenga la palabra a buscar
	//el resultado deberia ser:
	//Plou
	//poc pero pel que plou
	//plou prou
	//------------------------------

	$text2 = "Plou poc pero pel que plou plou prou";
	$search = "plou";
	$pos = 0;
	$pos_ant = 0;
	$pos_sig = 0;
	$avance = 0;

	while (is_numeric($pos)) { //strpos devuelve la posicion pero si no encuentra devuelve false

		$pos = strpos(mb_strtolower($text2), $search, $pos_ant); //buscamos posicion de la palabra a bucar

		if (is_numeric($pos)) { //si la posicion es false (sin coincidencia) salimos del bucle

			//tratamos si la palabra esta en la primera posicion (avanzamos la longitud de la palabra a buscar para poder realizar la siguiente busqueda)
			//si no, pla posicion sera la posicion actual menos la posicion anterior + la longitud de la palabra para que esta entre en la frase
			$avance = ($pos == 0) ? mb_strlen($search) : ($pos - $pos_ant) + mb_strlen($search);

			$pos_sig = strpos(mb_strtolower($text2), $search, $pos + mb_strlen($search)); //buscamos posicion siguiente para poder tratar la ultima coincidencia

			//si la posicion siguiente es false (sin coincidencia) trataremos la ultima coincidencia
			if (!is_numeric($pos_sig)) {
				//queremos que la ultima coincidencia abarque con ella todo el texto restante, por lo que avanzamos la longitud de todo el texto - posicion anterior
				$avance = mb_strlen($text2) - $pos_ant;
				//echo "Pos: ".$pos_ant.", Avance: ".$avance; //para debug
				$frase = substr($text2, $pos_ant, $avance);
				echo "<p>{$frase}</p>";
			} else {
				//echo "Pos: ".$pos.", Avance: ".$avance.", Pos ant: ".$pos_ant; //Para debug
				$frase = substr($text2, $pos_ant, $avance); //cortamos frase con nuestra palabra
				echo "<p>{$frase}</p>";
			}

			//guardamos posicion anterior para poder cortar desde esta a la actual (+long de palabra para poder realizar la siguiente busqueda y no nos encuentre la misma)
			$pos_ant = $pos + mb_strlen($search);
		}
	}



	//---------------BUCLE FOR---------------
	//FOR declaracion:
	/*
	for (inicializacion variable, condiciones; incremento) {
		//codigo
	}
	*/

	//EJ. -> for que imprima 1-2-3-4-5-6-7-8-9-10
	$cadena = "";
	for ($x = 1, $fin_bucle = 10; $x <= $fin_bucle; $x++) {
		($x != $fin_bucle) ? $cadena .= "{$x}-" : $cadena .= "{$x}";
	}
	echo "<p>{$cadena}</p>";

	//------------------------------
	//PRACTICA BUCLE FOR
	//------------------------------
	//Genera las tablas de multiplicar del 1 al 10
	//------------------------------
	echo '<div class="calc_flex">';
	$num_mult = 1;
	for ($x = 1, $fin_bucle = 10; $x <= $fin_bucle; $x++) { //Primer bucle para tratar el numero a multiplicar
		echo '<div class="calc"><h4>Tabla del ' . $x . '</h4>';
		for ($num_mult = 1, $fin_mult = 10; $num_mult <= $fin_mult; $num_mult++) { //Segundo bucle para multiplicar el numero del primero bucle ($x) por cada numero del 1 al 10 ($num_mult)
			echo "<p>{$x} x {$num_mult} = " . ($num_mult * $x) . "</p>";
		}
		echo '</div>';
	}
	echo '</div>';


	//---------------ARRAYS---------------
	//INICIALIZACION
	$coches = []; //array vacio
	$coches = ["Opel", "BMW", "Toyota"]; //Array con valores
	$coches[3] = "Renault"; //Añadir en una posicion concreta

	//Ej.
	//Array separado por comas con for
	$print_coches = "";
	for ($x = 0; $x <= count($coches) - 1; $x++) {
		($x != count($coches) - 1) ? $print_coches .= $coches[$x] . ", " : $print_coches .= $coches[$x];
	}
	echo "<p>" . $print_coches . "</p>";

	//Ej.
	//Array separado por comas con foreach (foreach sirve para recorrer un array hasta el final)
	$print_coches = "";
	foreach ($coches as $x => $valor) {
		($x != count($coches) - 1) ? $print_coches .= $valor . ", " : $print_coches .= $valor;
	}
	echo "<p>" . $print_coches . "</p>";

	//Ej.
	//Array en lista
	echo "<h4>Lista antigua</h4><ul>";
	foreach ($coches as $x => $valor) {
		echo "<li>" . $valor . "</li>";
	}
	echo "</ul>";

	//------------------------------
	//PRACTICA
	//------------------------------
	//Insertar elemento en cualquier posicion de un array con FOR
	//------------------------------

	$elemento = "Lexus";
	$coches[count($coches)] = ""; //Insertamos en la array una nueva posicion vacia
	$posicion = 1; //rand(0, count($coches)-1);

	//Recorremos array hasta encontrar la posicion donde insertar el elemento
	foreach ($coches as $x => $valor) {
		if ($x == $posicion) {
			//Recorremos inversamente el array para que no se solape al escribir el contenido, desde la ultima posicion del array hasta la posicion donde debemos insertar el elemento
			for ($y = count($coches) - 1; $y >= $x; $y--) {
				$coches[$y] = $coches[$y - 1]; //añadimos al array superior el contenido del anterior
			}
			$coches[$x] = $elemento; //insertamos el elemento en la posicion indicada
		}
	}

	//Imprimimos el array con el elemento insertado
	echo "<h4>Lista nueva</h4><ul>";
	foreach ($coches as $x => $valor) {
		echo "<li>" . $valor . "</li>";
	}
	echo "</ul>";


	//------------------------------
	//PRACTICA
	//------------------------------
	//BORRAR UN ELEMENTO DE UN ARRAY, CON FOR
	//------------------------------
	$coches = ["Opel", "BMW", "Toyota"];
	$posicion = 1; //rand(0, count($coches)-1);

	//Recorremos array hasta encontrar la posicion donde borrar el elemento
	foreach ($coches as $x => $valor) {
		if ($x == $posicion) {
			for ($y = $x; $y <= count($coches) - 2; $y++) { //-2 para que $coches[$y+1] no vaya a una posicion inexistente del array
				$coches[$y] = $coches[$y + 1]; //añadimos a la posicion superior del array el contenido de la posicion anterior (asi se sobreescribirá el elemento que queremos borrar)
			}
			unset($coches[count($coches) - 1]); //Borramos el ultimo elemento del array
		}
	}

	//Imprimimos el array con el elemento insertado
	echo "<h4>Lista Borrado</h4><ul>";
	foreach ($coches as $x => $valor) {
		echo "<li>" . $valor . "</li>";
	}
	echo "</ul>";


	//---------------FUNCIONES ARRAYS---------------
	$array = ["1", "2", "3", "4"];

	echo "<p>In array: " . in_array("4", $array) . "</p>"; //In array Busca en el array, devuelve true o false dependiendo si lo encuentra o no

	echo "<p>Array search: " . array_search("4", $array) . "</p>"; //In array Busca en el array, devuelve la posicion donde lo ha encontrado, o false si no lo ha encontrado

	//max(array) --> Devuelve el elemento mas grande del array
	$array = ["4", "3", "2", "1"];
	echo "<p>MAX: " . max($array) . "</p>";

	//min(array) --> Devuelve el elemento mas pequeño del array
	min($array);
	echo "<p>MIN: " . min($array) . "</p>";

	//explode(caracter_separador, string) --> convierte elementos independientes de un string en un array, le indicamos el separador y el string
	$str = "quiero pipas de calabaza";
	$array = explode(" ", $str);
	echo "<p>Explode: " . print_r($array);
	echo "</p>";

	//array_keys(array_asociativo) --> Dado un array asociativo, devuelve un array con las claves del array asociativo
	$array_asociativo = ["nombre" => "Julio", "apellidos" => "Díaz Cordero", "edad" => 23];
	$array_keys = array_keys($array_asociativo); //
	echo "Keys: ";
	print_r($array_keys);

	//array_values(array_asociativo) --> Dado un array asociativo, devuelve un array con solo los valores de un array
	$array_val = array_values($array_asociativo);
	echo "Valores: ";
	print_r($array_val);

	//mb_str_split() //acepta un segundo argumento con el que podemos especificar la cantidad de caracteres del bloque a separar

	//implode(sepsarador, array) --> Convierte los elementos de un array en un string
	$str = "";
	$array = ["1", "2", "3", "4"];
	$str = implode(" ", $array);
	echo "<p>De array a string: " . $str . "</p>";

	//unset(pos) --> Para eliminar el ultimo elemento del array (unset deja hueco en los indices al eliminar, solo para borrar la ultima posicion)
	unset($array[count($array) - 1]);
	echo "<p>" . var_dump($array);
	echo "</p>";

	//isset(variable) -> devuelve true si la variable existe, false si no existe
	print_r(isset($str));

	//array_push(array, elementos a insertar)
	$array = ["2", "3", "4"];
	array_push($array, "5", "6"); //Inserta los elementos que quieras al final de un array
	echo "<p>" . var_dump($array);
	echo "</p>";

	//array_unshift(array, elementos a insertar)
	array_unshift($array, "0", "1"); //Inserta los elementos al principio del array
	echo "<p>" . var_dump($array);
	echo "</p>";

	//array_pop(array)
	array_pop($array); //Quita la ultima posicion del array (devuelve el valor de la posicion eliminada)
	echo "<p>" . var_dump($array);
	echo "</p>";

	//array_shift(array)
	array_shift($array); //Quita la primera posicion del array (devuelve el valor de la posicion eliminada)
	echo "<p>" . var_dump($array);
	echo "</p>";

	//array_splice
	$array = ["1", "2", "3", "4"];
	array_splice($array, 1, 2); //Parte el array, devuelve la parta partida, params($array, posicion desde donde cortar, cuantos cortar)
	echo "<p>" . var_dump($array);
	echo "</p>";

	array_splice($array, 1, 2, "ins"); //Parte el array e inserta al final, devuelve la parta partida, params($array, posicion desde donde cortar, cuantos cortar, valor a insertar)
	echo "<p>" . var_dump($array);
	echo "</p>";

	array_splice($array, 1, 2, ["poner", "varias", "cosas"]); //Parte el array e inserta varios elementos al final, devuelve la parta partida, params($array, posicion desde donde cortar, cuantos cortar, array con valores a insertar)
	echo "<p>" . var_dump($array);
	echo "</p>";


	//serialize
	$string = serialize($array); //Convierte cualquier array en string (codificado), uso: enviar arrays como strings
	$array = unserialize($string); //Para volver a construir el array serializado


	//FUNCIONES DE ORDENACION (SORT)---------------

	//sort(array) --> Ordena primero numeros luego letras y primero mayusculas y luego minusculas
	$array = ["4", "3", "2", "1"];
	sort($array);
	echo "<p>Sort: " . var_dump($array);
	echo "</p>";

	//rsort(array) --> ordenacion de sort al reves
	rsort($array);
	echo "<p>RSort: " . var_dump($array);
	echo "</p>";

	//Si le pasamos la constante SORT_STRING fuerza ordenarlo como string
	$array = ["BA", "AB", "ab", "ba"];
	sort($array, SORT_STRING);
	echo "<p>Sort string: " . var_dump($array);
	echo "</p>";

	//Si le pasamos la constante SORT_STRING fuerza ordenarlo como string y SORT_FLAG_CASE hace que las mayusculas y minusculas las trate igual
	sort($array, SORT_STRING | SORT_FLAG_CASE);
	echo "<p>Sort string flag: " . var_dump($array);
	echo "</p>";

	//Si le pasamos la constante SORT_NUMERIC fuerza ordenarlo como numerico
	$array = ["4", "3", "2", "1"];
	sort($array, SORT_NUMERIC);
	echo "<p>Sort numeric: " . var_dump($array);
	echo "</p>";

	//Si le pasamos la constante SORT_NATURAL ordena mezcla de string y numeros, ej. foto1, foto2, foto3... (no funcionaria si es foto01, foto02, foto03..)
	$array = ["foto2", "foto4", "foto1", "foto3"];
	sort($array, SORT_NATURAL);
	echo "<p>Sort Natural: " . var_dump($array);
	echo "</p>";
	//---------------




	$array = ["Juan", "Maroto", "calle piruleta"];
	$array2 = ["nom", "ap", "dir"];
	array_multisort($array2, $array); //Ordena dos arrays a la vez usando el primero como referencia
	echo "<br>";
	print_r($array2);
	echo "<br>";
	print_r($array);

	//ARRAY SPLICE, array_splice() parte el array e inserta (si quieres), modifica el array y devuelve la parte del array cortada
	//Insertar elemento nuevo en cualquier posicion de un array
	$pos_ins = 2;
	$array = ["1", "2", "4", "5"];
	//$array2 sera 4 y 5 y $array sera 1, 2 y le insertariamos el 3; le indico desde la posicion desde donde cortar
	//En $array2 me guardara desde la posicion 2 de $array hasta el final del array y me lo sustituira por "3" ($array2 contendra el trozo separado y $array el principio hasta donde se ha cortado)
	$array2 = array_splice($array, $pos_ins, count($array) - 1, "3");
	$array = array_merge($array, $array2); //para unir arrays, imprimira ("1", "2", "3", "4", "5")
	echo "<p>Ins array: " . print_r($array);
	echo "</p>";


	//PRACTICA --> Eliminar cualquier posicion del array
	$pos_del = 2;
	$array = ["0", "1", "2", "3", "4"];
	array_splice($array, $pos_del, 1); //Desde la posicion que toca, cortamos 1 para adelante (devolvera el numero borrado y lo eliminara del array)
	echo "<p>Del pos array" . var_dump($array);
	echo "</p>";


	//PRACTICA --> BORRA DISTINTAS POSICIONES DE UN ARRAY
	//Eliminar cualquier posicion del array
	$array = ["0", "1", "2", "3", "4"];
	$pos_del = [rand(0, count($array) - 1), rand(0, count($array) - 1)]; //BORRAR POSICIONES RANDOM
	echo "<p>Pos a elim: " . var_dump($pos_del);
	echo "</p>";

	rsort($pos_del); //Ordenamos de mas grande a mas pequeño, para no alterar los indices al borrar
	foreach ($pos_del as $valor) {
		array_splice($array, $valor, 1); //Desde la posicion que toca, cortamos 1 para adelante (devolvera el numero borrado y lo eliminara del array)
	}
	echo "<p>" . var_dump($array);
	echo "</p>";

	//PRACTICA --> Dadas dos arrays, imprime una tabla HTML con la informacion de los arrays
	$columnas = ["Nombre", "Stock", "Vendidos"];
	$coches = [
		["Volvo", 22, 18],
		["BMW", 15, 13],
		["Saab", 5, 2],
		["Land Rover", 17, 15]
	];

	//CON FOREACH
	echo '<table class="table">';
	foreach ($columnas as $valor) {
		echo '<th class="table">' . $valor . '</th>';
	}
	foreach ($coches as $valor) {
		echo "<tr>";
		foreach ($valor as $valor_array) {
			echo '<td class="table">' . $valor_array . "</td>";
		}
		echo "</tr>";
	}
	echo "</table>";

	echo "<br>";

	//CON FOR
	echo '<table class="table">';
	for ($x = 0; $x <= count($columnas) - 1; $x++) {
		echo '<th class="table">' . $columnas[$x] . '</th>';
	}
	for ($x = 0; $x <= count($coches) - 1; $x++) {
		echo "<tr>";
		for ($y = 0; $y <= count($coches[$x]) - 1; $y++) {
			echo '<td class="table">' . $coches[$x][$y] . "</td>";
		}
		echo "</tr>";
	}
	echo "</table>";


	//---------------ARRAY ASOCIATIVO---------------
	//Declaracion
	$array_asociativo = ["nombre" => "Julio", "apellidos" => "Díaz Cordero", "edad" => 23];
	print_r($array_asociativo);

	//Para imprimir
	echo "<p> Nombre: " . $array_asociativo["nombre"] . ", Apellidos: " . $array_asociativo["apellidos"] . ", Edad: " . $array_asociativo["edad"];

	echo "<h4>Array asociativo</h4><ul>";
	foreach ($array_asociativo as $x => $valor) {
		//Para modifcar el valor de un array asociativo en un foreach se hace como en un array normal, $x y $valor son copias del original (no modifica el array original)
		$array_asociativo[$x] = $x; //Modifico el array asociativo para que en el valor tenga el indice
		echo "<li>" . $x . ": " . $array_asociativo[$x] . "</li>"; //Aqui imprimira el indice y el valor del array en ese indice -> imprimira nombre nombre, apellidos apellidos, edad edad
		echo "<li>" . $x . ": " . $valor . "</li>"; //aunque haya modificado el array original, al ser $x y $valor copias del original, no se modifica, imprimirá el array sin los cambios que he realizado
	}
	echo "</ul>";


	//FUNIONES ARRAY ASOCIATIVO
	//asort() --> ordena array asociastivo en orden ascendiente (respecto al valor)
	//arsort() --> ordena array asociastivo en orden descendiente (respecto al valor)
	//ksort() --> ordena array asociastivo en orden ascendiente (respecto la clave)
	//krsort() --> ordena array asociastivo en orden descendiente (respecto la clave)



	//PRACTICA --> Dadas dos arrays asociativos, imprime una tabla HTML con la informacion de los arrays
	$coches = [
		["Nombre" => "Volvo", "Stock" => 22, "Vendidos" => 18],
		["Nombre" => "BMW", "Stock" => 15, "Vendidos" => 13],
		["Nombre" => "Saab", "Stock" => 5, "Vendidos" => 2],
		["Nombre" => "Land Rover", "Stock" => 17, "Vendidos" => 15]
	];

	//CON FOREACH
	echo '<table class="table">';
	foreach ($coches[0] as $x => $valor) { //Utilizaremos las claves del array $coches para los titulos, para ello recorremos el primer array de $coches para sacar los titulos
		echo '<th class="table">' . $x . '</th>';
	}

	foreach ($coches as $valor) {
		echo "<tr>";
		foreach ($valor as $valor_array) {
			echo '<td class="table">' . $valor_array . "</td>";
		}
		echo "</tr>";
	}
	echo "</table>";


	//PRACTICA --> convertir el array asociativo coches en un array normal, y otro array con las claves
	$columnas = array_keys($coches[0]);

	foreach ($coches as $x => $valor) {
		$coches[$x] = array_values($coches[$x]);
	}

	print_r($columnas);
	print_r($coches);
	echo "<br><br>";

	//PRACTICA --> convertir el array coches en un array asociativo
	foreach ($coches as $x => $valor) {
		foreach ($valor as $y => $val) {
			$coches[$x][$columnas[$y]] = $val; //coches[0]["Nombre"] = "Volvo" | coches[0]["Stock"] = 22 | coches[0]["Vendidos"] = 18
			unset($coches[$x][$y]); //Borro pos [0][0] | [0][1] | [0][2]
		}
	}
	print_r($coches);


	//PRACTICA --> Programa que permita comparar dos arrays asociativos, las claves y valores que coinciden seran verdes y los que no rojos, si la clave existe será verde (usando isset)
	$array1 = ["grillo" => "crick", "pato" => "quack", "loro" => "croack", "perro" => "guau", "serpiente" => "fff", "gato" => "miau", "vaca" => "muuu"];
	$array2 = ["perro" => "wof", "cerdo" => "oink", "pato" => "quack", "gallo" => "kikiriki", "leon" => "roarr", "gato" => "miau"];

	echo '<table class="table">';
	foreach ($array1 as $x => $valor) {
		echo "<tr>";
		if (isset($array2[$x])) { //compruebo si la clave de array1 existe en array2
			echo '<td class="verde">' . $x . "</td>"; //si lo tiene pintamos en verde
			echo ($array2[$x] == $valor) ? '<td class="verde">' . $valor . '</td>' : '<td class="rojo">' . $valor . "</td>"; //Si existe y tiene el mismo valor pintamos en verde
		} else {
			echo '<td class="rojo">' . $x . "</td>"; //Si no pintamos en rojo
			echo '<td class="rojo">' . $valor . "</td>";
		}
		echo "</tr>";
	}
	echo '</table>';

	echo "<br>";
	//lo mismo en la otra tabla
	echo '<table class="table">';
	foreach ($array2 as $x => $valor) {
		echo "<tr>";
		if (isset($array1[$x])) {
			echo '<td class="verde">' . $x . "</td>";
			echo ($array1[$x] == $valor) ? '<td class="verde">' . $valor . '</td>' : '<td class="rojo">' . $valor . "</td>";
		} else {
			echo '<td class="rojo">' . $x . "</td>";
			echo '<td class="rojo">' . $valor . "</td>";
		}
		echo "</tr>";
	}
	echo "</table>";



	//---------------FUNCIONES---------------
	//Declaracion
	function nombrefuncion($params)
	{
	}

	//valor por defecto de parametros en una funcion
	function nombrefuncion2($param1 = 1, $param2 = 2)
	{
	}

	function prueba()
	{
		return "hola";
	};

	//------------------------------
	//PRACTICA FUNCIONES
	//------------------------------
	//Realiza la practica del comparador de arrays asociativos en una función
	//------------------------------

	//Llamo funcion para comparar el primer array con el segundo
	tableAssociativeArray($array1, $array2);
	//Llamo funcion para comparar el segundo array con el primero
	tableAssociativeArray($array2, $array1);

	//Creeo función
	function tableAssociativeArray($array1, $array2)
	{
		echo '<table class="table">';
		foreach ($array1 as $x => $valor) {
			echo "<tr>";
			if (isset($array2[$x])) { //compruebo si la clave de array1 existe en array2
				echo '<td class="verde">' . $x . "</td>"; //si lo tiene pintamos en verde
				echo ($array2[$x] == $valor) ? '<td class="verde">' . $valor . '</td>' : '<td class="rojo">' . $valor . "</td>"; //Si existe y tiene el mismo valor pintamos en verde
			} else {
				echo '<td class="rojo">' . $x . "</td>"; //Si no pintamos en rojo
				echo '<td class="rojo">' . $valor . "</td>";
			}
			echo "</tr>";
		}
		echo '</table>';
	}




	//---------------LIBRERIAS PROPIAS PHP---------------
	/*Las librerias pueden tener codigo PHP, HTML y CSS, si solo tiene PHP se recomienda no cerrar el archivo con ?> (o comprobar que no hay espacios o tabulaciones despues de cerrarlo)*/
	//Por lo tanto puede usarse como librería de funciones o para tener fragmentos de html y css e incluirlas en la pagina principal cuando quieras (html por modulos)

	//include("libreria.php");
	//include_once("libreria.php");

	//require("libreria.php"); --> Lo mismo que include pero saltara error si la inclusion no puede realizarse
	//require_once("libreria.php"); --> Si se vuelve a llamar a la libreria no la volvería a generar


	//------------------------------
	//PRACTICA FUNCIONES y LIBRERIA PROPIA
	//------------------------------
	//Realiza la practica del comparador de arrays asociativos, pero que primero ordene los OK (pintados en verde) luego los semiOK(clave en verde valor en rojo) y luego los KO (rojos)
	//que estos esten ordenados ascendientemente segun la clave, y pon las funciones en tu propia librería
	//------------------------------

	//añado librería donde estan las funciones de comparar y ordenar los array asociativos, y la de imprimirlos en la tabla
	require_once("libreria.php");

	//tabla 1
	$res = compareAssociativeArray($array1, $array2);
	printCompareAssociativeArray($res);

	//tabla 2
	$res = compareAssociativeArray($array2, $array1);
	printCompareAssociativeArray($res);



	//---------------$_SERVER SUPERGLOBAL---------------
	$_SERVER; //--> variable global que nos permite acceder a informacion del servidor, podemos acceder a ella en cualquier lugar del codigo

	print_r($_SERVER["PHP_SELF"]); //--> Contiene el nombre del archivo en ejecucion
	print_r($_SERVER["REQUEST_METHOD"]); //--> Contiene el metodo utilizado en la peticion (GET o POST)
	print_r($_SERVER["HTTP_HOST"]); //--> Nombre del dominio donde se encuentra la pagina
	print_r($_SERVER["DOCUMENT_ROOT"]); //--> Directorio raiz del servidor donde se encuentra la pagina
	print_r($_SERVER["SCRIPT_FILENAME"]); //--> Ruta completa del archivo en ejecucion
	print_r(isset($_SERVER["HTTP_REFERER"])); //--> Contiene la direccion de la pagina que nos ha enlazado
	print_r($_SERVER["HTTP_USER_AGENT"]); //--> Contiene el navegador utilizado
	print_r($_SERVER["REMOTE_ADDR"]); //--> Contiene la direccion IP del visitante



	//---------------$_POST SUPERGLOBAL---------------
	//$_POST es un array con la informacion recibida por el formulario (name del input del formulario y su valor)
	$logins = [
		["mail" => "prueba@gmail.com", "pwd" => password_hash("1234", PASSWORD_DEFAULT)],
		["mail" => "aa@gmail.com", "pwd" => password_hash("1111", PASSWORD_DEFAULT)],
		["mail" => "bb@gmail.com", "pwd" => password_hash("2222", PASSWORD_DEFAULT)]
	];
	$type_pwd = "";
	$usr = '';
	$pwd = '';
	$mail = '';
	$errusr = '';
	$errpwd = '';
	$errmail = '';
	$errgeneral = '';
	$mail_ok = false;
	$pwd_ok = false;

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		echo "<h1>ENTRA POR POST</h1>";
		print_r($_POST);

		if ($_COOKIE["contador"] <= 0) {
			$errgeneral = "Ya lo ha intentado 3 veces amigo, ¿no sera ud. un hacker?";
		} else {

			if (isset($_POST["mostrar_pwd"])) {
				$type_pwd = 'password';
			}

			if (isset($_POST["mostrar_text"])) {
				$type_pwd = 'text';
			}

			if (!empty($_POST["usr"])) {
				$usr = htmlspecialchars($_POST["usr"]);
			} else {
				$errusr = "Rellena el usuario, perro";
			}

			if (!empty($_POST["mail"])) {
				$mail = htmlspecialchars($_POST["mail"]);
				if (empty(filter_var($mail, FILTER_VALIDATE_EMAIL))) {
					$errmail = "Pon un correo valido, perro";
				} else {

					/*foreach ($logins as $x => $login) {
					foreach ($login as $y => $valor) {
						if ($y == "mail" && $valor == $mail) {
							$mail_ok = true;
							$pos_mail = $x;
							break;
						}
					}*/
				}

				/*
				if (!$mail_ok) {
					$errmail = "Correo inexistente en BD.";
				}
				*/
			} else {
				$errmail = "Rellena el correo, perro";
			}

			if (!empty($_POST["pwd"])) {
				$pwd = htmlspecialchars($_POST["pwd"]);

				/*
			foreach ($logins[$pos_mail] as $x => $valor) {
				if ($mail_ok && $x == "pwd" && password_verify($pwd, $valor)) {
					$pwd_ok = true;
				}
			}
			*/


				$cont = 0;
				while ((!$mail_ok && !$pwd_ok) && ($cont <= (count($logins) - 1))) {

					foreach ($logins[$cont] as $y => $valor) {
						if ($y == "mail" && $valor == $mail) {
							$mail_ok = true;
						}
						if ($y == "pwd" && password_verify($pwd, $valor)) {
							$pwd_ok = true;
						}
					}

					if (!$mail_ok || !$pwd_ok) {
						$mail_ok = false;
						$pwd_ok = false;
					}

					$cont++;
				}

				if (!$pwd_ok || !$mail_ok) {
					$errpwd = "Contraseña o correo incorrecto";
					//$_COOKIE["contador"] = $_COOKIE["contador"] - 1;
				}


				/*if ($pwd != $password || $mail != $correo) {
				$errpwd = "Contraseña o correo incorrecto.";
			}*/
			} else {
				$errpwd = "Rellena la contraseña, perro";
			}


			if (isset($_POST["form_html"])) {
				echo "Formulario HTML";
			} else {
				echo "Formulario PHP";
			}
		}
	} else {
		//Por defecto en una pagina entras por GET
		echo "<h1>ENTRA POR GET</h1>";
		$type_pwd = 'password';
	}


	//Podemos poner el valor de los datos que se han introducido correctamente para que el usuario no tenga que volver a ponerlos, si el formulario es en php se lo indicamos en la etiqueta value
	$form = '
	<form action="' . $_SERVER["PHP_SELF"] . '" method="POST" enctype="multipart/form-data">
	<label for="id_usr" tabindex="3">Usuario</label>
		<input type="text" name="usr" id="id_usr" value="' . $usr . '" required>
		<br><br>
		<label for="id_pwd" tabindex="1">Contraseña</label>
		<input type="password" name="pwd" id="id_pwd" required>
		<br><br>
		<input type="submit" name="form_php" value="Enviar"><br><br>
		</form>';

	echo $form;




	//Funcion empty($variable) --> devuelve true si $variable esta vacia o si $variable no existe, devuelve false si tiene valor
	//htmlspecialchars($string) --> llamar despues de cada input de texto, convierte caracteres especiales de html en asci para que no ejecuten codigo
	//html_entities($string) --> lo mismo que htmlspecialchars pero tambien convirtiendo las letras en asci (mas usado si trabajas en codificaciones distintas a uft8)
	//html_entity_decode($string) --> realiza la función inversa a html_entities (descodifica el asci a letras normales)
	//strip_tags($string) --> elimina todas las etiquetas html en un string, segundo parametro para permitir algunas etiquetas
	//filter_var($string, CONTSANT) --> Para comprobar si lo que introducen es un correo, una URL o una direccion IP, como segundo parametro se le indicara una de las siguientes constantes:
	//FILTER_VALIDATE_EMAIL --> comprobar correo
	//FILTER_VALIDATE_URL --> comprobar URL
	//FILTER_VALIDATE_IP --> comprobar IP


	?>

	<!-- Podemos poner el valor de los datos que se han introducido correctamente para que el usuario no tenga que volver a ponerlos, directamente en el HTML con: value="<?php?>"-->
	<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data" id="form">
		<label for="id_usr" tabindex="3">Usuario</label>
		<input type="text" name="usr" id="id_usr" value="<?php echo $usr; ?>" required>
		<br>
		<small class="red"><?php echo $errusr ?></small>
		<br><br>
		<label for="id_pwd" tabindex="1">Contraseña</label>
		<input type="password" name="pwd" id="id_pwd" required>
		<br>
		<small class="red"><?php echo $errpwd ?></small>
		<br><br>
		<label for="id_edad">Edad</label>
		<input type="number" min="18" max="99" name="edad" id="id_edad" value="<?php echo (isset($_POST["edad"])) ? htmlspecialchars($_POST["edad"]) : ""; ?>">
		<br><br>
		<label for="id_mail">Correo</label>
		<input type="text" name="mail" id="id_mail" value="<?php echo $mail; ?>">
		<br>
		<small class="red"><?php echo $errmail ?></small>
		<br><br>
		<label for="id_fecha">Fecha</label>
		<input type="date" name="fecha" id="id_fecha" value="<?php echo (isset($_POST["fecha"])) ? htmlspecialchars($_POST["fecha"]) : ""; ?>">
		<br><br>
		<p>Sexo</p>
		<label for="id_radh">Hombre</label>
		<input type="radio" name="sexo" value="hombre" id="id_radh" <?php if (isset($_POST["sexo"]) == "hombre" || empty($_POST["sexo"])) {
																		echo "checked";
																	} ?>>
		<label for="id_radm">Mujer</label>
		<input type="radio" name="sexo" value="mujer" id="id_radm" <?php if (isset($_POST["sexo"]) == "mujer") {
																		echo "checked";
																	} ?>>
		<br><br>
		<label for="id_ec">Estado Civil</label>
		<select name="ec" id="id_ec">
			<option value="soltero" <?php if (isset($_POST["ec"]) == "soltero" || empty($_POST["ec"])) {
										echo "selected";
									} ?>>Soltero</option>
			<option value="casado" <?php if (isset($_POST["ec"]) == "casado") {
										echo "selected";
									} ?>>Casado</option>
			<option value="jodido" <?php if (isset($_POST["ec"]) == "jodido") {
										echo "selected";
									} ?>>Con hijos</option>
		</select>
		<br><br>
		<label for="id_cond">Acepta las condiciones de privacidad</label>
		<input type="checkbox" name="cond" id="id_cond" required>

		<input type="submit" name="form_html" value="Enviar"><br><br>

		<?php if (!empty($pwd) && empty($errpwd) && !empty($usr) && empty($errusr)) {
			echo "<h4>Formulario enviado correctamente</h4>";
		} ?>
	</form>


	<?php
	//Si el formulario es correcto muestro formula de la cocacola
	if (!empty($pwd) && empty($errpwd) && !empty($mail) && empty($errmail)) {
		echo "<h1>Formula de la cocacola</h1>";
	} else {
		//Si no es correcto o no se ha enviado, muestro el formulario
		//para mostrar el formulario, cierro php en el else y lo muestro
	?>

		<!-- muestro formulario en HTML dentro del else de PHP -->
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_mail">Correo</label>
			<input type="text" name="mail" id="id_mail" value="<?php echo isset($_COOKIE["mail"]) ? $_COOKIE["mail"] : $mail; ?>">
			<br>
			<small class="red"><?php echo $errmail; ?></small>
			<br><br>
			<label for="id_pwd" tabindex="1">Contraseña</label>
			<input type="<?php echo $type_pwd ?>" name="pwd" id="id_pwd" value="<?php echo $pwd; ?>" required>
			<button type="submit" name="mostrar_text" style="<?php echo ($type_pwd == "text") ? "display: none" : "" ?>">Mostrar(&#60;o&#62;)</button>
			<button type="submit" name="mostrar_pwd" style="<?php echo ($type_pwd == "password") ? "display: none" : "" ?>">Ocultar(&#60;-&#62;)</button>
			<input type="checkbox" name="mostrar_text_js" onclick="show_pwd()">JS(&#60;o&#62;)
			<br>
			<small class="red"><?php echo $errpwd; ?></small>
			<br><br>
			<input type="submit" name="form_practica" value="Enviar"><br><br>
			<small class="red"><?php echo $errgeneral; ?></small>
		</form>
		<h3>Intentos restantes: <?php echo (isset($_COOKIE['contador'])) ? $_COOKIE['contador'] : "" ?></h3>
	<?php
	} //Vuelvo abrir PHP para cerrar el else
	?>


	<script>
		function show_pwd() {
			console.log("entro");
			var pwd = document.getElementsByName("pwd");
			console.log(pwd[2].type);

			if (pwd[2].type === "password") {
				pwd[2].type = "text";
			} else {
				pwd[2].type = "password";
			}
		}
	</script>





	<?php
	//CIFRADO CONTRASEÑAS

	//Funcion md5 --> sirve para que la contraseña no salga escrita directamente (no es para cifrar)
	$pwd = md5("password");
	echo $pwd . "<br>";

	//password_hash(string $password, PASSWORD_DEFAULT) //Cadenas resultantes son de hasta 256 caracteres (casmpo contraseña de la BD deberá ser de esa longitud)
	//password_verify(string $password, string $pwd_encriptada) //devuelve true o false

	$pwd = password_hash("password", PASSWORD_DEFAULT);
	echo $pwd . "<br>";

	echo password_verify("password", $pwd);



	//COOKIES
	/*
	//Crear cookie --> Solo se puede usar antes de imprimir contenido (ponerlo lo primero)
	setcookie(
		string $name,
		string $value = "",
		int $expires = 0, --> time()+60*60*24*30 (caducidad en 30 dias, 60 seg, 60 min, 24h, 30 dias)
		string $path = "",
		string $domain = "",
		bool $secure = false,
		bool $httponly = false
	); --> devuelve bool true o false

	//Coger valor de cookie
	$_COOKIE[$name];
	*/


	if (isset($_COOKIE["nombre"])) {
		echo '¡Hola ' . htmlspecialchars($_COOKIE["nombre"]) . '!';
		print_r($_COOKIE);
	}


	//SESIONES
	//Compartir informacion de un cliente entre varias paginas, expiran al cerrar el navegador
	//session_start(); //--> Funcion necesaria para crear sessiones y que debe estar arriba de la pagina antes de hacer ningun echo
	//$_SESSION["nombre"];

	//echo (session_status() == PHP_SESSION_NONE) ? "No hay sesion iniciada" : "Sesion activa";

	//	unset($_SESSION['count']); //Desregistrar una variable de sesion

	//session_destroy(); //--> para cerrar session


	//ENVIO DE MAIL
	//hay que configurar un servidor de correo en el servidor para que funcione.
	//mail(para, asunto, mensaje, cabecera) //devuelve true o false
	//Si se quiere poner varios destinatarios, los correos van separados por comas
	//Para hacer un salto de linea se usa \r\n
	//Cabecera se usa para parametros extra (From, CC y BCC), si elijes varios se separa con \r\n 

	//ej.
	//$destino = "nil_cule@hotmail.com";
	//$correo = "hola\r\nmundo";
	//$asunto = "primer correo";
	//$cabecera = "from:destino@gmail.com\r\nbcc:destino2@gmail.com";
	//mail($destino, $asunto, $correo, $cabecera);

	//Se ha de configurar php.ini (en C:/xampp/php/php.ini)
	//SMTP=mail.training.cat
	//smtp_port=465
	//sendmail_from = backend@training.cat
	//sendmail_path = "\"C:\xampp\sendmail\sendmail.exe\"-t"
	//sendmail_from = backend@training.cat

	//Y tambien se ha de modificar el archivo sendmail.ini (C:/xampp/sendmail/sendmail.ini)
	//smtp_server=mail.training.cat
	//smtp_port=465
	//auth_username=backend@training.cat
	//auth_password=backend
	//force_sender=backend@training.cat

	$enviado = "";
	$destino = "";
	$correo = "";
	$asunto = "";
	$cabecera = "";

	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		if (isset($_POST["form_email"])) {

			if (!empty($_POST["maild"])) {
				$destino = $_POST["maild"];
			}

			if (!empty($_POST["mailr"])) {
				$cabecera = "from:" . $_POST["mailr"];
			}

			if (!empty($_POST["asunto"])) {
				$asunto = $_POST["asunto"];
			}

			if (!empty($_POST["mensaje"])) {
				$correo = $_POST["mensaje"];
			}
		}
	}

	if (!empty($_POST["maild"]) && !empty($_POST["mailr"]) && !empty($_POST["asunto"]) && !empty($_POST["mensaje"])) {
		echo $destino . ", " . $cabecera . ", " . $asunto . ", " . $correo;
		$res = mail($destino, $asunto, $correo, $cabecera);

		if ($res) {
			$enviado = "Mensaje enviado correctamente";
		}
	}





	?>

	<h1>Envio de correo</h1>
	<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
		<label for="id_maild">Destinatario</label>
		<input type="text" name="maild" id="id_maild">
		<br>
		<br><br>
		<label for="id_mailr">Remitente</label>
		<input type="text" name="mailr" id="id_mailr">
		<br><br>
		<label for="id_asunto">Asunto</label>
		<input type="text" name="asunto" id="id_asunto">
		<br><br>
		<label for="id_mensaje">Mensaje</label>
		<textarea name="mensaje" id="id_mensaje" minlength="1" maxlength="255" placeholder="Escribe el correo"></textarea>
		<br><br>
		<input type="submit" name="form_email" value="Enviar Correo"><br><br>
		<br>
	</form>
	<h3><?php echo !empty($enviado) ? $enviado : "" ?></h3>




	<?php

	//FICHEROS
	/*
	file_exists("fichero.txt"); //devuelve true o false
	is_writable("fichero.txt"); //devuelve true o false
	is_readable("fichero.txt"); //devuelve true  o false
	$enlace = fopen("fichero.txt", $modo);
	//modo --> w write(machaca archivo existenete), x -> write sin machacar archivo existente, r -> read, a -> añadir a archivo al final   //devuelve false si no ha podido abrir el archivo
	//Cada modo tiene su variante + (w+ -> ademas puede leer, x+ -> ademas puede leer, r+ -> ademas puede escribir donde tu quieras, a+ -> ademas puede leer)
	$bytreescritos = fwrite($enlace, "texto a escribir"); //funcion para escribir en archivo, devuelve cuantos bytes a escrito
	PHP_EOL; //Constante de salto de línea en ficheros (donde se pondría \r\n o \n se pondrá esta contante)
	fgets($enlace); //lee linea por linea, devuelve false cuando no hay mas lineas
	filesize("fichero.txt"); //Tamaño del fichero en bytes
	$contenido = fread($enlace, $numerobytes); //PARA LEER FICHERO ENTERO --> fread($enlace, filesize("fichero.txt"));
	$posicion = ftell($enlace); //Para conocer la posicion actual del puntero
	rewind($enlace); //Poner el puntero al principio del archivo
	
	FSEEK --> WARNING!! --> si se salta el feof (end of file)  hara bucles infinitos
	fseek($enlace, 0); //Poner puntero donde quieras, Si tiene exito devuelve 0, si no -1
	fseek($enlace, 5, SEEK_CUR); //Cuenta 5 bytes desde la posicion del cursor
	fseek($enlace, -5, SEEK_END); //Retrocede 5 bytes desde la posicion final del archivo
	feof($enlace); //Para saber si el puntero está al final del archivo (ha acabado de leer), devuele true o false

	$data = file_get_contents("archivo.txt"); --> leer todo un fichero de una
	file_put_contents("archivo.txt", $datos); --> graba en un fichero, creando si no existe y machacandolo si existe

	$_FILES; //Toda informacion sobre el fichero enviado por formulario
	["archivo"] -> name del input de tipo file en el HTML
		["name"] -> Nombre y extension original
		["type"] -> Tipo de datos que contiene el archivo (devuelkve solo extension)
		["tmp_name"] -> Ruta temporal del archivo en el servidor
		["error"] -> Contiene 0 si no ha habido errores
		["size"] -> tamaño del fichero en bytes

	move_uploaded_file($ruta_temp, $ruta_final); //Para mover un fichero a otra ruta

	Para adjuntar varios ficheros en el selector de adjuntar ficheros
	Hay que ponerle [] en el nombre, como si fuera un array y ponerle el atributo multiple
	<input type="file" name="fichero[]" multiple>

	Para recoger multiples archivos:
	$_FILES["archivo"]
		["name"]
			0 => fichero0.txt
			1 => fichero1.txt
			...	
	Y asi con los demas apartados de $_FILES

	unlink($ruta_fichero); //Para eliminar ficheros
	mkdir("NuevaCarpeta"); //Para crear una carpeta
	rmdir("Carpeta"); //Para eliminar una carpeta
	rename("archivo.txt", "renombrado.txt"); //Para renombrar carpetas
	copy("ruta_archivo.txt", "ruta_copia_archivo.txt"); //Para copiar una carpeta

	if ($enlace = fopen("fichero.txt", "w")) {
		fwrite($enlace, "hola mundo" . PHP_EOL . "adios mundo" . PHP_EOL . "sekai nitami, shinratenseee" . PHP_EOL . date("d/m/Y H:i:s")); //date
		fclose($enlace);
	}

	*/

	$mens = "";
	$errdni = "";

	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		if (isset($_POST["form_dni"])) {

			if (!empty($_POST["dni"])) {
				$dni = htmlspecialchars($_POST["dni"]);
				if(search_dni($dni)){
					$mens = "Adelante, señor con DNI: ".$dni;
				}else{
					$errdni = "Ud no esta en la lista, señor con dni {$dni}, no es bienvenido aqui, FUERA!";
				}
			} else {
				$errdni = "pon un dni";
			}
		}



		if (isset($_POST["form_dni_change"])) {

			if (!empty($_POST["dni_orig"])) {

				$dni_orig = htmlspecialchars($_POST["dni_orig"]);

				if (!empty($_POST["dni_sust"])) {
					$dni_sust = htmlspecialchars($_POST["dni_sust"]);

					change_dni($dni_orig, $dni_sust);
				}else{
					$errdni = "pon un dni a sustituir";
				}

			}else{
				$errdni = "pon un dni origen";
			}


		}
	}

	?>



	<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
		<label for="id_dni">DNI</label>
		<input type="text" name="dni" id="id_dni">
		<br>
		<small class="red"><?php echo $errdni; ?></small>
		<br>
		<input type="submit" name="form_dni" value="Enviar DNI"><br><br>
		<br>
		<h3><?php echo $mens; ?></h3>
	</form>

	<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
		<label for="id_dni_orig">DNI origen</label>
		<input type="text" name="dni_orig" id="id_dni_orig">
		<br>
		<label for="id_dni_sust">DNI a sustituir</label>
		<input type="text" name="dni_sust" id="id_dni_sust">
		<br>
		<input type="submit" name="form_dni_change" value="Enviar DNI"><br><br>
		<br>
	</form>

</body>



</html>


<?php
function search_dni($dni_search)
{
	$search = false;
	$enlace = fopen("DNI.txt", "r");

	$num = 0;
	echo $num;
	while (!feof($enlace) && !$search) {
		$dni = fread($enlace, 9);
		echo $dni;
		if ($dni_search == $dni) {
			$search = true;
		} else {
			fread($enlace, 2);
		}
	}

	return $search;
}






//BASES DE DATOS-----------------------

/*
//Comandos consola------
dir --> contenido del directorio
CD --> cambiar de directorio
cd c:\xampp\mysql\bin --> ruta donde se encuentra el ejecutable de mysql en xampp
mysql -u usuario -p --> ejecutar sql (-u nombre de usuario, -p indicamos que solicite contraseña)
QUIT --> para salir de la consola de mysql

TIPOS DE DATOS BD------
textos --> varchar(x)
numeros --> 
	tinyint (-128, 127), int(-2Billones, 2Billones)
	UNSIGNED tinyint (0, 255), UNSIGNED int (0, 4Billones) //por si no usas numeros negativos, el rango de los negativos se lo da a los positivos
decimales -->
	FLOAT --> Hasta 23 decimales
	DECIMAL(totalLongitud, CantDecimales) | ej. DECIMAL(9,2) -> 1234567,89
booleanos --> 
	BOOL --> se usa true/false como valor
	BOOLEAN --> se almacena como 1 o 0
fechas -->
	DATETIME --> YYYY-MM-DD hh:mm:ss.999999 (los 9 son milisegundos)
				CURRENT_TIMESTAMP (fecha actual)




//BD------
CREATE DATABASE nombreDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci; (para crear BD con formato UTF8 con caracteres haste de 4 bytes y que el orden en que nos devuelva las consultas ordenadas)
Si creamos la BD asi nos aseguramos que todo lo que se le meta sea compatible.

SHOW DATABASES; --> Muestra las BD disponibles
SHOW TABLES; --> Muestra las tablas de la BD
DESRIBE tabla; --> ver la estructura de la tabla

USE nombreDB; //Para usar la BD seleccionada

IF NOT EXIST (en create database o create table para que no lo cree si ya existe) ej. CREATE DATABASE IF NOT EXIST nombreDB;

DROP DATABASE nombreDB;
IF EXIST (en drop database para que si no existe no haga nada) EJ. DROP DATABASE IF EXIST nombreDB;


//TABLES------
CREATE TABLE table_name (
    column1 datatype,
   ....
);

DROP TABLE table_name;

TRUNCATE TABLE table_name; //Para borrar todo lo que haya dentro de la tabla, pero no la tabla

ALTER TABLE table_name
DROP COLUMN column_name; //ELIMINAR campo de la tabla

ALTER TABLE table_name
ADD column_name datatype; //AÑADIR campo a la tabla

ALTER TABLE table_name
MODIFY COLUMN column_name datatype; //MODIFICAR campo de la tabla


//Columnas-----

INSERT INTO table_name (column1, column2, column3, ...)
VALUES (value1, value2, value3, ...);

DELETE FROM table_name WHERE condition;

UPDATE table_name
SET column1 = value1, column2 = value2, ...
WHERE condition;


//Parametros------
NOT NULL --> Si queremos que un campo sea obligatorio de llenar
PRIMARY KEY --> Para la PK 
AUTO_INCREMENT --> Si la PK es un contador le indicamos 
DEFAULT --> Valor por defecto en una columna
UNIQUE --> Forzar que los valores no puedan repetirse sin ser PK

ej.
CREATE TABLE personas (
	idpersona int PRIMARY KEY AUTO_INCREMENT,
	nombre varchar(50) NOT NULL,
	apellido varchar(50) NOT NULL,
	email varchar(255) NOT NULL UNIQUE,
	puntos int UNSIGNED NOT NULL DEFAULT 0,
	creado datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
	modificado datetime ON UPDATE CURRENT_TIMESTAMP NOT NULL
);


INSERT INTO personas (nombre, apellido, email)
VALUES ("Pepe", "Garcia", "email1@gmail.com");

Condiciones WHERE
AND, OR, IN, LIKE
ORDER BY campo DESC | ASC


*/


?>