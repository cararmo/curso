<?php

$nombre = "";
$sug = "";
$errgeneral = "";
$pwd = '';
$mail = '';
$mail_login = '';
$errpwd = '';
$errgeneral = '';
$mail_ok = false;
$pwd_ok = false;
$fase = "sugerencias_w";
$nom_fichero = "sugerencias.txt";
$mens = "";
$errnombre = "";
$errmail = "";
$errsugerencia = "";
$fichero = false;
$ruta_final = "sugerencias/";
$ruta_final_ficheros = [];

$logins = [
	["mail" => "prueba@gmail.com", "pwd" => password_hash("1234", PASSWORD_DEFAULT)],
	["mail" => "aa@gmail.com", "pwd" => password_hash("1111", PASSWORD_DEFAULT)],
	["mail" => "bb@gmail.com", "pwd" => password_hash("2222", PASSWORD_DEFAULT)]
];

if ($_SERVER["REQUEST_METHOD"] == "POST") {



	if (isset($_POST["enviar"])) {
		$fase = "sugerencias_w";

		if (!empty($_POST["nombre"])) {

			$nombre =  htmlspecialchars($_POST["nombre"]);
		} else {
			$errnombre = "Introduzca un nombre";
		}



		if (!empty($_POST["mail"])) {

			$mail = htmlspecialchars($_POST["mail"]);
			if (!empty(filter_var($mail, FILTER_VALIDATE_EMAIL))) {
				$correo = htmlspecialchars($_POST["mail"]);
			} else {
				$errmail = "Pon un correo valido";
			}
		} else {
			$errmail = "Introduzca una direccion de correo";
		}



		if (!empty($_POST["sugerencia"])) {

			$sug = htmlspecialchars($_POST["sugerencia"]);
		} else {
			$errsugerencia = "Introduzca una sugerencia";
		}

		if (!empty($_FILES["fichero"])) {

			foreach ($_FILES["fichero"]["name"] as $x => $name_fich) {
				//$cont = count(scandir($ruta_final))-1;
				$cont = count(glob($ruta_final . "sug*.*"));
				$ruta_final_ficheros[$x] = $ruta_final . "sug{$cont}." . pathinfo($name_fich, PATHINFO_EXTENSION);

				if (move_uploaded_file($_FILES["fichero"]["tmp_name"][$x], $ruta_final_ficheros[$x])) {
					$mens = "Sugerencia enviada correctamente";
					$fichero = true;
				}
			}


			/*
			//$cont = count(scandir($ruta_final))-1;
					$cont = count(glob($ruta_final . "sug*.*"));
					$ruta_final .= "sug{$cont}." . pathinfo($_FILES["fichero"]["name"], PATHINFO_EXTENSION);
					if (move_uploaded_file($_FILES["fichero"]["tmp_name"], $ruta_final)) {
						$mens = "Sugerencia enviada correctamente";
						$fichero = true;
					}
					*/
		}
	}


	if (isset($_POST["login_b"])) {
		$fase = "login";
	}


	if (isset($_POST["volver_sugw"])) {
		$fase = "sugerencias_w";
	}


	if (isset($_POST["login"])) {
		$fase = "login";

		if (!empty($_POST["mail_login"])) {
			$mail_login = htmlspecialchars($_POST["mail_login"]);
			if (empty(filter_var($mail_login, FILTER_VALIDATE_EMAIL))) {
				$errmail = "Pon un correo valido, perro";
			}
		} else {
			$errmail = "Rellena el correo, perro";
		}

		if (!empty($_POST["pwd"])) {
			$pwd = htmlspecialchars($_POST["pwd"]);

			$cont = 0;
			while ((!$mail_ok && !$pwd_ok) && ($cont <= (count($logins) - 1))) {

				foreach ($logins[$cont] as $y => $valor) {
					if ($y == "mail" && $valor == $mail_login) {
						$mail_ok = true;
					}
					if ($y == "pwd" && password_verify($pwd, $valor)) {
						$pwd_ok = true;
					}
				}

				if (!$mail_ok || !$pwd_ok) {
					$mail_ok = false;
					$pwd_ok = false;
				}

				$cont++;
			}

			if (!$pwd_ok || !$mail_ok) {
				$errpwd = "Contraseña o correo incorrecto";
			}
		} else {
			$errpwd = "Rellena la contraseña, perro";
		}
	}

	if (isset($_POST["borrar"])) {
		$fase = "sugerencias_r";

		$nom_fichero = "sugerencias.txt";
		if (file_exists($nom_fichero)) {
			$enlace = fopen($nom_fichero, "w");
			fclose($enlace);
		}

		//elimino tambien los ficheros adjuntos de las sugerencias
		foreach (glob($ruta_final . "sug*.*") as $fichero) {
			unlink($fichero);
		}

		leer_sugerencias($nom_fichero);
	}
}

//Si el login es OK camio de fase y leo y muestro las sugerencias
if (isset($_POST["login"]) && !empty($pwd) && empty($errpwd) && !empty($mail_login) && empty($errmail)) {
	$fase = "sugerencias_r";

	leer_sugerencias($nom_fichero);
}

//Si el formulario esta ok creo fichero
if (isset($_POST["enviar"]) && !empty($nombre) && !empty($mail) && !empty($sug)) {

	$sugarray_fich = [];
	$sugarray = ["nombre" => $nombre, "correo" => $mail, "sugerencia" => $sug, "ip" => $_SERVER["REMOTE_ADDR"], "fecha" => date("d/m/Y H:i:s")];


	if ($fichero) {
		foreach ($ruta_final_ficheros as $x => $ruta_fichero) {
			$sugarray["fichero"][$x] = $ruta_fichero;
		}


		//$sugarray["fichero"] = $ruta_final;
	}


	if (file_exists($nom_fichero)) {
		$sugarray_fich = unserialize(file_get_contents($nom_fichero)); //cargo datos del fichero y los convierto en array
		if (empty($sugarray_fich)) {
			$sugarray_fich = [];
		}
	}

	array_push($sugarray_fich, $sugarray); //inserto nueva posicion
	$sugstring = serialize($sugarray_fich); //convierto array a texto

	if (!file_put_contents($nom_fichero, $sugstring)) { //grabo fichero
		$errsugerencia = "Error al intentar grabar el fichero";
	} else {
		$mens = "Sugerencia enviada!";
	}
}


?>

<!DOCTYPE html>
<html>

<head>

	<style>
		.red {
			color: red;
		}
	</style>

</head>

<?php
switch ($fase) {
	case "login":
?>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<input type="submit" name="volver_sugw" value="Volver"><br>
		</form>

		<h2>LOGIN</h2>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_mail_login">Correo</label>
			<input type="text" name="mail_login" id="id_mail_login" value="<?php echo $mail_login; ?>">
			<br>
			<small class="red"><?php echo $errmail; ?></small>
			<br><br>
			<label for="id_pwd" tabindex="1">Contraseña</label>
			<input type="password" name="pwd" id="id_pwd" required><br>
			<small class="red"><?php echo $errpwd; ?></small>
			<br><br>
			<input type="submit" name="login" value="Login"><br>
		</form>

	<?php
		break;
	case "sugerencias_w":
	?>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<input type="submit" name="login_b" value="Login"><br>
		</form>

		<h2>Envia una sugerencia</h2>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_nombre">Nombre</label>
			<input type="text" name="nombre" id="id_nombre">
			<br>
			<small class="red"><?php echo $errnombre; ?></small><br>
			<label for="id_mail">Correo</label>
			<input type="email" name="mail" id="id_mail">
			<br>
			<small class="red"><?php echo $errmail; ?></small><br>
			<label for="id_sug">Usuario</label>
			<textarea name="sugerencia" id="id_sug" minlength="1" maxlength="255" placeholder="Escribe una descripcion"></textarea>
			<br>
			<small class="red"><?php echo $errsugerencia; ?></small><br>
			<label for="id_fichero">Adjuntar fichero</label>
			<input type="file" name="fichero[]" id="id_fichero" multiple><br>
			<!--Para que permita adjuntar varios ficheros-->
			<input type="submit" name="enviar" value="Enviar"><br>
			<small><?php echo "<h3>{$mens}</h3>"; ?></small><br>
		</form>
		<br>

	<?php
		break;
	case "sugerencias_r":
	?>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<input type="submit" name="login_b" value="Volver"><br>
			<input type="submit" name="borrar" value="Borrar"><br>
		</form>


<?php
		break;
}
?>

</html>


<?php

function leer_sugerencias($nom_fichero)
{

	if (file_exists($nom_fichero) && filesize($nom_fichero) > 0) {

		$sugprint = "<h3>SUGERENCIAS</h3>--------------------------------<br>";
		$arrays = unserialize(file_get_contents($nom_fichero)); //convierto texto del fichero en array
		foreach ($arrays as $array) { //recorro el array de arrays para que me muestre las sugerencias
			foreach ($array as $key => $value) {
				if ($key != "fichero") {
					$sugprint .= "<strong>{$key}:</strong> {$value}<br>";
				} else {
					foreach ($value as $x => $ruta_fichero) {
						$sugprint .= '<strong>' . $key . '-' . ($x+1) . ':</strong> <a href="' . $ruta_fichero . '" download> Descargar </a><br>';
					}
					//$sugprint .= '<strong>' . $key . ':</strong> <a href="' . $value . '" download> Descargar </a><br>';
				}
			}
			$sugprint .= "<br>--------------------------------<br>";
		}

		echo $sugprint;
	} else {
		echo "<h1>No hay sugerencias</h1>";
	}
}



?>