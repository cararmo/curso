<?php

session_start();

$errgeneral = "";
$mens = "";
$lista_grupos = "";

if (empty($_SESSION["usuarios"])) {
	$_SESSION["usuarios"] = [];
}

if (empty($_SESSION["grupos"])) {
	$_SESSION["grupos"] = [];
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if (isset($_POST["ins_participante"])) {

		if (!empty($_POST["nombre"])) {

			insert_participante($_SESSION["usuarios"], $_POST["nombre"]);
		} else {
			$errgeneral = "Introduzca un nombre";
		}
	}


	if (isset($_POST["next"])) {

		if (count($_SESSION["usuarios"]) == 0) {
			$errgeneral = "No hay nombres, no se puede continuar";
		} else {
			$_SESSION["fase"] = 2;
		}
	}

	if (isset($_POST["create_group"])) {

		if (!empty($_POST["num"])) {

			$num = htmlspecialchars($_POST["num"]);
			if (is_numeric($num) && $num > 0) {
				$_SESSION["grupos"] = create_group($_SESSION["usuarios"], $_POST["num"]);
				$_SESSION["fase"] = 3;
			} else {
				$errgeneral = "Introduzca un numero válido";
			}
		} else {
			$errgeneral = "Introduzca personas por grupo";
		}
	}


	if (!empty($_POST["mail"])) {

		$destino = htmlspecialchars($_POST["mail"]);
		if (!empty(filter_var($destino, FILTER_VALIDATE_EMAIL))) {
			$destino = $_POST["mail"];

			if (enviar_mail($destino)) {
				$mens = "Mensaje enviado correctamente";
			} else {
				$errgeneral = "ha ocurrido un problema al enviar el correo";
			}
		} else {
			$errgeneral = "Pon un correo valido";
		}
	}


	if (isset($_POST["reiniciar"])) {

		$_SESSION = [];
		session_destroy();
		
	}
}

//Aquí porque si lo pongo arriba con los demas peta, porque fase siempre tiene valor dentro de session y peta cuando se destruye sesion
if (empty($_SESSION["fase"])) {
	$_SESSION["fase"] = 1;
}

?>

<!DOCTYPE html>
<html>

<head>

	<style>
		.red {
			color: red;
		}
	</style>

</head>
<h1>GENERADOR DE EQUIPOS</h1>

<?php

if (!empty($_SESSION["usuarios"])) {
	echo print_users($_SESSION["usuarios"]);
}

?>


<?php


switch ($_SESSION["fase"]) {
	case 1:


?>

		<br>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_nombre">Nombre</label>
			<input type="text" name="nombre" id="id_nombre">
			<br>
			<input type="submit" name="ins_participante" value="Añadir Participante">
			<input type="submit" name="next" value="Siguiente Paso"><br>
			<small class="red"><?php echo $errgeneral; ?></small><br><br>
			<input type="submit" name="reiniciar" value="Reiniciar">
		</form>
		<br>


	<?php

		break;
	case 2:

	?>

		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_num">Personas por grupo</label>
			<input type="number" name="num" id="id_num">
			<br>
			<input type="submit" name="create_group" value="Crear Grupo"><br>
			<small class="red"><?php echo $errgeneral; ?></small><br><br>
			<input type="submit" name="reiniciar" value="Reiniciar">
		</form>



	<?php
		break;
	case 3:
		echo print_groups($_SESSION["grupos"], true);


	?>

		<br><br>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_mail">Destinatario</label>
			<input type="email" name="mail" id="id_mail">
			<br><br>
			<input type="submit" name="form_email" value="Enviar Correo"><br>
			<small class="red"><?php echo $errgeneral; ?></small>
			<small><?php echo $mens; ?></small><br><br>
			<input type="submit" name="reiniciar" value="Reiniciar">

		</form>

<?php
		break;
}
?>

</html>


<?php

function insert_participante(&$usuarios, $participante)
{
	$usuarios[count($usuarios)] = htmlspecialchars($participante);
}


function create_group($usuarios, $num)
{

	shuffle($usuarios);
	$num_grupos = floor(count($usuarios) / $num);

	$repartir = false;
	if (count($usuarios) % $num != 0) {
		$repartir = true;
	}

	$grupos = [];
	for ($i = 0; $i < $num_grupos; $i++) {
		$grupos[count($grupos)] = array_splice($usuarios, 0, $num);
	}

	if($repartir){
		$pers_rep = count($usuarios); //Personas a repartir
		//$num_grupos //grupos totales
		
		$pers_x_grupo = floor($pers_rep / $num_grupos);
		foreach ($grupos as $x => $value) {
			for ($i=0; $i < $pers_x_grupo; $i++) { 
				$grupos[$x][count($grupos[$x])] = $usuarios[$i];
			}
			array_splice($usuarios, 0, $pers_x_grupo);
		}
		if ($pers_rep % $num_grupos != 0) { //Si las personas x grupo tienen residuo, coloco la que sobra en el ultimo grupo
			$grupos[count($grupos)-1][count($grupos[$x])] = $usuarios[0];
		}
	}



	return $grupos;
}

function print_users($usuarios)
{
	$lista_users = "<h4>Participantes</h4><ul>";

	$lista_users .= "<ul>";
	foreach ($usuarios as $valor) {
		$lista_users .= "<li>" . $valor . "</li>";
	}
	$lista_users .= "</ul>";

	return $lista_users;
}

function print_groups($grupos, $lista)
{

	if ($lista) {
		$lista_grupos = "<h4>Grupos</h4><ul>";
		foreach ($grupos as $x => $grupo) {
			$lista_grupos .= "<li>Grupo " . ($x + 1) . "</li>";
			$lista_grupos .= "<ul>";
			foreach ($grupo as $valor) {
				$lista_grupos .= "<li>" . $valor . "</li>";
			}
			$lista_grupos .= "</ul>";
		}
	} else {
		$lista_grupos = "Grupos:\r\n\r\n";
		foreach ($grupos as $x => $grupo) {
			$lista_grupos .= "Grupo " . ($x + 1) . "\r\n";
			$lista_grupos .= "\r\n\r\n";
			foreach ($grupo as $valor) {
				$lista_grupos .= $valor . "\r\n";
			}
			$lista_grupos .= "\r\n\r\n";
		}
	}

	return $lista_grupos;
}


function enviar_mail($destino)
{
	$cabecera = "from:backend@training.cat";
	$asunto = "Generador de equipos";
	$correo = print_groups($_SESSION["grupos"], false);

	$res = mail($destino, $asunto, $correo, $cabecera);

	return $res;
}


?>