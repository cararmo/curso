<?php
$mens1 = "";
$mens2 = "";
$errdni = "";
$errdniorig = "";
$errdnisust = "";


if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if (isset($_POST["form_dni"])) {

		if (!empty($_POST["dni"])) {
			$dni = htmlspecialchars($_POST["dni"]);
			if (search_dni($dni)) {
				$mens1 = "Adelante, señor con DNI: " . $dni;
			} else {
				$errdni = "Ud no esta en la lista, señor con dni {$dni}, no es bienvenido aqui, FUERA!";
			}
		} else {
			$errdni = "pon un dni";
		}
	}


	if (isset($_POST["form_dni_change"])) {

		if (!empty($_POST["dni_orig"])) {

			$dni_orig = htmlspecialchars($_POST["dni_orig"]);

			if (!empty($_POST["dni_sust"])) {
				$dni_sust = htmlspecialchars($_POST["dni_sust"]);

				if (mb_strlen($dni_sust) == 9) {

					if (change_dni($dni_orig, $dni_sust)) {
						$mens2 = "DNI cambiado correctamente";
					} else {
						$errdnisust = "Error al cambiar dni";
					}
				} else {
					$errdnisust = "Pon un DNI valido";
				}
			} else {
				$errdnisust = "pon un dni a sustituir";
			}
		} else {
			$errdniorig = "pon un dni origen";
		}
	}
}


?>

<!DOCTYPE html>
<html>

<head>

	<style>
		.red {
			color: red;
		}
	</style>

</head>

<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
	<label for="id_dni">DNI</label>
	<input type="text" name="dni" id="id_dni">
	<br>
	<small class="red"><?php echo $errdni; ?></small>
	<br>
	<input type="submit" name="form_dni" value="Enviar DNI"><br>
	<h4><?php echo $mens1; ?></h4>
</form>

<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
	<label for="id_dni_orig">Cambiar DNI</label>
	<input type="text" name="dni_orig" id="id_dni_orig">
	<br>
	<small class="red"><?php echo $errdniorig; ?></small>
	<br>
	<label for="id_dni_sust">Cambiar DNI</label>
	<input type="text" name="dni_sust" id="id_dni_sust">
	<br>
	<small class="red"><?php echo $errdnisust; ?></small>
	<br>
	<input type="submit" name="form_dni_change" value="Enviar DNI">
	<br>
	<h4><?php echo $mens2; ?></h4>
</form>

</html>


<?php

function search_dni($dni_search, &$pos = 0)
{

	$search = false;
	$enlace = fopen("DNI.txt", "r");

	$num = 0;

	while (!feof($enlace) && !$search) {
		$dni = fread($enlace, 9);
		if ($dni_search == $dni) {
			$search = true;
			$pos = ftell($enlace);
		} else {
			fread($enlace, 2);
		}
	}

	fclose($enlace);
	return $search;
}

function change_dni($dni_orig, $dni_sust)
{
	$change = false;
	$pos = 0;

	if (search_dni($dni_orig, $pos)) {
		$enlace = fopen("DNI.txt", "r+");
		fseek($enlace, $pos - 9);
		fwrite($enlace, $dni_sust);
		fclose($enlace);

		$change = true;
	}

	return $change;
}




?>