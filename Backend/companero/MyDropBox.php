<?php
session_start();

if (empty($_SESSION["login"])) {
	$_SESSION["login"] = "";
}

if (empty($_SESSION["dir_actual"])) {
	$_SESSION["dir_actual"] = "MyBox/";
}

define("RAIZ", "MyBox/");
$pwd = '';
$mail_login = '';
$errpwd = '';
$fase = "login";
$mens = "";
$errnamedir = "";
$errupload = "";
$errmail = "";
$ruta_final_ficheros = [];
$namedir = "";
$login = false;
$dir = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {


	if (isset($_POST["login"])) {
		$fase = "login";

		if (!empty($_POST["mail_login"])) {
			$mail_login = htmlspecialchars($_POST["mail_login"]);
			if (empty(filter_var($mail_login, FILTER_VALIDATE_EMAIL))) {
				$errmail = "Pon un correo valido, perro";
			}
		} else {
			$errmail = "Rellena el correo, perro";
		}

		if (!empty($_POST["pwd"])) {
			$pwd = htmlspecialchars($_POST["pwd"]);

			$login = login($mail_login, $pwd);

			if (!$login) {
				$errpwd = "Contraseña o correo incorrecto";
			} else {
				$_SESSION["login"] = $mail_login;
			}
		} else {
			$errpwd = "Rellena la contraseña, perro";
		}
	}

	if (isset($_POST["mkdir"])) {

		if (!empty($_POST["namedir"])) {

			$namedir =  htmlspecialchars($_POST["namedir"]);

			mkdir($_SESSION["dir_actual"] . $namedir);
		} else {
			$errnamedir = "Introduzca un nombre para la carpeta a crear";
		}
	}

	if (isset($_POST["upload"])) {

		if (!empty($_FILES["fichero"]["name"][0])) {

			foreach ($_FILES["fichero"]["name"] as $x => $name_fich) {
				$ruta_final_ficheros[$x] = $_SESSION["dir_actual"] . $_FILES["fichero"]["name"][$x]; //-> Nombre y extension original

				if (move_uploaded_file($_FILES["fichero"]["tmp_name"][$x], $ruta_final_ficheros[$x])) {
					$mens = "Ficheros subidos correctamente";
				}
			}
		}else{
			$errupload = "Adjunte los ficheros que desea subir a la nube";
		}
	}


	if (isset($_POST["delete_file"])) {
		$file = $_POST["file"];

		if (file_exists($file)) {
			unlink($file);
		}
	}

	if (isset($_POST["borrar_carpeta"])) {
		$dir = $_POST["dir"];

		if (count(glob($dir . "/*")) > 0) {
			foreach (glob($dir . "/*") as $file) {
				unlink($file);
			}
		}

		rmdir($dir);
	}

	if (isset($_POST["rename_carpeta"])) {
		$dir = $_POST["dir"];
		$newname = atras($dir).htmlspecialchars($_POST["newname"]);

		rename($_POST["dir"]."/", $newname);
	}



	if (isset($_POST["close_session"])) {

		$_SESSION = [];
		session_destroy();
	}

	if (isset($_POST["entrar_carpeta"])) {
		$dir = htmlspecialchars($_POST["dir"]);
		$_SESSION["dir_actual"] = $dir . "/";
	}



	if (isset($_POST["atras"])) {
		$_SESSION["dir_actual"] = atras($_SESSION["dir_actual"]);
		//$_SESSION["dir_actual"] = dirname($_SESSION["dir_actual"], 1) . "/";
	}
}




if (!empty($_SESSION["login"])) {
	$fase = "mybox";
}
?>

<!DOCTYPE html>
<html>

<head>

	<style>
		.red {
			color: red;
		}
	</style>

</head>

<?php

switch ($fase) {
	case "login":
?>
		<h2>LOGIN</h2>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_mail_login">Correo</label>
			<input type="text" name="mail_login" id="id_mail_login" value="<?php echo $mail_login; ?>">
			<br>
			<small class="red"><?php echo $errmail; ?></small>
			<br><br>
			<label for="id_pwd" tabindex="1">Contraseña</label>
			<input type="password" name="pwd" id="id_pwd" required><br>
			<small class="red"><?php echo $errpwd; ?></small>
			<br><br>
			<input type="submit" name="login" value="Login"><br>
		</form><br>

	<?php
		break;
	case "mybox":
	?>

		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<input type="submit" name="close_session" value="Cerrar Session">
		</form>
		<br><br>
		<h2>My BOX</h2>
		<h3>Ruta actual: <?php echo $_SESSION["dir_actual"] ?></h3>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<input type="text" name="namedir" id="id_namedir" placeholder="Nueva Carpeta">
			<br>
			<input type="submit" name="mkdir" value="Crear Carpeta"><br>
			<small class="red"><?php echo $errnamedir; ?></small><br>
			<label for="id_fichero">Adjuntar ficheros</label>
			<input type="file" name="fichero[]" id="id_fichero" multiple><br>
			<small class="red"><?php echo $errupload; ?></small><br>
			<input type="submit" name="upload" value="Subir a la nube"><br>
			<small><?php echo "<h3>{$mens}</h3>"; ?></small><br><br>
		</form>

<?php

		if (!empty($_SESSION["dir_actual"])) {
			contenido_carpeta($_SESSION["dir_actual"]);
		}

		break;
}


?>

</html>


<?php


function login($mail_login, $pwd)
{

	$logins = [
		["mail" => "prueba@gmail.com", "pwd" => password_hash("1234", PASSWORD_DEFAULT)],
		["mail" => "aa@gmail.com", "pwd" => password_hash("1111", PASSWORD_DEFAULT)],
		["mail" => "bb@gmail.com", "pwd" => password_hash("2222", PASSWORD_DEFAULT)]
	];

	$login = false;
	$mail_ok = false;
	$pwd_ok = false;
	$cont = 0;
	while ((!$mail_ok && !$pwd_ok) && ($cont <= (count($logins) - 1))) {

		foreach ($logins[$cont] as $y => $valor) {
			if ($y == "mail" && $valor == $mail_login) {
				$mail_ok = true;
			}
			if ($y == "pwd" && password_verify($pwd, $valor)) {
				$pwd_ok = true;
			}
		}

		if (!$mail_ok || !$pwd_ok) {
			$mail_ok = false;
			$pwd_ok = false;
		} else {
			$login = true;
		}

		$cont++;
	}

	return $login;
}


function contenido_carpeta($dir)
{
	$form_gestion = "";
	foreach (glob($dir . "*") as $fichero) {
		if (is_dir($fichero)) {
			$form_gestion .= '<form action="' . $_SERVER["PHP_SELF"] . '" method="POST" enctype="multipart/form-data">';
			$form_gestion .= '<input type="submit" name="borrar_carpeta" value="X">';
			$form_gestion .= '<input type="submit" name="entrar_carpeta" value="O">';
			$form_gestion .= '<label>' . $fichero . '</label><br>';
			$form_gestion .= '<input type="text" name="newname" placeholder="Renombrar Carpeta">';
			$form_gestion .= '<input type="submit" name="rename_carpeta" value="R">';
			$form_gestion .=  '<input type="hidden" name="dir" value="' . $fichero . '">';
			$form_gestion .= '</form><br>';
		} else {
			$form_gestion .= '<form action="' . $_SERVER["PHP_SELF"] . '" method="POST" enctype="multipart/form-data">';
			$form_gestion .= '<input type="submit" name="delete_file" value="X">';
			$form_gestion .=  '<a href="' . $fichero . '" download>' . $fichero . '</a>';
			$form_gestion .=  '<input type="hidden" name="file" value="' . $fichero . '">';
			$form_gestion .= '</form><br>';
		}
	}

	if ($dir != RAIZ) {
		$form_gestion .= '<form action="' . $_SERVER["PHP_SELF"] . '" method="POST" enctype="multipart/form-data">';
		$form_gestion .= '<input type="submit" name="atras" value="Atras">';
		$form_gestion .= '</form><br>';
	}

	echo $form_gestion;
}


function atras($dir){
	return dirname($dir, 1) . "/";
}



?>