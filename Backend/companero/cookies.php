<?php

$mail = '';
$errmail = '';
$pwd = '';
$errpwd = '';
$errgeneral = '';
$cont = 0;

validar_formulario($mail, $errmail, $pwd, $errpwd, $errgeneral, $cont);
gestion_cookies($mail, $errpwd, $cont);

?>

<!DOCTYPE html>
<html>

<head>

	<style>
		.red {
			color: red;
		}
	</style>

</head>

<body>

	<?php

	//Si el formulario es correcto muestro formula de la cocacola
	if (!empty($pwd) && empty($errpwd) && !empty($mail) && empty($errmail)) {
		echo "<h1>Formula de la cocacola</h1>";
	} else {
		//Si no es correcto o no se ha enviado, muestro el formulario
		//para mostrar el formulario, cierro php en el else y lo muestro
	?>

		<!-- muestro formulario en HTML dentro del else de PHP -->
		<h1>LOGIN</h1>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_mail">Correo</label>
			<input type="text" name="mail" id="id_mail" value="<?php echo isset($_COOKIE["mail"]) ? $_COOKIE["mail"] : $mail; ?>">
			<br>
			<small class="red"><?php echo $errmail; ?></small>
			<br><br>
			<label for="id_pwd" tabindex="1">Contraseña</label>
			<input type="<?php echo $type_pwd ?>" name="pwd" id="id_pwd" value="<?php echo $pwd; ?>" required>
			<input type="checkbox" name="mostrar_text_js" onclick="show_pwd()">
			<br>
			<small class="red"><?php echo $errpwd; ?></small>
			<br><br>
			<input type="submit" name="form_practica" value="Enviar" <?php echo (isset($_COOKIE["contador"]) && $cont == 0) ? 'disabled = "disabled"' : "" ?>><br><br>
			<small class="red"><?php echo $errgeneral; ?></small>
		</form>
		<h3>Intentos restantes: <?php echo $cont ?></h3>
	<?php
	} //Vuelvo abrir PHP para cerrar el else
	?>


	<script>
		function show_pwd() {
			var pwd = document.getElementById("id_pwd");

			if (pwd.type === "password") {
				pwd.type = "text";
			} else {
				pwd.type = "password";
			}
		}
	</script>



<?php

function validar_formulario(&$mail, &$errmail, &$pwd, &$errpwd, &$errgeneral, &$cont)
{

	$mail_ok = false;
	$pwd_ok = false;
	$logins = [
		["mail" => "prueba@gmail.com", "pwd" => password_hash("1234", PASSWORD_DEFAULT)],
		["mail" => "aa@gmail.com", "pwd" => password_hash("1111", PASSWORD_DEFAULT)],
		["mail" => "bb@gmail.com", "pwd" => password_hash("2222", PASSWORD_DEFAULT)]
	];
	

	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		if (isset($_COOKIE["contador"]) && $_COOKIE["contador"] <= 0) {
			$errgeneral = "Ya lo ha intentado 3 veces amigo, ¿no sera ud. un hacker?";
		} else {

			if (!empty($_POST["mail"])) {
				$mail = htmlspecialchars($_POST["mail"]);
				if (empty(filter_var($mail, FILTER_VALIDATE_EMAIL))) {
					$errmail = "Pon un correo valido, perro";
				}
			} else {
				$errmail = "Rellena el correo, perro";
			}

			if (!empty($_POST["pwd"])) {
				$pwd = htmlspecialchars($_POST["pwd"]);

				$cont = 0;
				while ((!$mail_ok && !$pwd_ok) && ($cont <= (count($logins) - 1))) {

					foreach ($logins[$cont] as $y => $valor) {
						if ($y == "mail" && $valor == $mail) {
							$mail_ok = true;
						}
						if ($y == "pwd" && password_verify($pwd, $valor)) {
							$pwd_ok = true;
						}
					}

					if (!$mail_ok || !$pwd_ok) {
						$mail_ok = false;
						$pwd_ok = false;
					}

					$cont++;
				}

				if (!$pwd_ok || !$mail_ok) {
					$errpwd = "Contraseña o correo incorrecto";
					if (isset($_COOKIE["contador"]) && $_COOKIE['contador'] == 1) { //1 porque aun no lo he restado abajo, este es la ultima vez que puede intentarlo
						$errgeneral = "Ya lo ha intentado 3 veces amigo, ¿no sera ud. un hacker?";
					}
				}
			} else {
				$errpwd = "Rellena la contraseña, perro";
			}
		}
	}
}



function gestion_cookies($mail, $errpwd, &$cont){
	if (!isset($_COOKIE["mail"]) && empty($errmail)) {
		setcookie("mail", $mail, time() + 60 * 60 * 24 * 7);
	}
	
	if (isset($_COOKIE['contador'])) {
		if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($errpwd)) {
			if ($_COOKIE['contador'] > 0) {
				setcookie('contador', $_COOKIE['contador'] - 1, time() + 60 * 60 * 24 * 7);
				$cont = $_COOKIE['contador'] - 1;
			}
		} else {
			$cont = $_COOKIE['contador'];
		}
	} else {
		setcookie('contador', 3, time() + 60 * 60 * 24 * 7);
		$cont = 3;
	}
}
?>


</html>