<?php

$nombre = "";
$sug = "";
$errgeneral = "";
$pwd = '';
$mail = '';
$mail_login = '';
$errpwd = '';
$errgeneral = '';
$mail_ok = false;
$pwd_ok = false;
$fase = "sugerencias_w";
$nom_fichero = "sugerencias.txt";
$mens = "";
$errnombre = "";
$errmail = "";
$errsugerencia = "";


$logins = [
	["mail" => "prueba@gmail.com", "pwd" => password_hash("1234", PASSWORD_DEFAULT)],
	["mail" => "aa@gmail.com", "pwd" => password_hash("1111", PASSWORD_DEFAULT)],
	["mail" => "bb@gmail.com", "pwd" => password_hash("2222", PASSWORD_DEFAULT)]
];

if ($_SERVER["REQUEST_METHOD"] == "POST") {



	if (isset($_POST["enviar"])) {
		$fase = "sugerencias_w";

		if (!empty($_POST["nombre"])) {

			$nombre =  htmlspecialchars($_POST["nombre"]);
		} else {
			$errnombre = "Introduzca un nombre";
		}



		if (!empty($_POST["mail"])) {

			$mail = htmlspecialchars($_POST["mail"]);
			if (!empty(filter_var($mail, FILTER_VALIDATE_EMAIL))) {
				$correo = htmlspecialchars($_POST["mail"]);
			} else {
				$errmail = "Pon un correo valido";
			}
		} else {
			$errmail = "Introduzca una direccion de correo";
		}



		if (!empty($_POST["sugerencia"])) {

			$sug = htmlspecialchars($_POST["sugerencia"]);
		} else {
			$errsugerencia = "Introduzca una sugerencia";
		}
	}


	if (isset($_POST["login_b"])) {
		$fase = "login";
	}


	if (isset($_POST["volver_sugw"])) {
		$fase = "sugerencias_w";
	}


	if (isset($_POST["login"])) {
		$fase = "login";

		if (!empty($_POST["mail_login"])) {
			$mail_login = htmlspecialchars($_POST["mail_login"]);
			if (empty(filter_var($mail_login, FILTER_VALIDATE_EMAIL))) {
				$errmail = "Pon un correo valido, perro";
			}
		} else {
			$errmail = "Rellena el correo, perro";
		}

		if (!empty($_POST["pwd"])) {
			$pwd = htmlspecialchars($_POST["pwd"]);

			$cont = 0;
			while ((!$mail_ok && !$pwd_ok) && ($cont <= (count($logins) - 1))) {

				foreach ($logins[$cont] as $y => $valor) {
					if ($y == "mail" && $valor == $mail_login) {
						$mail_ok = true;
					}
					if ($y == "pwd" && password_verify($pwd, $valor)) {
						$pwd_ok = true;
					}
				}

				if (!$mail_ok || !$pwd_ok) {
					$mail_ok = false;
					$pwd_ok = false;
				}

				$cont++;
			}

			if (!$pwd_ok || !$mail_ok) {
				$errpwd = "Contraseña o correo incorrecto";
			}
		} else {
			$errpwd = "Rellena la contraseña, perro";
		}
	}

	if (isset($_POST["borrar"])) {
		$fase = "sugerencias_r";

		$nom_fichero = "sugerencias.txt";
		if (file_exists($nom_fichero)) {
			$enlace = fopen($nom_fichero, "w");
			fclose($enlace);
		}

		leer_sugerencias($nom_fichero);
	}
}

//Si el login es OK camio de fase y leo y muestro las sugerencias
if (isset($_POST["login"]) && !empty($pwd) && empty($errpwd) && !empty($mail_login) && empty($errmail)) {
	$fase = "sugerencias_r";

	leer_sugerencias($nom_fichero);
}

//Si el formulario esta ok creo fichero
if (isset($_POST["enviar"]) && !empty($nombre) && !empty($mail) && !empty($sug)) {


	$sugerencia = "Nombre: " . $nombre . PHP_EOL . "Correo: " . $mail . PHP_EOL . "Sugerencia: " . $sug . PHP_EOL . "IP: " . $_SERVER["REMOTE_ADDR"] . PHP_EOL . "Fecha: " . date("d/m/Y H:i:s");
	$sugerencia .= PHP_EOL."--------------------------------".PHP_EOL;

	/*
	//Cada sugerencia un fichero nuevo
	$cont = 0;
	$nom_fichero = "sugerencia".$cont.".txt";
	while (file_exists($nom_fichero)) {
		$nom_fichero = "sugerencia".$cont.".txt";
		$cont++;
	}

	$enlace = fopen($nom_fichero, "x");

	fwrite($enlace, $sugerencia);
	fclose($enlace);
	*/


	//Todas las sugerencias en un mismo archivo
	if (file_exists($nom_fichero)) {
		$enlace = fopen($nom_fichero, "a");
	} else {
		$enlace = fopen($nom_fichero, "x");
	}

	fwrite($enlace, $sugerencia);
	fclose($enlace);

	$mens = "Sugerencia enviada!";
}


?>

<!DOCTYPE html>
<html>

<head>

	<style>
		.red {
			color: red;
		}
	</style>

</head>

<?php
switch ($fase) {
	case "login":
?>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<input type="submit" name="volver_sugw" value="Volver"><br>
		</form>

		<h2>LOGIN</h2>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_mail_login">Correo</label>
			<input type="text" name="mail_login" id="id_mail_login" value="<?php echo $mail_login; ?>">
			<br>
			<small class="red"><?php echo $errmail; ?></small>
			<br><br>
			<label for="id_pwd" tabindex="1">Contraseña</label>
			<input type="password" name="pwd" id="id_pwd" required><br>
			<small class="red"><?php echo $errpwd; ?></small>
			<br><br>
			<input type="submit" name="login" value="Login"><br>
		</form>

	<?php
		break;
	case "sugerencias_w":
	?>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<input type="submit" name="login_b" value="Login"><br>
		</form>

		<h2>Envia una sugerencia</h2>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<label for="id_nombre">Nombre</label>
			<input type="text" name="nombre" id="id_nombre">
			<br>
			<small class="red"><?php echo $errnombre; ?></small><br>
			<label for="id_mail">Correo</label>
			<input type="email" name="mail" id="id_mail">
			<br>
			<small class="red"><?php echo $errmail; ?></small><br>
			<label for="id_sug">Usuario</label>
			<textarea name="sugerencia" id="id_sug" minlength="1" maxlength="255" placeholder="Escribe una descripcion"></textarea>
			<br>
			<small class="red"><?php echo $errsugerencia; ?></small><br>
			<input type="submit" name="enviar" value="Enviar"><br>
			<small><?php echo "<h3>{$mens}</h3>"; ?></small><br>
		</form>
		<br>

	<?php
		break;
	case "sugerencias_r":
	?>
		<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
			<input type="submit" name="login_b" value="Volver"><br>
			<input type="submit" name="borrar" value="Borrar"><br>
		</form>


<?php
		break;
}
?>

</html>


<?php

function leer_sugerencias($nom_fichero)
{

	
	if (file_exists($nom_fichero) && filesize($nom_fichero) > 0) {

		$enlace = fopen($nom_fichero, "r");


		echo "<h2>Sugerencias</h2>--------------------------------";
		while (!feof($enlace)) {
			$linea = fgets($enlace);
			$pos = strpos($linea, ":");
			if($pos > 0){
				$linea = "<strong>".substr_replace($linea, ':</strong>', $pos, 1);
			}
			echo "<p>{$linea}</p>";
		}

		fclose($enlace);
	} else {
		echo "<h1>No hay sugerencias</h1>";
	}

}


?>