<?php

	function compareAssociativeArray($array1, $array2)
	{
		$ok = [];
		$semiok = [];
		$ko = [];

		foreach ($array1 as $x => $valor) {
			if (isset($array2[$x])) { //compruebo si la clave de array1 existe en array2
				($array2[$x] == $valor) ? $ok[$x] = $valor : $semiok[$x] = $valor;
			} else {
				$ko[$x] = $array1[$x];
			}
		}

		ksort($ok);
		ksort($semiok);
		ksort($ko);

		return ["ok" => $ok, "semiok" => $semiok, "ko" => $ko];
	}

	function printCompareAssociativeArray($printArray){
		$table = '<table class="table">';
		foreach ($printArray as $i => $array){
			foreach ($array as $x => $valor) {
				$table .= "<tr>";
				switch ($i) {
					case "ok":
						$table .= '<td class="verde">';
						$table .= $x . "</td>";
						$table .= '<td class="verde">';
						$table .= $valor . '</td>';
						break;
					case "semiok":
						$table .= '<td class="verde">';
						$table .= $x . "</td>";
						$table .= '<td class="rojo">';
						$table .= $valor . '</td>';
						break;
					case "ko":
						$table .= '<td class="rojo">';
						$table .= $x . "</td>";
						$table .= '<td class="rojo">';
						$table .= $valor . '</td>';
						break;
				}
				$table .= "</tr>";
			}
		}
		$table .= '</table>';
		echo $table;
	}

	//







?>