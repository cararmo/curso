<?php

session_start();


$array1 = ["grillo" => "crick", "pato" => "quack", "loro" => "croack", "perro" => "guau", "serpiente" => "fff", "gato" => "miau", "vaca" => "muuu"];
$array2 = ["perro" => "wof", "cerdo" => "oink", "pato" => "quack", "gallo" => "kikiriki", "leon" => "roarr", "gato" => "miau"];

if (empty($_SESSION["array1"])) {
	$_SESSION["array1"] = $array1;
}
if (empty($_SESSION["array2"])) {
	$_SESSION["array2"] = $array2;
}

$errgeneral1 = "";
$errgeneral2 = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if (isset($_POST["ins_array1"])) {

		if (isset($_POST["animal1"]) && isset($_POST["sonido1"])) {

			insert_animal($_SESSION["array1"], $_POST["animal1"], $_POST["sonido1"], $errgeneral1);
		}
	}
	if (isset($_POST["ins_array2"])) {
		if (isset($_POST["animal2"]) && isset($_POST["sonido2"])) {

			insert_animal($_SESSION["array2"], $_POST["animal2"], $_POST["sonido2"], $errgeneral2);
		}
	}
	if (isset($_POST["del_array1"])) {
		if (isset($_POST["animal1"]) && isset($_POST["sonido1"])) {

			delete_animal($_SESSION["array1"], $_POST["animal1"], $_POST["sonido1"], $errgeneral1);
		}
	}
	if (isset($_POST["del_array2"])) {
		if (isset($_POST["animal2"]) && isset($_POST["sonido2"])) {

			delete_animal($_SESSION["array2"], $_POST["animal2"], $_POST["sonido2"], $errgeneral2);
		}
	}
	if (isset($_POST["close_session"])) {
		
		session_destroy();

	}
}


require_once("libreria.php");



?>

<!DOCTYPE html>
<html>

<head>

	<style>
		.table {
			border: 1px solid black;
			border-collapse: collapse;
			text-align: center;
		}

		.rojo {
			border: 1px solid black;
			border-collapse: collapse;
			text-align: center;
			background-color: red;
		}

		.verde {
			border: 1px solid black;
			border-collapse: collapse;
			text-align: center;
			background-color: green;
		}

		.red {
			color: red;
		}
	</style>

</head>
<h1>LABORATORIO DE COMPARACIÓN</h1>

<?php
//Imprimo tabla1
$res = compareAssociativeArray($_SESSION["array1"], $_SESSION["array2"]);
printCompareAssociativeArray($res);
?>

<br>
<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
	<label for="id_animal1">Animal</label>
	<input type="text" name="animal1" id="id_animal1">
	<br>
	<label for="id_sonido1" tabindex="1">Sonido</label>
	<input type="text" name="sonido1" id="id_sonido1">
	<br><br>
	<input type="submit" name="ins_array1" value="Inserta">
	<input type="submit" name="del_array1" value="Elimina"><br><br>
	<small class="red"><?php echo $errgeneral1; ?></small>
</form>
<br>

<?php
//Imprimo tabla 2
$res = compareAssociativeArray($_SESSION["array2"], $_SESSION["array1"]);
printCompareAssociativeArray($res);
?>

<br>
<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
	<label for="id_animal2">Animal</label>
	<input type="text" name="animal2" id="id_animal2">
	<br>
	<label for="id_sonido2" tabindex="1">Sonido</label>
	<input type="text" name="sonido2" id="id_sonido2">
	<br><br>
	<input type="submit" name="ins_array2" value="Inserta">
	<input type="submit" name="del_array2" value="Elimina"><br><br>
	<small class="red"><?php echo $errgeneral2; ?></small>
	<input type="submit" name="close_session" value="Cerrar sesion"><br><br>
</form>

</html>


<?php

function insert_animal(&$array, $animal, $sonido, &$errgeneral)
{

	if (!isset($array[$animal])) {
		$array[$animal] = $sonido; //si no existe lo creo
	} else {
		if ($array[$animal] == $sonido) {
			$errgeneral = "El animal con dicho sonido ya existe"; //Si existe con el mismo valor informo de ello
		} else {
			$array[$animal] = $sonido; //Si existe pero con otro valor lo machaco por el actual
		}
	}
}

function delete_animal(&$array, $animal, $sonido, &$errgeneral)
{
	if (!isset($array[$animal])) {
		$errgeneral = "El animal no existe, no se pudo eliminar"; //Si no existe informo de que no puedo eliminarlo
	} else {
		if ($array[$animal] == $sonido) {
			//Si existe con el mismo valor lo elimino
			unset($array[$animal]);
		}else{
			$errgeneral = "El animal existe pero su sonido no, no se pudo eliminar";
		}
	}
}
?>