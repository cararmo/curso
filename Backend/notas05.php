
//ingresar 

// eliminar bd
drop databases if exists ombredb;

//consideraciones al crear BD

 -El set de caracteres (charset) especifica que caracteres podran almacenrase en la BD.
  normalmente se elige UTF8.

 -sino se declara, seleccionara uno en funcion del idioma del sistema operativo (por ejemplo latin1) y
  provocara errores al interactuar con la pagaina web (que usa utf8).

 -create database nombredb default character set utf8;

 - si se quiere almacenar algunos caracterrs especiales (emoticoos ..)ocupan mas que un caracter normal (4 bytes)
   se utiliza el utf8mb4, (solo usa 4 bytes si los necesita.

- la copicon COLLATE, especifica como se comparan las cadenas de texto(por ej. para ordenar busquedas).

  utf8mb4_unicode_ci -> contiene las reglas para comparar y ordenar la mayoria de idiomas.

	create database nombredb
	default character set utf8mb4
	collate utf8mb4_unicode_ci;

- para mostrar las bd existentes se usa:
  show databases;

- para seleccionar la bd que se utilizará se usa use nombredb;

- para crear una tabla se usa el comando:
    create table nombretabla (
	    nombrecol1 tipodesatos detalles,
	    nombrecol2 tipodesatos detalles,
	    nombrecol3 tipodesatos detalles,
	    ...
    );

// tipos de datos para cadenas.

  char(x) nro fijo de caracteres            varchar(x) (max 64kb)(indexable)
  tinytext(max 255 bytes)                   text (max 64kb)
  mediumtext (max 16 MB)                    longtext(max 4GB)

// tipos de datoas para numeros enteros.


  tinyint 1b [-128, 127]                    unsigned tinyint [0,255]
  smallint 2b [-32k, 32k ]                  mediumint 3b [-8m,8m]
  int 4b [-2b, 2b]                          bigint 8b [-9z,9z]

// el boleano

    se usa BOOL o BOOLEAN  aunque en realizad son tinynt, pero permiten usar true/false como valor, 
    (se almacenan como 0/1). 

// tipos de datos para numeros con decimales.

decimal (p,s)  decimal(9,2) => 1234567.89

dec               
numeric          ==> son sinonimos de DECIMAL, mejor usar deciamal.
fixed
  
FLOAT   4b  La preciison máxima del float es 23.
DOUBLE  8b La precision máxima del double es 53. 

    float y double NO son 100% exactos.
    float y double tambien permiten especificar (p, s)
    float es más preciso hasta aproximadamente 7 cifras despues de la coma.

// Tipos de datos para fechas

DATE

// EJEMPLO CREAR TABLA:

  CREATE TABLE personas (
    idpersona INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    puntos INT UNSIGNED NOT NULL DEFAULT 0,
    ceado DATETIME DEFAULT CURRENT_TIMESTAMP,
    modificado DATETIME ON UPDATE CURRENT_TIMESTAMP
  );

// mostrar tablas de una bd. 

  SHOW TABLES;

// borrar una tabla en la BD seleccionada.

  DROP TABLE personas;

// Mostrar estructura de la tabla.

  DESCRIBE tabla;

// Modificar estructura de una tabla (modificar columnas), NO se recomienda hacerlo. 

  ALTER TABLE permite 
  añadir, modificar o eliminar
  columnas de una tabla.

  ALTER TABLE personas
  ADD pass VARCHAR(256) NOT NULL  AFTER nombre,
  MODIFY COLUMN email VARCHAR(128) not null;
  DROP apellido;

<?// insertar informacion a la tabla. ?>

se usa:

  INSERT INTO nombre_tabla(col3,col1,..)
  VALUES (val3, val1,...);

  INSERT INTO personas(nombre,pass,apellido,email,puntos)
  VALUES ('Carlos Arturo','Mora Muñoz','cararmo@yahoo.com',5);

// para insertar varios registro a la vez:

  VALUES (val1,val2),(val3,val4),(val5,val6)...;

  INSERT INTO personas(nombre,pass,apellido,email,puntos)
  VALUES ('Luis','4321','Perez','prueba@yahoo.com',3),
  ('Angela','1234','Ruiz','prueba2@yahoo.com',4);

<? // LECTURA DE LOS DATOS DE UNA TABLA ?>

    Se utiliza la funcion SELECT.

    SELECT * FROM nom_tabla;
    Select col2,col1,col3 FROM nom_tabla;

    <?// restringir resultado de la consulta ?>

  se utiliza la instruccion WHERE.

    SEELCT * FROM tabla WHERE col_id=1;

// se puede usar AND y OR y parentesis.

  SELECT * FROM usuarios WHERE (nombre='narcis'OR nombre='siso')AND email='siso@cuarteto.es';

  // Filtrado con LIKE.

  - se puede filtrar igualando a cualquiera de los valores de una lista con IN().
    Esto equivale a OR para multiples valores pero mas compacto.

  SELECT * FROM personas
  WHERE nombre In ('pepe','juan','mario');

  devuelve los registros cuyo nombre coincida con pepe, juan o mario.

  // además se puede filtrar con determinadas restricciones.

    SELECT * FROM personas
    WHERE nombre LIKE '%e_e%';

    % equivale a cualquier número de caracteres (cualuqiera).
    _ equivale excatamente a 1 caracter (cualquiera).

  // Se pueden combinar los filtros.

  SELECT * FROM personas
    WHERE nombre LIKE '%e_e%';

<?// ORDENAR RESULTADOS 09/03/22  ?>

  - Para ordenar los rsultados de forma aleatoria se usa la funcion rand().
      SELECT nombre, apellido FROM personas ORDER BY rand();

<?// LISTAR VALORES DIFERENTES ?>

  - SELECT DISTINC muestra los valores diferentes.
  - si se leccionan varias columnas, muestra solo combinaicones diferentes.

      SELECT DISTINC nombre,apellido FROM personas;

<?// LIMITAR ELEMENTOS A MOSTRAR ?>

  Se pude solicitar una cantidad máxima de filas usando LIMIT.

    SELECT * FROM personas ORDER BY apellido LIMIT 3;

<?// RANGO DE RESULTADOS ?>

  - se puede solicitar un rango de filas usando unoffset y un limite.

      SELECT * FROM personas ORDER BY apellido;
      LIMIT 2,3 (offset,limite) => se salta las 2 primeras filas y devuelve las 3 siguientes.


<?// FUNCIONES AGREGADAS ?>

  - las funciones agregadas permiten obtener datos estadisticos del conjunto de campos de una columna.

    SELECT 
      MAX(puntos),
      MIN(ountos),
      AVG(puntos),
      SUM(puntos),
      COUNT(idpersona), cuenta el nro de flas (no NULL)
    FROM personas;

<?// MODIFICAR REGISTROS ?>

  - Se modifican registros con UPDATE.
      Es imprescindible usar WHERE adecuadamente en caso contrario, se podria estropear todos los registros.

      UPDATE personas SET
        apellido='vega',
        email=mrvega@gmail.com'
      WHERE
        id_persona=3;
    
<?// BORRAR REGISTROS  ?>

  - para borrar regstros se hace con DELETE FROM, e igualmente es imprescidible usar WHERE adecuadamente, 
    en caso contrario podriamos borrar todos los registros.

      DELETE FROM nom_tabla WHERE condicion;

      DELETE FROM clientes WHERE id_cliente=1;

<?// CREAR USAURIOS ?>

  - se debe crear un usuario independiente para cada bd por seguridad, con sus respectivos permisos.

      CREATE USER 'nombre'@'localhost' IDENTIFIED BY 'password';

  // para una bd externa se utiliza @%.

      CREATE USER 'nombre'@'%' IDENTIFIED BY 'password';
  
  <?// modificar la contraseña ?>

    - se utiliza la opcion AlTER USER.

      ALTER USER 'nombreusuario'@'localhost' IDENTIFIED BY 'newpassword';

<?// ELIMINAR USUARIOS ?>

  - para eliminar usuarios se utiliza DROP_USER

      DROP USER 'usuario_prueba'@'localhost';

<?// OTORGAR PRIVILEGIOS ?>

  - para otorgar priviligeios a un usuario se usa GRANT PRIVILEGES

      GRANT ALL PRIVILEGES ON *(TODAS LAS BD).*(TODAS SUS TABLAS) TO 'nombre'@'localhost' WITH GRANT OPTION (iguala los permisos de root de la bd);

      GRANT ALL PRIVILEGES ON personas(BD personas).*(Todas sus tablas) TO 'nombre'@'localhost';
       
      ALL PRIVILEGES: todos los privilegios.
      CREATE: permite crear nuevas tablas o bd.
      ALTER: permite modificar tablas o bd.
      DROP: permite eliminar tablas o bd.
      DELETE: permite eliminar registros de tablas.
      INSERT: permite insertar registros en tablas.
      SELECT: permite leer registros en tablas.
      UPDATE: permite actualizar registros seleccionados en tablas.

      GRANT SELECT ON personas.* TO 'nombre'@'lcoalhost';
      GRANT ALL PRIVILEGES ON *.* TO 'nombre'@'lcoalhost';
      

<? // RELACIONES ENTRE TABLAS 10/03/22   ?>

  - relacionamos tablas con una columna en común.

  - se indica las dos columnas relacionadas usando una lalve extranjera (foreing key) en la columna que tiene los dtaos de la otra tabla.

    CREATE TABLE reservas(
      idReserva INT PRIMARY KEY AUTO_INCREMENT,
      idPersona INT NOT NULL,
      entrada DATE NOT NULL,
      salida DATE NOT NULL,
      FOREING KEY fkReservasPersonas (idPersona)
      REFERENCES personas (idPersona)
    );
    
<?    // SELECT DE MULTIPLES TABLAS ?>

    - para combinar datos de dos tablas diferentes se usa JOIN.

      SELECT * FROM  personas JOIN reservas
      ON  personas.idPersona=reservas.idPersona;

    - con JOIN solo se obitne las filas que tienene datos en las dos tablas,
    si se requiere todos los resultados de un lado de la tabla, se usa LEFT JOIN /RIGHT JOIN

      SELECT nombre,apellido, entrada, salida 
      FROM  personas LEFT JOIN reservas
      ON personas.id_persona=reservas.id_personas;

      