<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Mi primera web en PHP</h1>
    <p>
        <?php
            echo "Texto procedente de PHP";
            echo("<h2>Me encanta el PHP!</h2>");
            echo "<p>Hola Mundo!! sin parentesis</p>";
            echo "<p>","varios ","textos ","concatenados ","</p>";
            
            $texto="texto de prueba <br><br>";
            echo $texto;
            
            $test="prueba con comillas dobles";
            echo "texto para hacer {$test}s. <br><br>";
            
            $test="prueba con comillas simples";
            echo 'texto para hacer {$test}s.' ;
            echo "<br><br>";
            $texto="Texto ". "concatenado con puntos "."de "."prueba.";
            echo $texto. "<br><br>";

            $texto="Texto unido con .= de ";
            $texto .="prueba. <br><br>";
            echo $texto;

            // funcion de reeemplazo de texto
            echo str_replace ("world","dolby","Hello world ");

            // funcion para eliminar espacios de principio y final de una cadena
            echo trim ("  Borra espacios del principio y final  de una cadena de texto \n" );

            echo trim(" *Borra.*.otros...",".*\n \t");

            echo ltrim(" Borra esapcios al prinicpio del texto");
            echo rtrim(" Borra esapcios al final del texto");

            // cobnvertir a mayusculas y minusculas
            echo mb_strtoupper("hola mayusculas!");
            echo mb_strtolower("HOLA MINUSCULAS");

            echo ucfirst("hola primera letra en mayusculas \n" );
            echo ucwords("Hola Cada priemra Letra en Minusculas \n" );

            echo strlen("Hola longitud de la cadena" );

            

        ?>
    </p>
</body>
</html>
