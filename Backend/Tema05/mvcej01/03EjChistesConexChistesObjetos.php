<?php
/*
CREATE TABLE chistes (
idchiste INT PRIMARY KEY AUTO_INCREMENT,
nomchiste varchar(100),
deschiste varchar(300)
); 

INSERT INTO chistes (nomchiste,deschiste)
values ('chistes01','erase una vez...'),
    ('chiste02','erase dos veces..'),
    ('chiste03','erase tres veces..'),
    ('chiste04','erase cuatro veces..');

$chistes = [['titulo'=>'chistes01','chiste'=>'blabla'],['titulo'=>'chiste02','chiste'=>''],['titulo'=>'','chiste'=>'']];
); 
*/
// ------ modelo -------

class MChistes{

    function dameChistes()
    {
        $dbUser='root';
        $dbPass='';
        $dbHost='localhost';
        $dbName='chistes';
    
        if($dblink=new mysqli($dbHost,$dbUser,$dbPass,$dbName))
        {
            $dblink->set_charset('utf8mb4');
            $query="SELECT * FROM chistes";
            $resultados = $dblink -> query($query);
            $todo=[];
            if ($resultados -> num_rows > 0)
            {
                while($fila=$resultados->fetch_assoc())
                {
                    $todo[]=$fila;
                }
            }

            return $todo;
        }
    }   

}

// ----- Fin Modelo --------


//--- Vistas
class pagina
{
    public $datos=[];

    function todo()
    {
        $this->impHead();
        $this->impBody();
        $this->impFooter();

    }
    function css()
    {?>
        <style>
            .headerGenerico            {
                text-align: center;
                background-color: gray;
                color:honeydew;
                
            }
        </style>
    <?php
    }
    function impBody()
    {?>
            <header class="headerGenerico">
                MENU DE OPCIONES HEADER GENERICO
            </header>
            <p>esto es un parrafo interno funct impBody</p>
            <a href="pagina2.html" target="_blank">pagina2</a>
        <footer>
             <p>footer generico</p>
        </footer>
       
    <?php                                                                                                          
    }
    function impHead()
    {?>
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <?php $this->css();?>
                
                <title>Cabecera generica</title>
            </head>
            <body>
    <?php
    }
    
    function impFooter()
    {?>
        </body>
        </html>

    <?php
    }
}

class PaginaChistes extends pagina
{
    function impBody()
    {?>
        <header class="headerGenerico">Cabecera de pagina chistes</header>
        <p>parrafos de pagina chistes</p>
        <?php
        foreach($this->datos['chistes'] as $chistesdos)
        {
                echo $chistesdos['deschiste'].'<br>';
            
        }
        //echo '<pre>';
        //print_r($this->datos['chistes']);

    }
    function impFooter()
    {?>
        <p>footer de los chistes</p>
    <?php
    }          
}
//-----Fin Vistas -------


//-----controlador ------

$modelo = new MChistes();
$chistes=$modelo->dameChistes();
//echo '<pre>';
//print_r($chistes);
//echo '</pre>';

$pagina = new paginaChistes();
$pagina->datos['chistes']=$chistes;
$pagina->todo();

//-----fin controlador ------


?>