<?php 
    if(isset($_COOKIE['user'])&&isset($_COOKIE['email']))
    {
        $nombre=$_COOKIE['user'];
        $email=$_COOKIE['email'];
    }
    else
    {
        $caducidad=time()+(60*60*24*7); // 1semana (segundos)
        setcookie('user','juan',$caducidad);
        setcookie('correo','inicioGarvia',$caducidad);
        $user='';
        $email='';
    }
    echo $user .'<br>';
    echo $email .'<br>';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    .errPass{
        background-color:aqua;
    }
    .errEmail{
        background-color:violet;
    }
    .normal{
        background-color: white;
    }
</style>
<?php


//    $users=['camo'=>'1234','cama'=>'4321','otro'=>'1127','yopo'=>'2312'];
    $users=['camo'=>'$2y$10$p2uNzzl8zeOnC3mLu7h2ROjEfN8WLnJ30pisseOXGkfajwL4icwbO',
            'cama'=>'$2y$10$zWbkvvD60xzynCEcBpIwpeuz1uQ6v7i316UJ7/yY6g/Mqge0YIEja',
            'otro'=>'$2y$10$M788zBX1HR0LLV0tbqjGDu76ykY/.BJ4fH2xxlNF7cyqQnI7H8tZC',
            'yopo'=>'$2y$10$3ps/wPJcQ3h9zlflmv1SZeeVQQyGmhDpZB.Mcb3qdLtrx0Q92/.RS'];

   // $has01='$2y$10$p2uNzzl8zeOnC3mLu7h2ROjEfN8WLnJ30pisseOXGkfajwL4icwbO';
   // $has02='$2y$10$zWbkvvD60xzynCEcBpIwpeuz1uQ6v7i316UJ7/yY6g/Mqge0YIEja';
   // $has03='$2y$10$M788zBX1HR0LLV0tbqjGDu76ykY/.BJ4fH2xxlNF7cyqQnI7H8tZC';
   // $has04='$2y$10$3ps/wPJcQ3h9zlflmv1SZeeVQQyGmhDpZB.Mcb3qdLtrx0Q92/.RS';

   /*
insert into usuarios (user,email,passwd)
values ('camo','cararmo@yahoo.com','$2y$10$p2uNzzl8zeOnC3mLu7h2ROjEfN8WLnJ30pisseOXGkfajwL4icwbO'),
    ('cama','yoyo@gmail.com','$2y$10$zWbkvvD60xzynCEcBpIwpeuz1uQ6v7i316UJ7/yY6g/Mqge0YIEja'),
    ('yopo','yoyo@gmail.com','$2y$10$3ps/wPJcQ3h9zlflmv1SZeeVQQyGmhDpZB.Mcb3qdLtrx0Q92/.RS');

*/

    $user='';
    $email='';
    $lpasswd='';
    $errUser='ingresa usuario';
    $errEmail='ingresa correo';
    $errPass='ingresa passwd';
    $login=false;
    // se verifica que sea metodo post
    if ($_SERVER['REQUEST_METHOD'] =='POST')
    {   
        $user=$_POST['user'];
        $lpasswd=$_POST['lpasswd'];
        
       // echo $user.' inicial <br>';
       // echo $lpasswd.' inicial <br>';

        if(!empty($_POST['user']) && !empty($_POST['lpasswd']))
        {
            $dbUser='root';
            $dbPass='';
            $dbHost='localhost';
            $dbName='tienda';
            // creamos la conexion
            if($dblink=mysqli_connect($dbHost,$dbUser,$dbPass,$dbName))
            {
                mysqli_set_charset($dblink,'utf8mb4');
                echo 'conectado <br>';
                $query="SELECT passwd FROM usuarios WHERE idpersona='{$_POST["user"]}'";
                $resultados=mysqli_query($dblink,$query);
                if(mysqli_num_rows($resultados)==1)
                {
                    $contra_array=mysqli_fetch_assoc($resultados);
                    if(password_verify($_POST['lpasswd'], $contra_array['passwd']))
                    {
                        echo "has pasado";
                    }
                    else
                    {
                        echo 'usaurio /contraseña invalidos <br>';
                    }
                }
                else
                {
                    echo 'usaurio /contraseña invalidos <br>';
                }
            }else{
                echo 'error de conexión <br>';
            }
        }
        else
        {
            $errUser="usuario  y password obligatorios";

        }
        if(!empty($_POST['email']))
        {
            if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
            {
                $email=$_POST['email'];
            }
            else
            {
                $errEmail="correo invalido";
            }
        }
        else
        {
            $errEmail="correo obligarotio";

        }
    }
        else
    {
        echo 'he recibido GET'; 
    }
    

?>

<body>
    <?php
    if ($login){
        echo 'HAS PASAO !! <br><br>' ;
    }else
    {
    ?>
    
    <h1>    FORM para ingresar al Campus</h1>

    <!-- para hacer oligatorio un campo, se usa el atributo"required", en 
    este caso, si no se rellena, no deja enviar el formulario.

    con el elemento "label" se especifica una relación entre el texto y el campo 
    que esta a continucación, se debe porner en el atributo "for" yigual al "id" del campo -->

    <form enctype="multipart/form-data" action='<?php echo  htmlspecialchars($_SERVER['PHP_SELF']);?>' method="POST"> 
        <fieldset>
            <legend>TUS DATOS:</legend>

            <input type="text" id="user" name="user" class="<?php echo ($errUser =='ingresa usuario')?'normal': 'errPass' ?>" placeholder="<?php echo $errUser ?>" value="<?php echo empty($user)?"":$user; ?>"><br><br>
            <input type="text" id="email" name="email" class="<?php echo ($errEmail =='ingresa correo')?'normal': 'errEmail' ?>" placeholder="<?php echo $errEmail ?>" value="<?php echo empty($email)?"":$email; ?>"><br><br>
              <!--  <input type="password" id="lpasswd" class="lpass" name="lpasswd" placeholder=" <?php //echo $errPass ?>" value="<?//php echo empty($lpasswd)?"":$lpasswd; ?>"><br><br> -->        
            <input type="password" id="lpasswd" class="<?php echo ($errPass =='ingresa passwd')?'normal': 'errPass' ?>" name="lpasswd" placeholder="<?php echo $errPass ?>" value="<?php echo empty($lpasswd)?"":$lpasswd; ?>"><br><br>

        
                <input type="submit" name="btnvalidar" value="Entrar">

        </fieldset>
    </form>
    <?php
    }
    ?>

</body>
</html>