-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-03-2022 a las 18:56:18
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adjuntos`
--

CREATE TABLE `adjuntos` (
  `idadjunto` int(11) NOT NULL,
  `idsugerencia` int(11) NOT NULL,
  `rutaadjunto` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `adjuntos`
--

INSERT INTO `adjuntos` (`idadjunto`, `idsugerencia`, `rutaadjunto`) VALUES
(1, 51, ''),
(2, 52, 'sugerencias/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

CREATE TABLE `reservas` (
  `idReserva` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `entrada` date NOT NULL,
  `salida` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `reservas`
--

INSERT INTO `reservas` (`idReserva`, `idPersona`, `entrada`, `salida`) VALUES
(1, 2, '2022-03-22', '2022-04-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sugerencias`
--

CREATE TABLE `sugerencias` (
  `idsugerencia` int(11) NOT NULL,
  `usuario` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `descsugerencia` varchar(256) NOT NULL,
  `creado` datetime DEFAULT current_timestamp(),
  `modificado` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sugerencias`
--

INSERT INTO `sugerencias` (`idsugerencia`, `usuario`, `email`, `descsugerencia`, `creado`, `modificado`) VALUES
(48, 'camo', 'pri@pri.com', 'uno', '2022-03-11 18:19:56', NULL),
(49, 'camo', 'cararmo@yahoo.com', 'dos', '2022-03-11 18:20:12', NULL),
(50, 'camo', 'pri@pri.com', 'tres', '2022-03-11 18:33:14', NULL),
(51, 'camo', 'pri@pri.com', 'tres', '2022-03-11 18:51:43', NULL),
(52, 'camo', 'pri@pri.com', 'cuatro', '2022-03-11 18:52:28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(10) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `passwd` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `idPersona`, `email`, `passwd`) VALUES
('cama', 1, 'yoyo@gmail.com', '$2y$10$zWbkvvD60xzynCEcBpIwpeuz1uQ6v7i316UJ7/yY6g/Mqge0YIEja'),
('camo', 2, 'cararmo@yahoo.com', '$2y$10$p2uNzzl8zeOnC3mLu7h2ROjEfN8WLnJ30pisseOXGkfajwL4icwbO'),
('yopo', 3, 'yoyo@gmail.com', '$2y$10$3ps/wPJcQ3h9zlflmv1SZeeVQQyGmhDpZB.Mcb3qdLtrx0Q92/.RS');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adjuntos`
--
ALTER TABLE `adjuntos`
  ADD PRIMARY KEY (`idadjunto`),
  ADD KEY `fk_adjuntos_sugerencias` (`idsugerencia`);

--
-- Indices de la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD PRIMARY KEY (`idReserva`),
  ADD KEY `fkReservasPersonas` (`idPersona`);

--
-- Indices de la tabla `sugerencias`
--
ALTER TABLE `sugerencias`
  ADD PRIMARY KEY (`idsugerencia`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idPersona`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adjuntos`
--
ALTER TABLE `adjuntos`
  MODIFY `idadjunto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `reservas`
--
ALTER TABLE `reservas`
  MODIFY `idReserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sugerencias`
--
ALTER TABLE `sugerencias`
  MODIFY `idsugerencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `adjuntos`
--
ALTER TABLE `adjuntos`
  ADD CONSTRAINT `fk_adjuntos_sugerencias` FOREIGN KEY (`idsugerencia`) REFERENCES `sugerencias` (`idsugerencia`);

--
-- Filtros para la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD CONSTRAINT `fkReservasPersonas` FOREIGN KEY (`idPersona`) REFERENCES `usuarios` (`idPersona`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
