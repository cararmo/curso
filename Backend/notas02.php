<?php

// PROGRAMACION CON BUCLES

    - usamos un bucle cuando necesitams repetir varias veces un bloque de codigo.
    - es imprescindible asegurarse de que el bucle alcanza un valor de salida.

// BUCLE WHILE

    - Ejecutará repetidamente un bloque de codigo mientras que la condicion se siga cumpliendo.
    - con la instruccion BREAK; sale del bucle en cualquier momento.
    - con la instruccion CONTINUE; se salta directamente a la siguiente iteracion
        del bucle. No se ejecutara la parte del codigo que haya despues del continue.

        while (condicion sea true){
        codigo a ejecutar;
        }
  

// BUCLE DO..WHILE

    - este bucle ejecuta el codigo al menos una vez y lo repite mientras la condicion sea true.

    <?php
        do
        {
            codigo a ejecutar;
        }
        while(condicion sea true);
    ?>

// BUCLE FOR 
    - el bucle for permite crear toda la estructura de bucle 
        en una sola linea 
        <?php

        for (inicializacion; condicion;incremento){
            codigo a ejecutar;
        }
        >?
 // VECTOR - ARRAY
 
    - permite almacenar multiples valores dentro de una sola variable.

    - Es como un carpeta que tiene compartimientos, y en cada compartimiento se pueden guardar otras cosas.
    - para accesar cada compartimiento se identifica con un incide.

    <?php
    $coches=[];
    $coches[0]='Volvo';
    $coches[1]='Bmw';
    $coches[2]='Toyota';

    echo $coches[0].'<br>' ;
    echo $coches[1].'<br>';
    echo $coches[2];
     ?>

    <?php

    $coches=['Volvo','Bmw','Toyota'];

    echo $coches[0].'<br>' ;
    echo $coches[1].'<br>';
    echo $coches[2];

    ?>

// print_r permite visualizar tdoo el contenido de un array. es especial para arrays

    $coches=['Volvo','Bmw','Toyota'];
    PRINT_R($coches);
    ?>


// COUNT EN PHP * para saber la longitud del array
    
    echo count ($coches);

// opereadores logicos para VECTORES, se pueden usar casi los mismos

  == igualdad $x==$y

// funcion para borrar una variable

        - con la funcion unset() permite eliminar cualquier variable independiente o en el interior de un array.

        $numeros=[65,77,22,6];
        unset($numeros[count(numeros)-1]);
        var_dump($numeros);
        unset($numeros);

        solo se puede usar unset para borrar la ultima posicion de un array.
        si borramos cualquier otra Posicion,quedara un agujero


// FUNCION PARA INSERTAR EN EL ARRAY

        - La funcion array_push()permite insertar varios elementos al final del vector.
        $numeros=[65,77,22,6];
        array_push($numero,33,125,78);
        print_r($numeros);

        - Para insertar un nuevo elemento o varios AL PRINCPIO DEL VECTOR.
        $numeros=[65,77,22,6];
        array_unshift($numero,33);
        print_r($numeros);

// FUNCION PARA EXTRAER DEL ARRY

        - la Funcion array_pop()elimina el ultimo elemento del vector.
        $numeros=[65,77,22,6];
        array_pop($numeros); //y si se quiere guaradar en una variable esa posicion entonces
        // $posBorrada= array_pop($numeros);
        print_r($numeros);

// FUNCION PARA EXTRAER DEL ARRY AL PRINCIPIO

        array_shift($numero)
        print_r($numeros)

// FUNCION PARA EXTRAER UNO O VARIOS ELEMENTOS CONTINUOS

        con la funcion array_splice() se puede eliminar un o o varios elementos contiguos del vector.
        $numeros=[65,77,22,6];
        array_splice($numeros,2,2); // (array a eliminar,posicion inicial a borrar, cantidad de elementos a borrar)
        print_r($numeros);
        $borrardas=array_slice($numeros,2,2); // si sequiere guardar en una var, el sistema lo convierte en una array aut. con los valores borrados

    
// CON LA MISMA FUNCION array_splice(), se puede insertar un elemento en cualquier posicion 
    //    para INSERTAR se ponen minimo 4 parametros.

        $numeros=[65,77,22,6];
        print_r($numeros);
        echo"<br>";
        array_splice($numeros,2,0,123); //(array ORIG, que posicion quiero hacer algo, 0 si no quiero borrar nada , 123->LO QUE SE INSERTARA O UN ARRRAY )
        array_splice($numeros,2,0) // con esta funcion 

        print_r($numeros);

    // esta en el array?

        La funcion in_array() devuelve true si encuentra la clave dentro del vector y false si no.

        $numeros=[65,77,22,6];
        in_array(22,$numeros); //devolveria true porque el 22 existe en $numeros.

    // buescar en el array

        LA funcion array_search() busca un elemento en el arry y devuelve SU POSICION.

        $numeros=[65,77,22,6];
        echo array_search(22,$numeros); //devolveria la posicion donde esta el elemento $numeros.

    // ORDENAR UN ARRAY O VECTOR y de mayor a menor

        La funcion sort() ordena los elementos de un vector de menor a mayoria
        y detecta automatcamente el tipo de datos a ordenar.
        $numeros=[65,77,22,6];
        $print_r($numeros);
        sort($numeros);
        $print_r($numeros);

        lafuncion rsort() ordena los elementos de mayor a menor, tambien se puede especificar el tipo de ordenacion.

        $numeros=[65,77,22,6];
        $print_r($numeros);
        rsort($numeros);
        $print_r($numeros);

    // ordenar un vector

        La funcion sort() ordena elementos de menos a mayoria. y detecta los tipos del vector automaticamente

        sort($array)

        sort($array,SORT_REGULAR);

        la constante SORT_REGULAR , ORDENA DE MAYOR A MENOR , NROS DE MENOS A MAYOR Y SI ESTAN MEZCLADOS PONE LOS NROS PRIMERO, LUEGO
        LAS MAYUSCULAS Y POR ULTIMO LAS MINUSCULAS.

    // ORDENAR EN FORMA PERSONALIZADA.

        Se hae a traves de constantes.

        sort($array,SORT_NUMERIC); -> fuerza el array para ordenarlos como numeros
        sort ($array,SORT_STRING); -> fuerza al array para ordenarlos como cadenas de texto.


//  otro modo de ordenar.

        si se quiere ordenar strings iguales que contienen un numero de la manera que haria un humano , se UTILIZARIA
            SORT_NATURAL

            SORT_NATURAL([0]->foto-1.jpg)
                         [1]->foto-2.jpg
                         [1]->foto-11.jpg
                         [1]->foto-20.jpg
                         [1]->foto-100.jpg

                         LOS STRING DEBEN TENER EL MISMO NOMBRE.en este caso 'foto'.
                         
        -con el parametro SORT_FLAG_CASE, ayuda a ordenar hasta cierto punto case sensitive

        sort($array,SORT_STRING|SORT_FLAG_CASE)
        rsort($array,SORT_STRING|SORT_FLAG_CASE) ordena de mayor a menor con case sensitive.

// array multisort

        La funcion array_multisort() ordena dos arrys a la vez, usando el primero como referencia.


        $array1=[4,2,6,0]
        $array2=["`porche","audi","bmw","tesla","ferrari"];
        array_multisort($array1,$array2);


// elemento mas grande y mas pequeño

    la funcion max() devuelve el elemento mas grande dentro del vector
    la funcino min() devuelve el elementor menor dentro del vector

        $numeros=[65,77,22,6];
        echo max($numeros); -> 77 
        echo min($numeros); -> 6

// STRING A VECTOR

        La funcion explode() convierte elementos independientes de un string en un array.
        NO PERMITE usar un string vacio como separador.

        $string="quiero pipas de calabaza";
        echo $string;
        $array=explode(" ",$string);
        print_r($array) ->  [0] quiero
                            [1] pipas
                            [2] de

        LA FUNCION mb_str_split() convierte un string en un array(cada caracter sera una posicion del array)

        $string="abcd";
        echo $string;
        $array=mb_str_split($string);
        print_r($array) ->  [0] a
                            [1] b
                            [2] c


        acepta un segundo argumento con el que podemos especificar la cantidad de caracteres del loque a separar.

        $string_dnis="43234322t49321447r";
        echo $string;
        $array_dnis=mb_str_split($string,9);
        print_r($array);

        // vector a string

        LA FUNCION IMPLODE() convierte los elementos de un vector en un string, usando un separador,
        esta SI permite usar un string vacio como separador

        $numeros=[65,77,22,6];
        print_r($numeros);
        $stringnumeros=implode(" ",$numeros);
        echo $stringnumeros;



// BUCLE FOREACH

    se usa para recorrer todos los elementos del array. Para cada iteracion el bucle, el valor del elemento actual es asignado a $elemento,
    desde el primero hasta le ultimo elemento de $vector

    foreach ($vector as $elemento){
        codigo a ejecutar;
    }
    $vector=['áudi','nissan','pegout']
    foreach ($vector as $elemento){
        echo "$elemento<br>";
    }        
 
// VECTOR MULTIDIMENSIONAL

    - un vector multidimensional es un vector que contiene otros vectores en su interior.

    PHP permite multidimensionales de 2,3,4,5 o mas diveles de profundidad.


    vector[[1,2,3],[4,5,6]]

//VECTOR O ARRAY ASOCIATIVO

    Un vector asociativo permite usar un nombre como indice.

    $persona=['nom'=>'juan','ap'=>'lopez','dni'=>'23232Q'];
    echo $persona["nom"].' '.$persona['ap'].' '.$persona['dni'];
    foreach($persona as $x=>$valor)
    {
        echo $x.' '.$valor;
    }

// bucle foreach para recorrer un buqle asociativo

    foreach($vector as $clave=>$valor){
        codigo a ejecutar;
    }

    para cada iteracion del bucle el valor del elemento  actual es COPIADO   A $clave=>$valor, hasta llegar 
    al ultimo elemento de $vector. si se modifican $clave o $valor, no se modificara el elemento original en $vector.

    $edad=['juan'=>'35','Jordi'=>'37','luis'=>'43'];

    foreach($edad as $x=>$x_valor){
        echo 'clave= '.$x.', valor= '.$x_valor;
        echo '<br>';
    }


// ordenar un array asociaativo

    La funcion asort() ordena un vector asocitaivo en orden ascendente(respecto el valor);
    La funcion arsort() ordena un vector asociativo en orden descendente(respecto el valor);

    $animal=['gato'=>'miau','perro'=>'guau','loro'=>'pio'];
    asort($animal);
    print_r($animal);

    La funcion ksort() ordena un vector asocitaivo en orden ascendente(respecto a la clave);
    La funcion krsort() ordena un vector asocitaivo en orden descendente(respecto a la clave);


    $animal=['gato'=>'miau','perro'=>'guau','loro'=>'pio'];
    ksort($animal);
    print_r($animal);


