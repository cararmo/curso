<?php
/* primera forma de hacerlo, con icnludes dentro del index.php
/*include('../Librerias/Librerias.php');
include('../Controllers/CCentral.php');
include('../Models/Mchistes.php');  
include('../Controllers/Cchistes.php');
include('../Controllers/CPagPrincipal.php');
include('../Views/VPagChistesGenerica.php');
include('../Views/VPaginaChistes.php');
include('../Views/VPaginaprincipal.php');
*/

// segunda forma de hacerlo con autoload y swich case

spl_autoload_register('importar');

function importar($clase)
{
    echo 'clase buscada: '.$clase.'<br>';

    // Tercera forma de hacerlo: con foreach y un array, es mas fácil.

    $carpetas=['Controllers','Librerias','Views','Models'];
    
   
    foreach ($carpetas as $carpeta)
    {
        $ruta='../'.$carpeta.'/'.$clase.'.php';
        if (file_exists($ruta))
        {
                include($ruta);
        }
    }



}

/*
    switch($clase)
     {
        
        case 'CCentral':
            //if (file_exists($clase))
           // {
                //echo 'asignar <br>';
                include('../Controllers/'.$clase.'.php');
           // }
           // else{
        //        echo ' entra a ccentral';
            //}
            break;
        case 'CChistes' :
          //  if (file_exists($clase))
         //   {
                    //echo 'asignar <br>';
            include('../Controllers/'.$clase.'.php');
          //  }
            break;
        case 'CPagPrincipal' :
                include('../Controllers/'.$clase.'.php');
                break;

        case 'Librerias' :
            include('../Librerias/'.$clase.'.php');
            break;

        
        case 'VPaginaPrincipal' :
                include('../Views/'.$clase.'.php');
                break;
        
        case 'VPagChistesGenerica' :
                include('../Views/'.$clase.'.php');
                break;
        case 'VPaginaChistes' :
                include('../Views/'.$clase.'.php');
                break;
        default:
            
                echo "esto es de: '.$clase.',no esta!! <br>";

    } */

   
   ?>