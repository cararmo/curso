<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <!-- <link rel="stylesheet" href="<?//=asset('css/style.css')?> "> -->
    <link rel="stylesheet" href=@yield('css') >

   <title>@yield('titulo')</title>
</head>
<body>
    @yield('body')
</body>
</html>
