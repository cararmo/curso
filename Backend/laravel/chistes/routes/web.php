<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChistesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function() {
    return view('chistes');
}); 
Route::get('ruta', function() {
    return  '<!DOCTYPE html><html><body><p>Página de pruebas</p></body></html>';
}); /*
Route::get('chistes', function() {
    return view('chistes');
});
*/
Route::get('chistes', ChistesController::class);
