﻿using Common.Lib.Core;
using System;

namespace Library.lib.Models
{
    public class Loan : Entity
    {
        #region BookCopy
        public Guid PBookId { get; set; }
        public PBook PBook { get; set; }
        #endregion

        #region Client
        public Guid ClientId { get; set; }
        public Client Client { get; set; }

        #endregion
        public DateTime RequestDate { get; set; }
        public DateTime? ReturnDate { get; set; }

    }
}
