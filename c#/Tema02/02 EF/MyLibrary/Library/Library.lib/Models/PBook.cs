﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;


namespace Library.lib.Models
{
    public class PBook : Entity
    {
        #region Book
        public Guid BookId { get; set; }
        public Book Book { get; set; }

        #endregion
        public string Barcode { get; set; }
        public string ISDN { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int YearPublih { get; set; }

        public virtual List<Loan> Loans { get; set; }


    }
}
