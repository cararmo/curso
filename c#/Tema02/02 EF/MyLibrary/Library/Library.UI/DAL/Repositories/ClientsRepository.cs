﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.lib.Models;
using Library.UI.DAL;

namespace Library.Lib.DAL
{
    public class ClientsRepository : IDisposable
    {
        public LibraryDbContext DbContext { get; set; }

        public List<Client> ClientList
        {
            get
            {
                return GetAll();
            }
        }

        public ClientsRepository()
        {
            DbContext = new LibraryDbContext();
            LoadInitData();
        }

        public void LoadInitData()
        {
            if (DbContext.Clients.Count() == 0)
            {
                //creo Clients de prueba
                var std1 = new Client()
                {
                    Id = Guid.NewGuid(),
                    Name = "Pepe",
                    Email = "p@p.com",
                   // Dni = "12345678a"
                };
                var std2 = new Client()
                {
                    Id = Guid.NewGuid(),
                    Name = "Marta",
                    Email = "m@m.com",
                   // Dni = "12345678b"
                };

                // y lo meto en el campo interno
                DbContext.Clients.Add(std1);
                DbContext.Clients.Add(std2);

                DbContext.SaveChanges();
            }

        }

        public List<Client> GetAll()
        {
            // opción 1: a pelo

            // creo un objeto de tipo List de Client
            // que vamos a devolver
            //var output = new List<Client>();

            ////recorremos cada par de key/value en el diccionario
            //foreach (var item in Clients)
            //{
            //    // y sacamos su valor (objeto de tipo Client)
            //    // y lo metemos en la lista de salida
            //    var Client = item.Value;
            //    output.Add(Client);
            //}

            //// devolvemos la lista
            //return output;

            // opción 2: short cut para lo de arriba
            //return Clients.Values.ToList(); antes
            return DbContext.Clients.ToList();
        }

        public Client Get(Guid id)
        {
            // si existe un Client con es id en la DB me lo devuelve
            //var Client = DbContext.Clients.FirstOrDefault(x => x.Id == id);
            //para el id es mejor el find
            var output = DbContext.Clients.Find(id);
            return output;
        }

        public Client GetByDni(string dni)
        {
            //foreach (var item in Clients)
            //{
            //    var Client = item.Value;
            //    if (Client.Dni == dni)
            //        return Client;
            //}

            //return default(Client);
            var output = DbContext.Clients.FirstOrDefault(s => s.Dni == dni);
            return output;
        }

        public List<Client> GetByName(string name)
        {
            //var output = new List<Client>();

            //foreach (var item in Clients)
            //{
            //    var Client = item.Value;

            //    if (Client.Name == name)
            //        output.Add(Client);
            //}

            //// devolvemos la lista
            //return output;

            var output = DbContext.Clients.Where(s => s.Name == name).ToList();
            return output;
        }

        public ClientValidationsTypes Add(Client Client)
        {
            if (Client.Id != default(Guid))
            {
                // todo bien porque no hay ningún Id
                return ClientValidationsTypes.IdNotEmpty;
            }
            else if (!Client.ValidateDniFormat(Client.Dni))
            {
                //el dni está mal construido
                return ClientValidationsTypes.WrongDniFormat;
            }
            else
            {
                var stdWithSameDni = GetByDni(Client.Dni);
                if (stdWithSameDni != null && Client.Id != stdWithSameDni.Id)
                {
                    // hay dos estudiantes distintos con mismo dni
                    return ClientValidationsTypes.DniDuplicated;
                }
            }

            if (!Client.ValidateName(Client.Name))
            {
                return ClientValidationsTypes.WrongNameFormat;
            }
            //else if (!DbContext.Clients.Any(s => s.Id == Client.Id)) ineficiente
            else if (DbContext.Clients.All(s => s.Id != Client.Id)) // más eficiente
            {                
                Client.Id = Guid.NewGuid();
                DbContext.Clients.Add(Client);
                DbContext.SaveChanges();

                return ClientValidationsTypes.Ok;
            }

            // si el Id no estaba vacío y ya existía en la DB devolvmenos false
            return ClientValidationsTypes.IdDuplicated;
        }

        public ClientValidationsTypes Update(Client Client)
        {
            if (Client.Id == default(Guid))
            {
                // no se puede actualizar un registro sin id
                return ClientValidationsTypes.IdEmpty;
            }
            if (DbContext.Clients.All(s => s.Id != Client.Id))
            {
                // no se puede actualizar un registro
                // que no exista en la DB
                return ClientValidationsTypes.ClientNotFound;
            }

            /* comprobamos que el dni sea correcto
            if (!Client.ValidateDniFormat(Client.Dni))
            {
                //el dni está mal construido
                return ClientValidationsTypes.WrongDniFormat;
            }*/


            // comprobamos que no haya otro alumno diferente
            // con el mismo dni
            /*
            var stdWithSameDni = GetByDni(Client.Dni);
            if (stdWithSameDni != null && Client.Id != stdWithSameDni.Id)
            {
                // hay dos estudiantes distintos con mismo dni
                return ClientValidationsTypes.DniDuplicated;
            }
            */
            if (!Client.ValidateName(Client.Name))
            {
                return ClientValidationsTypes.WrongNameFormat;
            }

            DbContext.Clients.Update(Client);
            DbContext.SaveChanges();

            return ClientValidationsTypes.Ok;
        }

        public ClientValidationsTypes Delete(Guid id)
        {
            var Client = DbContext.Clients.Find(id);
            if (Client == null)
            {
                return ClientValidationsTypes.ClientNotFound;
            }
            else
            {
                DbContext.Clients.Remove(Client);
                return ClientValidationsTypes.Ok;
            }
        }

        public void Dispose()
        {
        }
    }
}
