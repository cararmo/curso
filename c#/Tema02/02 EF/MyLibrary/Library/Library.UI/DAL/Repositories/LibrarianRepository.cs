﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.lib.Models;
using Library.UI.DAL;

namespace Library.Lib.DAL
{
    public class LibrariansRepository : IDisposable
    {
        public LibraryDbContext DbContext { get; set; }

        public List<Librarian> LibrarianList
        {
            get
            {
                return GetAll();
            }
        }

        public LibrariansRepository()
        {
            DbContext = new LibraryDbContext();
            LoadInitData();
        }

        public void LoadInitData()
        {
            if (DbContext.Librarians.Count() == 0)
            {
                //creo Librarians de prueba
                var std1 = new Librarian()
                {
                    Id = Guid.NewGuid(),
                    Name = "Pepe",
                    Email = "p@p.com",
                   // Dni = "12345678a"
                };
                var std2 = new Librarian()
                {
                    Id = Guid.NewGuid(),
                    Name = "Marta",
                    Email = "m@m.com",
                   // Dni = "12345678b"
                };

                // y lo meto en el campo interno
                DbContext.Librarians.Add(std1);
                DbContext.Librarians.Add(std2);

                DbContext.SaveChanges();
            }

        }

        public List<Librarian> GetAll()
        {
            // opción 1: a pelo

            // creo un objeto de tipo List de Librarian
            // que vamos a devolver
            //var output = new List<Librarian>();

            ////recorremos cada par de key/value en el diccionario
            //foreach (var item in Librarians)
            //{
            //    // y sacamos su valor (objeto de tipo Librarian)
            //    // y lo metemos en la lista de salida
            //    var Librarian = item.Value;
            //    output.Add(Librarian);
            //}

            //// devolvemos la lista
            //return output;

            // opción 2: short cut para lo de arriba
            //return Librarians.Values.ToList(); antes
            return DbContext.Librarians.ToList();
        }

        public Librarian Get(Guid id)
        {
            // si existe un Librarian con es id en la DB me lo devuelve
            //var Librarian = DbContext.Librarians.FirstOrDefault(x => x.Id == id);
            //para el id es mejor el find
            var output = DbContext.Librarians.Find(id);
            return output;
        }

        public Librarian GetByDni(string dni)
        {
            //foreach (var item in Librarians)
            //{
            //    var Librarian = item.Value;
            //    if (Librarian.Dni == dni)
            //        return Librarian;
            //}

            //return default(Librarian);
            var output = DbContext.Librarians.FirstOrDefault(s => s.Dni == dni);
            return output;
        }

        public List<Librarian> GetByName(string name)
        {
            //var output = new List<Librarian>();

            //foreach (var item in Librarians)
            //{
            //    var Librarian = item.Value;

            //    if (Librarian.Name == name)
            //        output.Add(Librarian);
            //}

            //// devolvemos la lista
            //return output;

            var output = DbContext.Librarians.Where(s => s.Name == name).ToList();
            return output;
        }

        public LibrarianValidationsTypes Add(Librarian Librarian)
        {
            if (Librarian.Id != default(Guid))
            {
                // todo bien porque no hay ningún Id
                return LibrarianValidationsTypes.IdNotEmpty;
            }
            else if (!Librarian.ValidateDniFormat(Librarian.Dni))
            {
                //el dni está mal construido
                return LibrarianValidationsTypes.WrongDniFormat;
            }
            else
            {
                var stdWithSameDni = GetByDni(Librarian.Dni);
                if (stdWithSameDni != null && Librarian.Id != stdWithSameDni.Id)
                {
                    // hay dos estudiantes distintos con mismo dni
                    return LibrarianValidationsTypes.DniDuplicated;
                }
            }

            if (!Librarian.ValidateName(Librarian.Name))
            {
                return LibrarianValidationsTypes.WrongNameFormat;
            }
            //else if (!DbContext.Librarians.Any(s => s.Id == Librarian.Id)) ineficiente
            else if (DbContext.Librarians.All(s => s.Id != Librarian.Id)) // más eficiente
            {                
                Librarian.Id = Guid.NewGuid();
                DbContext.Librarians.Add(Librarian);
                DbContext.SaveChanges();

                return LibrarianValidationsTypes.Ok;
            }

            // si el Id no estaba vacío y ya existía en la DB devolvmenos false
            return LibrarianValidationsTypes.IdDuplicated;
        }

        public LibrarianValidationsTypes Update(Librarian Librarian)
        {
            if (Librarian.Id == default(Guid))
            {
                // no se puede actualizar un registro sin id
                return LibrarianValidationsTypes.IdEmpty;
            }
            if (DbContext.Librarians.All(s => s.Id != Librarian.Id))
            {
                // no se puede actualizar un registro
                // que no exista en la DB
                return LibrarianValidationsTypes.LibrarianNotFound;
            }

            /* comprobamos que el dni sea correcto
            if (!Librarian.ValidateDniFormat(Librarian.Dni))
            {
                //el dni está mal construido
                return LibrarianValidationsTypes.WrongDniFormat;
            }*/


            // comprobamos que no haya otro alumno diferente
            // con el mismo dni
            /*
            var stdWithSameDni = GetByDni(Librarian.Dni);
            if (stdWithSameDni != null && Librarian.Id != stdWithSameDni.Id)
            {
                // hay dos estudiantes distintos con mismo dni
                return LibrarianValidationsTypes.DniDuplicated;
            }
            */
            if (!Librarian.ValidateName(Librarian.Name))
            {
                return LibrarianValidationsTypes.WrongNameFormat;
            }

            DbContext.Librarians.Update(Librarian);
            DbContext.SaveChanges();

            return LibrarianValidationsTypes.Ok;
        }

        public LibrarianValidationsTypes Delete(Guid id)
        {
            var Librarian = DbContext.Librarians.Find(id);
            if (Librarian == null)
            {
                return LibrarianValidationsTypes.LibrarianNotFound;
            }
            else
            {
                DbContext.Librarians.Remove(Librarian);
                return LibrarianValidationsTypes.Ok;
            }
        }

        public void Dispose()
        {
        }
    }
}
