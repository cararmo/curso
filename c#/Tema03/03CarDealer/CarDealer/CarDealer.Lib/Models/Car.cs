﻿using System;

namespace CarDealer.Lib.Models
{
    public class Car
    {
        public Guid Id { get; set; }
        public string Brand { get; set; }
        public int Door { get; set; }
        public string license { get; set; }
        public string Color { get; set; }

    }

}
