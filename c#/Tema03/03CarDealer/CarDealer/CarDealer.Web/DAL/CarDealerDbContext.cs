﻿using Microsoft.EntityFrameworkCore;
using CarDealer.Lib.Models;

namespace CarDealer.Web.DAL
{
    public class CarDealerDbContext : DbContext
    {
        public DbSet<Car> CarDealers { get; set; }

        public CarDealerDbContext() : base()
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Car>().ToTable("CarDealers");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=LAPTOP-TSPE80VL;Database=Academy;Trusted_Connection=True;MultipleActiveResultSets=true");
            optionsBuilder
                .UseMySql(connectionString: @"server=localhost;database=cardealer;uid=caradmin;password=1234;",
                new MySqlServerVersion(new Version(8, 0, 23)));

        }
    }
}
