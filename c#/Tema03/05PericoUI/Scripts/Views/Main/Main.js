class Main
{
    get IsLogon()
    {
        return App.ClientGlobals.CurrentUser  != null;
    }
    
    constructor()
    {

    }
}

App.
  component('main', {   
    templateUrl: 'scripts/views/main/main.html',
    controller: Main,
    controllerAs: "vm"
  });
