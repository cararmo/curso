﻿using System;

namespace SpaceShipShop.Lib.Models
{
    public class SpaceShip
    {
        public Guid Id { get; set; }
        public string Brand { get; set; }
        public string Password { get; set; }
        public int FTLFactor { get; set; }
        public string color { get; set; }
        public int PassengerCapacity { get; set; }
    }

}

