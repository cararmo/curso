﻿using Microsoft.EntityFrameworkCore;
using SpaceShipShop.Lib.Models;

namespace SpaceShipShop.Web.DAL
{
    public class SpaceShipShopDbContext: DbContext
    {
        public DbSet<SpaceShip> SpaceShips { get; set; }

        public SpaceShipShopDbContext() : base()
        {

        
        }

        protected override void OnModelCreating (ModelBuilder builder) 
        {
            builder.Entity<SpaceShip>().ToTable("SpaceShips");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
            .UseMySql(connectionString: @"server=localhost;database=spaceships;uid=spaceshipadmin;password=1234;",
                new MySqlServerVersion(new Version(8, 0, 23)));

        }
    }

}
