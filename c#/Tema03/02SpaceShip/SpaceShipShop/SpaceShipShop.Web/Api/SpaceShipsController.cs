﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using SpaceShipShop.Lib.Models;
using SpaceShipShop.Web.DAL;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SpaceShipShop.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpaceShipsController : ControllerBase
    {
        // GET: api/<SpaceShipsController>
        [HttpGet]
        public IEnumerable<SpaceShip> Get()
        {
            var dbContext = new SpaceShipShopDbContext();

            if (dbContext.SpaceShips.Count() == 0)
            {
                var nave1 = new SpaceShip()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Pegasus",
                    FTLFactor = 10,
                    PassengerCapacity = 20,
                    color = "Red",
                    Password = "Alucino.1234"
                };

                var nave2 = new SpaceShip()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Phoenix",
                    FTLFactor = 15,
                    PassengerCapacity = 30,
                    color = "Green",
                    Password = "Alucino.1234"
                };

                dbContext.SpaceShips.Add(nave1);
                dbContext.SpaceShips.Add(nave2);

                dbContext.SaveChanges();

            }
            return dbContext.SpaceShips.ToList();
        }

        // GET api/<SpaceShipsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<SpaceShipsController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<SpaceShipsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<SpaceShipsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
