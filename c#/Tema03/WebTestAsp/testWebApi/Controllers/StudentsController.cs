﻿using Microsoft.AspNetCore.Mvc;
using testWebApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace testWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        // GET: api/<StudentsController>
        [HttpGet]
        public IEnumerable<Student> Get()
        {
            var st1 = new Student()
            {
                Id = Guid.NewGuid(),
                Name = "uno",
                Password = "1234",
                Email = "yo@yo.com"
            };
            var st2 = new Student()
            {
                Id= Guid.NewGuid(),
                Password = "1234",
                Name = "uno",
                Email = "yo@yo.com"
            };
            
            var output = new List<Student>();
            output.Add(st1);
            output.Add(st2);
            return output;

            //return new string[] { "value1", "value2" };
        }

        // GET api/<StudentsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<StudentsController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<StudentsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<StudentsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
