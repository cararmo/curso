﻿using System;

using System.Collections.Generic;

namespace NotasAlumnosEstructurada
{
    class Program
    {
        static string EScapeWord = "FIN";
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenid@ al programa para gestión de alumnos");
            Console.WriteLine("Introduzca las notas de los alumnos");

            var notasDeAlumnos = new List<double>();
            
            var keepdoing = true;

            var msg = "";

            while (keepdoing) 
            {
                Console.WriteLine($"Nota del alumno {notasDeAlumnos.Count+1}:");
                var notaText = Console.ReadLine();

                if (notaText == EScapeWord)
                {
                    keepdoing = false;
                    var opcion = "";

                    Console.WriteLine($"Presione 1, si calcula la nota maxima de los alumnos o 2 si es la nota media{opcion}");
                    opcion = Console.ReadLine();

                    int totalAlumnos = notasDeAlumnos.Count;

                    var notaDeEntrada = 0.0;
                    var notaMaxima = 0.0;
                    var notaPromedio = 0.0;

                    //var totalAlumnos=1;
                    switch (opcion)
                    {
                        case "1":
                            for (int i = 0; i< totalAlumnos; i++)
                            {
                                notaDeEntrada = notasDeAlumnos[i];
                                notaMaxima = notasDeAlumnos[i];
                                if (notaMaxima <= notaDeEntrada)
                                {
                                    notaMaxima = notaDeEntrada;
                                 
                                }
                                msg = "La nota máxima es: " + notaMaxima;

                            }
                            break;

                        case "2":
                            for (int i = 0; i < totalAlumnos; i++)
                            {
                                notaPromedio += notasDeAlumnos[i];
                            }
                            notaPromedio = notaPromedio/totalAlumnos;
                            msg = "La nota promedio es: " + notaPromedio;
                            break;
                       
                        default:
                            // Statements to Execute if No Case Matches
                        break;
                    }
                }
                else
                {

                 var nota = 0.0;

                    if (double.TryParse(notaText.Replace(",","."), out nota))
                    {
                        notasDeAlumnos.Add(nota);
                    }
                    else
                    {
                        Console.WriteLine("la nota introducida es incorrecta");
                    }                }
            }
            Console.WriteLine(msg); 

            /*var suma = 0.0;
            for (var i = 0; i< notasDeAlumnos.Count; i++)
            {
                suma += notasDeAlumnos[i];
            }

            var average = suma / notasDeAlumnos.Count;
            Console.WriteLine("la media de los examenes es: {0}", average);*/
        }
    }
}
