﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Lib.Models
{
    public class Exam: Entity
    {
        public Student Student { get; set; }
        public Subject Subject { get; set; }
        public Double Mark { get; set; }
        public DateTime TimeStamp { get; set; }

    }
}
