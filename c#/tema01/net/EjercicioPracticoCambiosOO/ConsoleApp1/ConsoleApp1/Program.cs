﻿// C#
using System;
using System.Collections.Generic;

namespace ConsoleApp1.Lib.Models
{
    /// <summary>
    /// El objetivo de esta iteración es usar clases para definir:
    /// Students (Estudiante)
    /// Subjects (Materias)
    /// Exams (Exámenes)
    /// y añadir opciones extras al menú para poder manejarlas
    /// En el ejemplo crearemos las entidades (clases que definen el dominio -funcionalidad- de nuestro programa)
    /// y la opción de menú para añadir estudiantes
    /// El alumno debe completar el resto de funcionalidades CRUD (Create, Read, Update, Destroy)
    /// </summary>
    class Program
    {
        //static List<double> Marks { get; set; }

        public static Dictionary<string, Student> Students = new Dictionary<string, Student>();
        public static Dictionary<string, Subject> Subjects = new Dictionary<string, Subject>();
        public static List<Exam> Exams = new List<Exam>();

        //static string EscapeWord = "fin";

        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenid@ al programa para gestión de alumnos");
            Console.WriteLine("Para ir a la gestión de alumnos use la opción a");
            Console.WriteLine("Para ir a la gestión de NOTAS use la opción n");
            Console.WriteLine("Para obtener estadísticas use la opción e");

            //Marks = new List<double>();
            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;

                if (option == 'a')
                {
                    ShowStudentsMenu();
                }
                else if (option == 'e')
                {
                    ShowStatsMenu();
                }
                else if (option == 'n')
                {
                    ShowSubjectMenu();
                }
            }
        }

        static void ShowStudentsMenu()
        {
            Console.WriteLine();
            ShowStudentsMenuOptions();

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "all":
                        ShowAllStudents();
                        break;
                    case "add":
                        AddNewStudent();
                        break;
                    case "edit":
                        EditStudent();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    case "notas":
                        AddMark(text);
                        break;
                    case null:
                        break;
                }
            }

            ShowMainMenu();
        }
        private static void ShowStudentsMenuOptions()
        {
            Console.WriteLine("--Menu de Estudiantes--");

            Console.WriteLine("Para ver todos los estudiantes escriba all");
            Console.WriteLine("Para añadir un nuevo estudiante escriba add");
            Console.WriteLine("Para editar un estudiante escriba edit");
            Console.WriteLine("Para borrar un estudiante escriba del");
            Console.WriteLine("Para volver al menú principal escriba m");
        }
        private static void ShowMainMenu()
        {
            Console.WriteLine("--- Menú principal ---");
            Console.WriteLine("Para introducir notas de los alumnos use la opción a");
            Console.WriteLine("Para obtener estadísticas use la opción e");
            Console.WriteLine("Para gestión de NOTAS use la opción n");
        }
        static void ShowAllStudents()
        {
            foreach (var student in Students.Values)
            {
                Console.WriteLine($"{student.Dni} {student.Name}");
            }
        }
        static void AddNewStudent()
        {
            Console.WriteLine("Primero inserte el dni o escriba anular para interrumpir");

            var keepdoing = true;
            while (keepdoing)
            {
                var dni = Console.ReadLine();

                if (dni == "anular")
                {
                    break;
                }
                else if (string.IsNullOrEmpty(dni) || dni.Length != 9)
                {
                    Console.WriteLine("El dni está en un formato incorrecto");
                }
                else if (Students.ContainsKey(dni))
                {
                    Console.WriteLine($"Ya existe un estudiante con el dni {dni}");
                }
                else
                {
                    while (true)
                    {
                        Console.WriteLine("Ahora inserte el nombre o escriba anular para interrumpir");
                        var name = Console.ReadLine();

                        if (name == "anular")
                        {
                            keepdoing = false;
                            break;
                        }

                        if (string.IsNullOrEmpty(name))
                        {
                            Console.WriteLine("El nombre está vacío");
                        }
                        else
                        {
                            var student = new Student
                            {
                                Id = Guid.NewGuid(),
                                Dni = dni,
                                Name = name
                            };
                            Students.Add(student.Dni, student);
                            keepdoing = false;
                            break;
                        }
                    }
                }
            }

        }
        static void EditStudent()
        {
            Console.WriteLine("Cambiar datos: Primero inserte el dni o escriba anular para interrumpir");

            var keepdoing = true;
            while (keepdoing)
            {
                var dni = Console.ReadLine();

                if (dni == "anular")
                {
                    break;
                }
                else if (string.IsNullOrEmpty(dni) || dni.Length != 9)
                {
                    Console.WriteLine("El dni está en un formato incorrecto");
                }
                else if (Students.ContainsKey(dni))
                {
                    Console.WriteLine($"escriba el nombre corregido , cuyo Dni es {dni}");
                    var name = Console.ReadLine();

                    if (string.IsNullOrEmpty(name))
                    {
                        Console.WriteLine("El nombre está vacío");
                    }
                    else
                    {
                        var student = Students[dni];
                        student.Name = name;
                        keepdoing = false;
                        break;
                    }
                }
            }

            ShowStudentsMenuOptions();
        }
        static void AddMark(string text)
        {
            var mark = 0.0;

            if (double.TryParse(text, out mark))
            {
                //Marks.Add(mark);
                Console.WriteLine("Nota introducida correctamente, añada otra nota más, pulse all para lista todas las notas o pulse m para volver al menú principal");
            }
            else
            {
                Console.WriteLine($"La nota introducida {text} no está en un formato correcto, vuelva a introducirla correctamente");
            }
        }
        static void ShowSubjectMenu()
        {
            Console.WriteLine();
            ShowSubjectsMenuOptions();
            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();
                switch (text)
                {
                    case "a":
                        ShowAllSubjects();
                        break;
                    case "add":
                        AddNewSubject();
                        break;
                    case "edit":
                        EditSubject();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    case null:
                        break;
                }
            }
        }
        private static void ShowSubjectsMenuOptions()
        {

            Console.WriteLine("para agregar Notas escriba a");
            Console.WriteLine("Para modificar Notas escriba m");
            Console.WriteLine("Para retornar al menu principal escriba r");
        }
        static void ShowAllSubjects()
        {

        }
        static void AddNewSubject()
        { 
                var option = Console.ReadKey().KeyChar;
                var keepdoing = true;
                var subject="";
            while (keepdoing)
            {
                do 
                {
                    Console.WriteLine("Inserte Materia");
                    subject = Console.ReadLine();
          
                   /// if (string.IsNullOrEmpty(subject))
                     //   Console.WriteLine("Escriba nombre de la materia");
                      //  subject=Console.ReadLine();


                } while ( string.IsNullOrEmpty(subject));

                Console.WriteLine("inserte el dni, o escriba anular para interrumpir");
                var dni = Console.ReadLine();

                if (dni== "anular")
                {
                    break;
                }
                else if (string.IsNullOrEmpty(dni) || dni.Length != 9)
                {
                    Console.WriteLine("El dni está en un formato incorrecto");
                }
                else if (Students.ContainsKey(dni))
                {

                    if (option == 'a')
                    {
                        var mark = 0.0;
                        Console.WriteLine("Inserte nueva nota");
                        var entrada= Console.ReadLine();
                        //                        mark = Double.TryParse(entrada, out mark);

                        if (double.TryParse(entrada, out mark))
                        {
                            var exam = new Exam()
                            {
                                Student = Students[dni],
                                Subject = Subjects[subject],
                                Mark = mark,
                                TimeStamp = DateTime.Now
                            };
                            // añadimos el objeto exam a su repositorio
                            Exams.Add(exam);
                            Console.WriteLine("Nota introducida correctamente, añada otra nota más, pulse all para lista todas las notas o pulse m para volver al menú principal");
                        }
                        else
                        {
                            Console.WriteLine($"La nota introducida {entrada} no está en un formato correcto, vuelva a introducirla correctamente");
                        }

                    }
                    break;
                }
                
                ShowMainMenu();
            }
        }
        static void EditSubject()
        {

        }
        static void ShowStatsMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--Menu de Estadísticas--");

            Console.WriteLine("Para ver la media escriba avg");
            Console.WriteLine("Para ver la nota más alta escriba max");
            Console.WriteLine("Para ver la nota más baja escriba min");
            Console.WriteLine("Para volver al menú principal escriba m");

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "avg":
                        ShowAverage();
                        break;
                    case "max":
                        ShowMaximum();
                        break;
                    case "min":
                        ShowMinimum();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        Console.WriteLine("comando no reconocido, introduzca una opción válida");
                        break;
                }
            }
            ShowMainMenu();
        }
        static void ShowAverage()
        {
            //var suma = 0.0;
            //for (var i = 0; i < Marks.Count; i++)
            //{
            //    suma += Marks[i];
            //}

            //var average = suma / Marks.Count;
            //Console.WriteLine("la media los exámenes es: {0}", average);
        }
        static void ShowMaximum()
        {
            //var max = 0.0;
            //for (var i = 0; i < Marks.Count; i++)
            //{
            //    if (Marks[i] > max)
            //        max = Marks[i];
            //}

            //Console.WriteLine("la nota más alta es: {0}", max);
        }
        static void ShowMinimum()
        {
            //var min = 0.0;
            //if (Marks.Count == 0)
            //    min = 0.0;
            //else
            //    min = Marks[0];

            // esto (el operador ternario) hace lo mismo que lo de arriba
            //var min = Marks.Count == 0 ? 0.0 : Marks[0];

            //for (var i = 0; i < Marks.Count; i++)
            //{
            //    if (Marks[i] < min)
            //        min = Marks[i];
            //}

            //Console.WriteLine("la nota más baja es: {0}", min);
        }
    }
}
