﻿namespace NotasAlumnosOO.Lib.Models
{
    public class Subject : Entity
    {
        public string Name { get; set; }

        public string Teacher { get; set; } 
        
    }
}
