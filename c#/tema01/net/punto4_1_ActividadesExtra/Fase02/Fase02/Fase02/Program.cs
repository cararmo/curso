﻿using System;

namespace Fase02
{
    class Program
    {

        const int anyInicial = 1948;
        const int intervalo = 4;
        const int anyNacimiento = 1966;
        static void Main(string[] args)
        {
            int i;
            int periodoEntreBisiestos = intervalo;
            int totalBisiestos = 0;

            for (i = anyInicial; i<=anyNacimiento; i++)
            {
                if (periodoEntreBisiestos == intervalo)
                {
                    totalBisiestos++;
                    periodoEntreBisiestos = 0;
                }
                periodoEntreBisiestos++;
            }

            Console.WriteLine("Existen: "+ totalBisiestos + " año(s) bisiesto(s) Desde: " + anyInicial + " hasta: " + anyNacimiento);

        }

     }
}

