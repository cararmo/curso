﻿using System;

namespace Fase03
{
    class Program
    {

        const int anyInicial = 1948;
        const int intervalo = 4;
        const int anyNacimiento = 1966;
        static void Main(string[] args)
        {
            int i;
            int periodoEntreBisiestos = intervalo;
            int totalBisiestos = 0;
            bool esBisiesto = false;
            string msgBisiesto = " SI es bisiesto";
            string msgNoBisiesto = " NO es bisiesto";

            for (i = anyInicial; i <= anyNacimiento; i++)
            {
                if (periodoEntreBisiestos == intervalo)
                {
                    Console.WriteLine("el año: " + i + " es bisiesto ");
                    totalBisiestos++;
                    periodoEntreBisiestos = 0;
                    if (i == anyNacimiento)
                    {
                        esBisiesto = true;
                    }
                }
                periodoEntreBisiestos++;
            }

            if (esBisiesto)
            {
                Console.WriteLine("Mi año de nacimiento: " + anyNacimiento + msgBisiesto);
            }
            else
            {
                Console.WriteLine("Mi año de nacimiento: " + anyNacimiento + msgNoBisiesto);
            }

        }
    }

}
