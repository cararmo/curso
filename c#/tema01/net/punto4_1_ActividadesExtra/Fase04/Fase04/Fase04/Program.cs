﻿using System;

namespace Fase04
{
    class Program
    {

        const int anyInicial = 1948;
        const int intervalo = 4;
        const int anyNacimiento= 1966;
        
        
        static void Main(string[] args)
        {
            string fechaNac = "31/12/1966";
            string nombreCompleto = "Carlos Arturo Mora Muñoz";

            int i;
            int periodoEntreBisiestos = intervalo;
            bool esBisiesto = false;
            string msgBisiesto = " SI es bisiesto";
            string msgNoBisiesto = " NO es bisiesto";

            for (i = anyInicial; i <= anyNacimiento; i++)
            {
                if (periodoEntreBisiestos == intervalo)
                {
                    periodoEntreBisiestos = 0;
                    if (i == anyNacimiento)
                    {
                        esBisiesto = true;
                    }
                }
                periodoEntreBisiestos++;
            }

            Console.WriteLine("Mi nombre es: " + nombreCompleto);
            Console.WriteLine("Nací el: " + fechaNac);

            if (esBisiesto)
            {
                Console.WriteLine("Mi año de nacimiento: " + anyNacimiento + msgBisiesto);
            }
            else
            {
                Console.WriteLine("Mi año de nacimiento: " + anyNacimiento + msgNoBisiesto);
            }

        }
    }
}
