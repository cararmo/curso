﻿using System;

namespace NotasAlumnosOO
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //forma 1
            var pepe = new Persona();
            pepe.Name = "pepe";
            pepe.Year = 1978;
            pepe.Email = "a@a.com";

            //forma 2
            var lolo = new Persona("lolo", 2019, "b@b.com");

            //forma 3
            var maria = new Persona
            {
                Name = "Maria",
                Year = 1977,
                Email = "m@m.com"
            };
            // prog estructurada
            ShowPersonaInformation(pepe);
            ShowPersonaInformation(lolo);
            ShowPersonaInformation(maria);

            Console.WriteLine();

            // estas dos formas son las misma, se recomeinda la segunda que es oo
            pepe.ShowInfo();
            lolo.ShowInfo();
            maria.ShowInfo();
        }

        static void ShowPersonaInformation(Persona persona)
        {
            Console.WriteLine("funcion estatica: la persona con nombre " + persona.Name + 
                " que nacio en el año del señor " + persona.Year.ToString() + 
                " y que tiene de email " + persona.Email);
        }
    }


    class Persona
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public string Email { get; set; }
        public Persona()
        {

        }
        public Persona(string name, int year, string email)
        {
            this.Name = name;
            this.Year = year;
            this.Email = email;

            ShowInfo();
        }

        public void ShowInfo()
        {
            Console.WriteLine("método de clase: la persona con nombre " + this.Name +
                " que nacio en el año del señor " + this.Year.ToString() +
                " y que tiene de email " + this.Email);
        }
    }
}
