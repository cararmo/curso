﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculadora
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            Calculate("+");
        }

        private void BtSub_Click(object sender, RoutedEventArgs e)
        {
            Calculate("-");
        }

        private void BtMult_Click(object sender, RoutedEventArgs e)
        {
            Calculate("*");
        }

        private void BtDiv_Click(object sender, RoutedEventArgs e)
        {
            Calculate("/");

        }
        private void BtClean_Click(object sender, RoutedEventArgs e)
        {
            CleanFields();

        }


        public void Calculate(string op )
        {
            double num1 = double.Parse(TbNum1.Text);
            double num2 = double.Parse(TbNum2.Text);
            var msg = "";
            if (op== "+")
            {
                var result = num1 + num2;
                msg = "the sumatory is: " + result;
            }
            else if (op== "-")
            {
                var result = num1 - num2;
                msg = "the substraction is: " + result;
            }
            else if (op== "*")
            {
                var result = num1 * num2;
                msg = "the multi is: " + result;
            }
            else if (op== "/")
            {
                var result = num1 / num2;
                msg = "the divi is: " + result;
            }
            LbOutput.Content = msg;

        }
        public void CleanFields()
        {
            TbNum1.Text = "";
            TbNum2.Text = "";
            LbOutput.Content = "";
        }
    }
}
