
var anterior = document.querySelector('.anterior');
var siguiente = document.querySelector('.siguiente');
var visor = document.querySelector('.contenedor');
var fotos = ["sl01.jpg", "sl02.jpg", "sl03.jpg","sl04.jpg","sl05.jpg"];
var totalImagenes = fotos.length;

var fotoActual = 0;
var elemento;


    refrescaVisor();

    siguiente.addEventListener("click",avanza);
    anterior.addEventListener("click",retrocede);

    function avanza(){
        if(fotoActual==fotos.length-1){
            fotoActual=0;
        }else{
            fotoActual++;
        }
        refrescaVisor();
    }

    function retrocede(){
        if(fotoActual==0){
            fotoActual=fotos.length-1;
        }else{
            fotoActual--;
        }
        refrescaVisor();
    }

    function refrescaVisor(){
        
        document.querySelector('#fotoprincipal').src=fotos[fotoActual];
    }
    
    

